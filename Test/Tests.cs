﻿using System;
using System.Linq;
using WertbauLibrary;
using WertbauLibrary.Classes;
using WertbauLibrary.Types;

namespace Test
{
    public static class Tests
    {
        public static void GetPartIdFrameIdInfoTest()
        {
            Console.Clear();

            Console.WriteLine("Part- oder Frame-ID eingeben");
            string partId = Console.ReadLine();

            if (string.IsNullOrEmpty(partId))
            {
                Console.Clear();
                Console.WriteLine("Part- oder Frame-ID eingeben");
                
                partId = "00554073029007020110001";
                Console.WriteLine(partId);
            }

            PartIdFrameIdInfo partIdFrameIdInfo = Wertbau.GetPartIdFrameIdInfo(partId);

            if (partIdFrameIdInfo != null)
            {
                Console.WriteLine();
                Console.WriteLine("Auftragsnummer: " + partIdFrameIdInfo.AuftragsNummer);
                Console.WriteLine("Versionsnummer: " + partIdFrameIdInfo.VersionsNummer);
                Console.WriteLine("Position: " + partIdFrameIdInfo.Position);
                Console.WriteLine("Höhle / ID-Quadro: " + partIdFrameIdInfo.Hoehle_IdQuadro);
                Console.WriteLine("Material-ID: " + (partIdFrameIdInfo.MaterialId == null ? "<NULL>" : partIdFrameIdInfo.MaterialId));
                Console.WriteLine("Instanz: " + partIdFrameIdInfo.Instanz);
            }

            WiederholeTest(GetPartIdFrameIdInfoTest);
        }

        public static void GetBauverglasungPositionsTest()
        {
            Console.Clear();

            Console.WriteLine("Auftragsnummer eingeben");
            string aunr = Console.ReadLine();

            Console.WriteLine("Version eingeben");
            string versionString = Console.ReadLine();
            int version = 0;

            if(!string.IsNullOrEmpty(versionString))
                int.TryParse(versionString, out version);

            Console.WriteLine("EK?");
            string isEkString = Console.ReadLine().ToLower();

            if(string.IsNullOrEmpty(aunr))
            {
                Console.Clear();

                Console.WriteLine("Auftragsnummer eingeben");
                aunr = "180480";
                Console.WriteLine(aunr);

                Console.WriteLine("Version eingeben");
                version = 11;
                Console.WriteLine(version);

                Console.WriteLine("EK?");
                isEkString = "y";
                Console.WriteLine(isEkString);
            }

            bool isEk = false;

            switch(isEkString)
            {
                case "0": isEk = false;
                    break;
                case "1":
                    isEk = true;
                    break;
                case "false":
                    isEk = false;
                    break;
                case "true":
                    isEk = true;
                    break;
                case "n":
                    isEk = false;
                    break;
                case "y":
                    isEk = true;
                    break;
                case "j":
                    isEk = true;
                    break;
                case "no":
                    isEk = false;
                    break;
                case "yes":
                    isEk = true;
                    break;
                case "nein":
                    isEk = false;
                    break;
                case "ja":
                    isEk = true;
                    break;
            }

            int[] positions = Wertbau.GetBauverglasungPositions(aunr, version, isEk);

            if (positions.Length > 0)
                Console.WriteLine(string.Join(", ", positions.Select(x => x.ToString()).ToArray()));

            WiederholeTest(GetBauverglasungPositionsTest);
        }

        public static void GetBauverglasungTest()
        {
            Console.Clear();

            Console.WriteLine("Auftragsnummer eingeben");
            string aunr = Console.ReadLine();

            Console.WriteLine("Version eingeben");
            string versionString = Console.ReadLine();
            int version = 0;

            if (!string.IsNullOrEmpty(versionString))
                int.TryParse(versionString, out version);

            Console.WriteLine("Position eingeben");
            string positionString = Console.ReadLine();
            int position = 0;

            if (!string.IsNullOrEmpty(positionString))
                int.TryParse(positionString, out position);

            Console.WriteLine("EK?");
            string isEkString = Console.ReadLine().ToLower();

            if (string.IsNullOrEmpty(aunr))
            {
                Console.Clear();

                Console.WriteLine("Auftragsnummer eingeben");
                aunr = "180480";
                Console.WriteLine(aunr);

                Console.WriteLine("Version eingeben");
                version = 11;
                Console.WriteLine(version);

                Console.WriteLine("Position eingeben");
                position = 1;
                Console.WriteLine(position);

                Console.WriteLine("EK?");
                isEkString = "y";
                Console.WriteLine(isEkString);
            }

            bool isEk = false;

            switch (isEkString)
            {
                case "0":
                    isEk = false;
                    break;
                case "1":
                    isEk = true;
                    break;
                case "false":
                    isEk = false;
                    break;
                case "true":
                    isEk = true;
                    break;
                case "n":
                    isEk = false;
                    break;
                case "y":
                    isEk = true;
                    break;
                case "j":
                    isEk = true;
                    break;
                case "no":
                    isEk = false;
                    break;
                case "yes":
                    isEk = true;
                    break;
                case "nein":
                    isEk = false;
                    break;
                case "ja":
                    isEk = true;
                    break;
            }

            var isBauverglasung = Wertbau.GetBauverglasung(aunr, version, position, isEk);

            string isBauverglasungString = isBauverglasung ? "Ja" : "Nein";

            Console.WriteLine("Position ist Bauverglasung: " + isBauverglasungString);

            WiederholeTest(GetBauverglasungTest);
        }

        public static void GetFestverglasungTypTest()
        {
            Console.Clear();

            Console.WriteLine("Part- oder Frame-ID eingeben");
            string frameId = Console.ReadLine();

            if (string.IsNullOrEmpty(frameId))
            {
                Console.Clear();
                Console.WriteLine("Part- oder Frame-ID eingeben");

                frameId = "0000206601100101001";
                Console.WriteLine(frameId);
            }

            Festverglasung festverglasungsTyp = Wertbau.GetFestverglasungTyp(frameId);

            Console.WriteLine("Festverglasungstyp: " + festverglasungsTyp.ToString());

            WiederholeTest(GetFestverglasungTypTest);
        }

        public static void GetFertigungsTypeTest()
        {
            Console.Clear();

            Console.WriteLine("Auftragsnummer eingeben");
            string aunr = Console.ReadLine();

            Console.WriteLine("Position eingeben");
            string positionString = Console.ReadLine();
            int position = 0;

            if (!string.IsNullOrEmpty(positionString))
                int.TryParse(positionString, out position);

            if (string.IsNullOrEmpty(aunr))
            {
                Console.Clear();

                Console.WriteLine("Auftragsnummer eingeben");
                aunr = "180480";
                Console.WriteLine(aunr);
                
                Console.WriteLine("Position eingeben");
                position = 1;
                Console.WriteLine(position);
            }

            var fertigungsType = Wertbau.HoleFertigungstype(aunr, position);

            Console.WriteLine("TypeName: " + fertigungsType[0].ToString());
            Console.WriteLine("System: " + fertigungsType[1].ToString());

            WiederholeTest(GetFertigungsTypeTest);
        }

        // TESTS ===> ENDE

        #region Hilfsfunktionen

        private static void WiederholeTest(Action function)
        {
            Console.WriteLine("");
            Console.WriteLine("Nochmal (J/N)");
            var result = Console.ReadLine();

            if (result.ToUpper().Equals("J") || result.ToUpper().Equals("j"))
                function();
        }

        #endregion
    }
}
