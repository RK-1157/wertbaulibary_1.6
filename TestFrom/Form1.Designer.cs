﻿namespace TestFrom
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.dat_bis = new System.Windows.Forms.DateTimePicker();
            this.cbo_Bereich = new System.Windows.Forms.ComboBox();
            this.chk_all = new System.Windows.Forms.CheckBox();
            this.dat_von = new System.Windows.Forms.DateTimePicker();
            this.txt_Time = new System.Windows.Forms.TextBox();
            this.cmd_UpdateBereich = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Txt_trans_My = new System.Windows.Forms.TextBox();
            this.Txt_trans_Ms = new System.Windows.Forms.TextBox();
            this.Cmd_trans_Ms = new System.Windows.Forms.Button();
            this.Cmd_trans_My = new System.Windows.Forms.Button();
            this.txt_output = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dat_bis
            // 
            this.dat_bis.Location = new System.Drawing.Point(11, 38);
            this.dat_bis.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dat_bis.Name = "dat_bis";
            this.dat_bis.Size = new System.Drawing.Size(323, 22);
            this.dat_bis.TabIndex = 14;
            // 
            // cbo_Bereich
            // 
            this.cbo_Bereich.FormattingEnabled = true;
            this.cbo_Bereich.Location = new System.Drawing.Point(161, 68);
            this.cbo_Bereich.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbo_Bereich.Name = "cbo_Bereich";
            this.cbo_Bereich.Size = new System.Drawing.Size(175, 24);
            this.cbo_Bereich.TabIndex = 13;
            // 
            // chk_all
            // 
            this.chk_all.AutoSize = true;
            this.chk_all.Location = new System.Drawing.Point(91, 70);
            this.chk_all.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chk_all.Name = "chk_all";
            this.chk_all.Size = new System.Drawing.Size(53, 21);
            this.chk_all.TabIndex = 12;
            this.chk_all.Text = "Alle";
            this.chk_all.UseVisualStyleBackColor = true;
            this.chk_all.CheckedChanged += new System.EventHandler(this.chk_all_CheckedChanged);
            // 
            // dat_von
            // 
            this.dat_von.Location = new System.Drawing.Point(12, 11);
            this.dat_von.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dat_von.Name = "dat_von";
            this.dat_von.Size = new System.Drawing.Size(323, 22);
            this.dat_von.TabIndex = 11;
            // 
            // txt_Time
            // 
            this.txt_Time.Location = new System.Drawing.Point(12, 97);
            this.txt_Time.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_Time.Name = "txt_Time";
            this.txt_Time.ReadOnly = true;
            this.txt_Time.Size = new System.Drawing.Size(100, 22);
            this.txt_Time.TabIndex = 10;
            // 
            // cmd_UpdateBereich
            // 
            this.cmd_UpdateBereich.Location = new System.Drawing.Point(11, 68);
            this.cmd_UpdateBereich.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmd_UpdateBereich.Name = "cmd_UpdateBereich";
            this.cmd_UpdateBereich.Size = new System.Drawing.Size(75, 23);
            this.cmd_UpdateBereich.TabIndex = 9;
            this.cmd_UpdateBereich.Text = "Update";
            this.cmd_UpdateBereich.UseVisualStyleBackColor = true;
            this.cmd_UpdateBereich.Click += new System.EventHandler(this.cmd_UpdateBereich_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(11, 236);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Txt_trans_My
            // 
            this.Txt_trans_My.Location = new System.Drawing.Point(412, 14);
            this.Txt_trans_My.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Txt_trans_My.Name = "Txt_trans_My";
            this.Txt_trans_My.Size = new System.Drawing.Size(100, 22);
            this.Txt_trans_My.TabIndex = 16;
            // 
            // Txt_trans_Ms
            // 
            this.Txt_trans_Ms.Location = new System.Drawing.Point(517, 14);
            this.Txt_trans_Ms.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Txt_trans_Ms.Name = "Txt_trans_Ms";
            this.Txt_trans_Ms.Size = new System.Drawing.Size(100, 22);
            this.Txt_trans_Ms.TabIndex = 17;
            // 
            // Cmd_trans_Ms
            // 
            this.Cmd_trans_Ms.Location = new System.Drawing.Point(412, 41);
            this.Cmd_trans_Ms.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Cmd_trans_Ms.Name = "Cmd_trans_Ms";
            this.Cmd_trans_Ms.Size = new System.Drawing.Size(100, 50);
            this.Cmd_trans_Ms.TabIndex = 18;
            this.Cmd_trans_Ms.Text = "Transform to MS";
            this.Cmd_trans_Ms.UseVisualStyleBackColor = true;
            this.Cmd_trans_Ms.Click += new System.EventHandler(this.Cmd_trans_Ms_Click);
            // 
            // Cmd_trans_My
            // 
            this.Cmd_trans_My.Location = new System.Drawing.Point(517, 41);
            this.Cmd_trans_My.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Cmd_trans_My.Name = "Cmd_trans_My";
            this.Cmd_trans_My.Size = new System.Drawing.Size(100, 50);
            this.Cmd_trans_My.TabIndex = 19;
            this.Cmd_trans_My.Text = "Transform to MY";
            this.Cmd_trans_My.UseVisualStyleBackColor = true;
            this.Cmd_trans_My.Click += new System.EventHandler(this.Cmd_trans_My_Click);
            // 
            // txt_output
            // 
            this.txt_output.Location = new System.Drawing.Point(12, 124);
            this.txt_output.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_output.Name = "txt_output";
            this.txt_output.ReadOnly = true;
            this.txt_output.Size = new System.Drawing.Size(100, 22);
            this.txt_output.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(451, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 21;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(451, 184);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 22;
            this.label2.Text = "label2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_output);
            this.Controls.Add(this.Cmd_trans_My);
            this.Controls.Add(this.Cmd_trans_Ms);
            this.Controls.Add(this.Txt_trans_Ms);
            this.Controls.Add(this.Txt_trans_My);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dat_bis);
            this.Controls.Add(this.cbo_Bereich);
            this.Controls.Add(this.chk_all);
            this.Controls.Add(this.dat_von);
            this.Controls.Add(this.txt_Time);
            this.Controls.Add(this.cmd_UpdateBereich);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dat_bis;
        private System.Windows.Forms.ComboBox cbo_Bereich;
        private System.Windows.Forms.CheckBox chk_all;
        private System.Windows.Forms.DateTimePicker dat_von;
        private System.Windows.Forms.TextBox txt_Time;
        private System.Windows.Forms.Button cmd_UpdateBereich;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox Txt_trans_My;
        private System.Windows.Forms.TextBox Txt_trans_Ms;
        private System.Windows.Forms.Button Cmd_trans_Ms;
        private System.Windows.Forms.Button Cmd_trans_My;
        private System.Windows.Forms.TextBox txt_output;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

