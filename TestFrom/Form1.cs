﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TageszaehlerLibary;
using WertbauLibrary.Types;

namespace TestFrom
{
    public partial class Form1 : Form
    {
        private System.Diagnostics.Stopwatch SW = new System.Diagnostics.Stopwatch();

        public Form1()
        {
            InitializeComponent();

            cbo_Bereich.Items.AddRange(Enum.GetValues(typeof(Fertigungsbereich)).Cast<object>().ToArray());
            cbo_Bereich.SelectedItem = Fertigungsbereich.Endmontage_NGWA;
        }

        private void cmd_UpdateBereich_Click(object sender, EventArgs e)
        {
            SW.Restart();
            //Wertbau.UpdateTageszaehlerAll(new DateTime(2018, 9, 12));
            //Wertbau.UpdateTageszaehlerAll(DateTime.Now);
            //var TL = new List<Task>();
            //for (var val = dateTimePicker1.Value.Date; val <= DateTime.Today; val = val.AddDays(1))
            //{
            //    var T = new Task(() => Wertbau.UpdateTageszaehlerAll(val));
            //    T.Start();
            //    TL.Add(T);
            //}
            //Task.WaitAll(TL.ToArray());
            //Wertbau.UpdateTageszaehlerAll(dateTimePicker1.Value);
            if (chk_all.Checked)
                for (var date = dat_von.Value.Date; date <= dat_bis.Value.Date; date = date.AddDays(1))
                   Tageszaehler.UpdateTageszaehlerAll(date);
            else
                for (var date = dat_von.Value.Date; date <= dat_bis.Value.Date; date = date.AddDays(1))
                    txt_output.Text = Tageszaehler.UpdateTageszaehler(date, (Fertigungsbereich)cbo_Bereich.SelectedItem).RueckstandBisher.ToString();
            SW.Stop();
            txt_Time.Text = SW.Elapsed.ToString();
            //Wertbau.UpdateTageszaehler(new DateTime(2018,9,12), Wertbau.E_Fertigungsbereiche.Endmontage_Holz);
            MessageBox.Show("Finsihed");
        }

        private void chk_all_CheckedChanged(object sender, EventArgs e)
        {
            cbo_Bereich.Enabled = !chk_all.Checked;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            Tageszaehler.UpdateTageszaehlerAll(DateTime.Now);
        }

        private void Cmd_trans_Ms_Click(object sender, EventArgs e)
        {
            Txt_trans_Ms.Text = WertbauLibrary.Wertbau.AuftragMySqlToPPS(Txt_trans_My.Text);
        }

        private void Cmd_trans_My_Click(object sender, EventArgs e)
        {
            Txt_trans_My.Text = WertbauLibrary.Wertbau.AuftragPPSToMySql(Txt_trans_Ms.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Text = WertbauLibrary.Wertbau.HoleKennzeichen(WertbauLibrary.Wertbau.AuftragMySqlToPPS("19002722002", true));
            label2.Text = WertbauLibrary.Wertbau.HoleKennzeichen(WertbauLibrary.Wertbau.AuftragMySqlToPPS("19002722002", true));
        }
    }
}
