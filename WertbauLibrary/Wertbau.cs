﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using WertbauLibrary.Classes;
using WertbauLibrary.Types;


namespace WertbauLibrary
{
    public static class Wertbau

    {
        public static string AuftragPPSToMySql(string ppsAuftragsNummer, int Version = -1)
        {
            // Wertbau (alter Nummernkreis)
            if (!ppsAuftragsNummer.StartsWith("8") && ppsAuftragsNummer.Length < 10)
            {
                if (Version < 0)
                    return ppsAuftragsNummer;
                else
                    return ppsAuftragsNummer + Version.ToString("000");
            }
            else
            {
                var ret = ppsAuftragsNummer;

                // Bei B-Nummern (SapNr) wird die '8' am Anfang entfernt
                if (ppsAuftragsNummer.StartsWith("8"))
                    ret = ppsAuftragsNummer.Remove(0, 1);

                // Punkt einfügen
                var norm = ret.Substring(0, ret.Length - 2);
                var ends = ret.Substring(ret.Length - 2, 2);

                ret = norm + "." + ends;

                return ret;
            }
        }

        /// <summary>
        /// Gibt die Auftragsnummer mit oder ohne Version per Definition des Nummernkreises zurück (MySql / Tabelle 'feprod.auftrag')
        /// </summary>
        public static string AuftragMySqlToPPS(string auftragsNummer, string sapNummer, string sap2Nummer, bool versionAbschneiden = false)
        {
            // Wertbau-Auftragsnummer
            if (string.IsNullOrEmpty(sapNummer) && string.IsNullOrEmpty(sap2Nummer))
                return versionAbschneiden ? auftragsNummer.Remove(auftragsNummer.Length - 3, 3) : auftragsNummer;

            var auNr = string.Empty;

            // B-Nummern und Aufträge mit neuem NUmmernkreis
            if (!string.IsNullOrEmpty(sapNummer)) // Überprüfe Feld 'auftrag.sapnr'
            {
                auNr = sapNummer.StartsWith("B") ? sapNummer.Replace("B", "") : sapNummer;

                if (auNr.Length < 10)
                    auNr = "8" + auNr.Replace(".", "");
                else
                    auNr = auNr.Replace(".", "");

                return auNr;
            }

            if (string.IsNullOrEmpty(sap2Nummer))  // Überprüfe Feld 'auftrag.sapnr_2'
                return auNr;

            auNr = sap2Nummer.StartsWith("B") ? sap2Nummer.Replace("B", "") : sap2Nummer;

            if (auNr.Length < 10)
                auNr = "8" + auNr.Replace(".", "");
            else
                auNr = auNr.Replace(".", "");

            return auNr;
        }

        /// <summary>
        /// Gibt die Auftragsnummer mit oder ohne Version per Definition des Nummernkreises zurück (MySql / Tabelle 'feprod.auftrag')
        /// </summary>
        public static string AuftragMySqlToPPS(string auftragsNummer, bool versionAbschneiden = false)
        {
            if (string.IsNullOrEmpty(auftragsNummer))
                return auftragsNummer;

            // WICHTIG!!!! wurde leider vergessen...
            if (!IsSapNr(auftragsNummer) && (auftragsNummer.StartsWith("00") || (auftragsNummer.StartsWith("187"))))
            {
                using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
                {
                    conn.Open();
                    using (var comm = new MySqlCommand("SELECT Sapnr FROM Auftrag WHERE auftragsnr like '%" + auftragsNummer + "%'", conn))
                    using (var reader = comm.ExecuteReader())
                        if (reader.Read())
                        {
                            auftragsNummer = reader["Sapnr"].ToString();
                        }
                }
            }

            if (IsSapNr(auftragsNummer))
            {
                auftragsNummer = auftragsNummer.StartsWith("B") ? auftragsNummer.Replace("B", "") : auftragsNummer;

                if (auftragsNummer.Length < 10)
                    auftragsNummer = "8" + auftragsNummer.Replace(".", "");
                else
                    auftragsNummer = auftragsNummer.Replace(".", "");

                return auftragsNummer;
            }


            if (auftragsNummer.Length < 4)
                return auftragsNummer;

            return versionAbschneiden ? auftragsNummer.Remove(auftragsNummer.Length - 3, 3) : auftragsNummer;
        }

        /// <summary>
        /// Berechnet die Quadratmeter selbst statt sie zu extrahieren
        /// </summary>
        /// <param name="au"></param>
        /// <param name="berechnung"></param>
        /// <returns></returns>
        public static double AuftragQuadratMeter(string au, bool berechnung)
        {
            if (au.Length < 1) return 0.0;

            double qm = 0.0;

            if (au.Contains("."))
                au = HoleMySQLAuNrFromSAPNR(au);

            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                const string sql = @"SELECT SUM(Round((totalheight / 1000) * (totalwidth / 1000), 2)) AS Quadrat from position WHERE position.ORderNR like CONCAT(@AuNr,'%')";

                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@AuNr", au);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            qm += double.Parse(reader["Quadrat"].ToString());
                        }
                    }
                }
            }

            return Math.Round(qm, 2);
        }

        /// <summary>
        /// Hole Quadratmeter von Auftrag mit Aunr aus MYSQL
        /// </summary>
        /// <param name="Aunr"></param>
        /// <returns></returns>
        public static double AuftragQuadratMeter(string Aunr)
        {
            double qm = 0.0;

            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                const string sql = @"SELECT `Quadratmeter` AS QM FROM `auftragsstatus` WHERE `Auftragsnummer` LIKE CONCAT(@AuNr, '%')";

                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@AuNr", Aunr);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            qm += double.Parse(reader["QM"].ToString());
                        }
                    }
                }
            }

            if (qm <= 0)
                qm = AuftragQuadratMeter(Aunr, true);

            return Math.Round(qm, 2);
        }

        public static bool IsSapNr(string auftragsNummer)
        {
            return !string.IsNullOrEmpty(auftragsNummer) && auftragsNummer.Contains(".");
        }

        /// <summary>
        /// Bei Übergabe einer Auftragsnummer (184510.16 / 18007621003) gibt er zurück, ob es ein Auftrag für HAlle 6 ist anhand des Systems der Positionen.
        /// </summary>
        /// <param name="aunr">Auftragsnummer (184510.16 / 18007621003)</param>
        /// <returns>Bool Halle 6? JA/NEIN</returns>
        public static bool IstAuftragHalle6MySQL(string aunr)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                const string sql =
                    @"SELECT AuS.`Gebaeude6`, Au.`Halle6` AS Halle6
                    FROM `auftrag` AS Au
                    LEFT JOIN `auftragsstatus` AS AuS
	                    ON AuS.`Auftragsnummer` IN (Au.`AuftragsNr`,Au.`SAPNr`)
                    WHERE @AuNr IN (Au.`AuftragsNr`, Au.`SAPNr`)";

                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@AuNr", aunr);

                    using (var reader = cmd.ExecuteReader())
                        if (reader.Read())
                        {
                            if (reader.IsDBNull(0) || reader.IsDBNull(1))
                            {
                                bool istAuftragHalle6 = IstAuftragHalle6MySQLFromPosition(aunr);

                                SetAuftragHalle6MySQL(aunr, istAuftragHalle6);

                                return istAuftragHalle6;
                            }
                            return (reader.GetBoolean("Gebaeude6") || reader.GetBoolean("Halle6"));
                        }
                        else
                        {
                            bool istAuftragHalle6 = IstAuftragHalle6MySQLFromPosition(aunr);

                            SetAuftragHalle6MySQL(aunr, istAuftragHalle6);

                            return istAuftragHalle6;
                        }
                }
            }
        }

        /// <summary>
        /// Diese Funktion muss Public gestellt sein damit die Aufträge eingelesen werden können!
        /// Funktion gibt bool zurück, Gebäude6 Ja/Nein
        /// </summary>
        /// <param name="aunr"></param>
        /// <returns></returns>
        public static bool IstAuftragHalle6MySQLFromPosition(string aunr)
        {
            bool halle6 = false;

            string sqlSortOrder = " group by position.positionsid";

            string sqlcomm = @"SELECT orderposnr, profilesystem, productiontype from auftragsstatus 
                    JOIN Auftrag on auftragsstatus.auftrag_id = auftrag.id
                    JOIN position on auftrag.auftragsnr = position.ordernr
                    join framewood on position.positionsid =framewood.positionsid
                    WHERE auftragsnummer  = '" + aunr + "' " + sqlSortOrder;

            string[] systemF = { "", "" };

            MySqlConnection conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring);
            conn.Open();
            MySqlCommand comm = new MySqlCommand(sqlcomm, conn);
            MySqlDataReader reader = comm.ExecuteReader();
            while (reader.Read())
            {
                if (!halle6)
                {
                    string system = reader["profilesystem"].ToString();
                    string system2 = reader["productiontype"].ToString();
                    switch (system.Trim())
                    {
                        case "NGWA-SF":
                            {
                                halle6 = true;
                                break;
                            }
                        case "FE-HO SF":
                            {
                                halle6 = true;
                                break;
                            }

                        // Ab hier nur noch Holz 
                        case "HO-HT":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HO-2F-HT":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HO-HST":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HO-2F-HST":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HO-Sonderbau":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HO-2F-Sonderbau":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HO-CNC":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HO-2F-CNC":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HA-HT":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HA-HST":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HA-CNC":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HA-Linie UC":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HA-Sonderbau":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HA-Linie UC-GK":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HST-HO 216":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HST-HO/AL 216":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HST-HO/AL XL":
                            {
                                halle6 = true;
                                break;
                            }
                    }
                    switch (system2.Trim())
                    {
                        case "NGWA-SF":
                            {
                                halle6 = true;
                                break;
                            }
                        case "FE-HO SF":
                            {
                                halle6 = true;
                                break;
                            }

                        // Ab hier nur noch Holz 
                        case "HO-HT":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HO-2F-HT":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HO-HST":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HO-2F-HST":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HO-Sonderbau":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HO-2F-Sonderbau":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HO-CNC":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HO-2F-CNC":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HA-HT":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HA-HST":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HA-CNC":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HA-Linie UC":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HA-Sonderbau":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HA-Linie UC-GK":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HST-HO 216":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HST-HO/AL 216":
                            {
                                halle6 = true;
                                break;
                            }
                        case "HST-HO/AL XL":
                            {
                                halle6 = true;
                                break;
                            }
                    }
                    if (system2.Trim().Contains("HST") || system.Trim().Contains("HST"))
                    {
                        halle6 = true;
                        break;
                    }
                }
                if (halle6)
                    break;
            }
            reader.Close();
            conn.Close();


            return halle6;


            //bool halle6 = false;
            ////bool EKA = false;
            ////string auf = "";

            //string SqlDB = Konstanten.V200ConnectionString;

            //string sqlstr = GetFertigungsTypeSelect(aunr, -1);

            //if (aunr.Contains("."))
            //{
            //    //EKA = true;
            //    SqlDB = Konstanten.EKAWertbauConnectionString;
            //    /*string au = aunr.Split('.')[0];
            //    string sub = aunr.Split('.')[1];

            //    sqlstr = "SELECT     ContenidoPAFSubModels.Numero, ContenidoPAFSubModels.Version, ContenidoPAFSubModels.Orden, ProductionTypes.TypeName,  " +
            //              "ContenidoPAF.SortOrder + 1 as Position , ContenidoPAF.System as Sys " +
            //            "FROM PAF " +
            //                "JOIN ContenidoPAFSubModels ON PAF.Numero = ContenidoPAFSubModels.Numero AND PAF.Version = ContenidoPAFSubModels.Version " +
            //            "INNER JOIN " +
            //                "ProductionTypes ON ContenidoPAFSubModels.ProductionType = ProductionTypes.RowId " +
            //            " INNER JOIN " +
            //                "ContenidoPAF ON ContenidoPAFSubModels.Numero = ContenidoPAF.Numero AND ContenidoPAFSubModels.Version = ContenidoPAF.Version AND " +
            //                "ContenidoPAFSubModels.Orden = ContenidoPAF.Orden " +
            //            "where PAF.CIP = '" + au + "' AND PAF.SubOrderNumber = " + sub + "; COMMIT;";*/
            //}
            //else
            //{
            //    /*sqlstr = "SELECT     ContenidoPAFSubModels.Numero, ContenidoPAFSubModels.Version, ContenidoPAFSubModels.Orden, ProductionTypes.TypeName, " +
            //            "ContenidoPAF.SortOrder + 1 as  Position , ContenidoPAF.System as Sys " +
            //            "FROM ContenidoPAFSubModels INNER JOIN " +
            //                  "ProductionTypes ON ContenidoPAFSubModels.ProductionType = ProductionTypes.RowId INNER JOIN " +
            //                  "ContenidoPAF ON ContenidoPAFSubModels.Numero = ContenidoPAF.Numero AND ContenidoPAFSubModels.Version = ContenidoPAF.Version AND " +
            //                  "ContenidoPAFSubModels.Orden = ContenidoPAF.Orden " +
            //            "where ContenidoPAF.Numero like '%" + (aunr.Length == "18007644".Length ? aunr : aunr.Remove(aunr.Length - 3, 3)) + "%' and ContenidoPAF.Version = 2" +
            //            "; COMMIT;";*/
            //}

            //using (var conn = new SqlConnection(SqlDB))
            //{
            //    conn.Open();

            //    using (var comm = new SqlCommand(sqlstr, conn))
            //    {
            //        SqlDataReader reader = comm.ExecuteReader();

            //        while (reader.Read())
            //        {
            //            if (!halle6)
            //            {
            //                string system = reader["TypeName"].ToString();
            //                string system2 = reader["Sys"].ToString();

            //                switch (system.Trim())
            //                {
            //                    case "NGWA-SF":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "FE-HO SF":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }

            //                    // Ab hier nur noch Holz 
            //                    case "HO-HT":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HO-2F-HT":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HO-HST":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HO-2F-HST":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HO-Sonderbau":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HO-2F-Sonderbau":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HO-CNC":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HO-2F-CNC":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HA-HT":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HA-HST":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HA-CNC":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HA-Linie UC":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HA-Sonderbau":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HA-Linie UC-GK":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                }
            //                switch (system2.Trim())
            //                {
            //                    case "HST-HO 216":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HST-HO/AL 216":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                    case "HST-HO/AL XL":
            //                        {
            //                            halle6 = true;
            //                            break;
            //                        }
            //                }
            //            }
            //        }
            //    }
            //}

            //return halle6;
        }

        public static void SetAuftragHalle6MySQL(string aunr, bool isHalle6)
        {
            const string sql =
                    @"UPDATE `auftrag` SET `Halle6` = @IsHalle6
                    WHERE  @AuNr IN (`SAPNr`, `AuftragsNr`);";
            const string sql2 =
                    @"UPDATE `auftragsstatus` SET `Gebaeude6` = @IsHalle6
                     WHERE  @AuNr =`Auftragsnummer`;";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@AuNr", aunr);
                    cmd.Parameters.AddWithValue("@IsHalle6", isHalle6 ? 1 : 0);

                    cmd.ExecuteNonQuery();

                    cmd.CommandText = sql2;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Gibt zurück ob es sich um WS1 Holz System handelt oder nicht
        /// </summary>
        /// <param name="aunr"></param>
        /// <returns>Bool WS1 Fenster? JA/NEIN</returns>
        public static bool IstAuftragWS1(string aunr)
        {
            #region OLD
            /*using (MySqlConnection conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                bool ws1 = false;

                string SQLstr = "SELECT IFNULL(ProfileSystem,'') from Auftrag " +
                    "JOIN Position on Auftrag.Auftragsnr = Position.Ordernr " +
                    "WHERE Auftrag.Auftragsnr = @AuNr OR Auftrag.SAPNR = @AuNr";

                using (MySqlCommand comm = new MySqlCommand(SQLstr, conn))
                {
                    comm.Parameters.Add("@AuNr", MySqlDbType.VarString);
                    using (MySqlDataReader reader = comm.ExecuteReader())
                        while (reader.Read())
                            if (reader.GetString(0).Contains("FE-HO"))
                            {
                                ws1 = true;
                                break;
                            }

                }
                return ws1;
            }*/
            #endregion

            bool WS1 = false;

            string sqlSortOrder = " group by position.positionsid";

            string sqlcomm = @"SELECT orderposnr, profilesystem, productiontype from auftragsstatus 
                    JOIN Auftrag on auftragsstatus.auftrag_id = auftrag.id
                    JOIN position on auftrag.auftragsnr = position.ordernr
                    join framewood on position.positionsid =framewood.positionsid
                    WHERE auftragsnummer  = '" + aunr + "' " + sqlSortOrder;

            string[] systemF = { "", "" };

            MySqlConnection conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring);
            conn.Open();
            MySqlCommand comm = new MySqlCommand(sqlcomm, conn);
            MySqlDataReader reader = comm.ExecuteReader();
            while (reader.Read())
            {
                if (!WS1)
                {
                    string system = reader["profilesystem"].ToString();
                    string system2 = reader["productiontype"].ToString();
                    if (system.Contains("FE-HO"))
                    {
                        WS1 = true;
                        break;
                    }
                    if (system2.Contains("FE-HO"))
                    {
                        WS1 = true;
                        break;
                    }
                }
                if (WS1)
                    break;
            }
            reader.Close();
            conn.Close();


            return WS1;



            //bool WS1 = false;
            ////bool EKA = false;
            ////string auf = "";

            //string SqlDB = Konstanten.V200ConnectionString;

            //string sqlstr = GetFertigungsTypeSelect(aunr, -1);

            //if (aunr.Contains("."))
            //{
            //    //EKA = true;
            //    SqlDB = Konstanten.EKAWertbauConnectionString;
            //    /*string au = aunr.Split('.')[0];
            //    string sub = aunr.Split('.')[1];

            //    sqlstr = "SELECT     ContenidoPAFSubModels.Numero, ContenidoPAFSubModels.Version, ContenidoPAFSubModels.Orden, ProductionTypes.TypeName,  " +
            //              "ContenidoPAF.SortOrder + 1 as Position , ContenidoPAF.System as Sys " +
            //            "FROM PAF " +
            //                "JOIN ContenidoPAFSubModels ON PAF.Numero = ContenidoPAFSubModels.Numero AND PAF.Version = ContenidoPAFSubModels.Version " +
            //            "INNER JOIN " +
            //                "ProductionTypes ON ContenidoPAFSubModels.ProductionType = ProductionTypes.RowId " +
            //            " INNER JOIN " +
            //                "ContenidoPAF ON ContenidoPAFSubModels.Numero = ContenidoPAF.Numero AND ContenidoPAFSubModels.Version = ContenidoPAF.Version AND " +
            //                "ContenidoPAFSubModels.Orden = ContenidoPAF.Orden " +
            //            "where PAF.CIP = '" + au + "' AND PAF.SubOrderNumber = " + sub + "; COMMIT;";*/
            //}
            //else
            //{
            //    /*sqlstr = "SELECT     ContenidoPAFSubModels.Numero, ContenidoPAFSubModels.Version, ContenidoPAFSubModels.Orden, ProductionTypes.TypeName, " +
            //            "ContenidoPAF.SortOrder + 1 as  Position , ContenidoPAF.System as Sys " +
            //            "FROM ContenidoPAFSubModels INNER JOIN " +
            //                  "ProductionTypes ON ContenidoPAFSubModels.ProductionType = ProductionTypes.RowId INNER JOIN " +
            //                  "ContenidoPAF ON ContenidoPAFSubModels.Numero = ContenidoPAF.Numero AND ContenidoPAFSubModels.Version = ContenidoPAF.Version AND " +
            //                  "ContenidoPAFSubModels.Orden = ContenidoPAF.Orden " +
            //            "where ContenidoPAF.Numero like '%" + (aunr.Length == "18007644".Length ? aunr : aunr.Remove(aunr.Length - 3, 3)) + "%' and ContenidoPAF.Version = 2" +
            //            "; COMMIT;";*/
            //}

            //using (var conn = new SqlConnection(SqlDB))
            //{
            //    conn.Open();

            //    using (var comm = new SqlCommand(sqlstr, conn))
            //    {
            //        SqlDataReader reader = comm.ExecuteReader();

            //        while (reader.Read())
            //        {
            //            if (!WS1)
            //            {
            //                string system = reader["TypeName"].ToString();
            //                string system2 = reader["Sys"].ToString();

            //                if (system.Contains("FE-HO"))
            //                {
            //                    WS1 = true;
            //                    break;
            //                }
            //                if (system2.Contains("FE-HO"))
            //                {
            //                     WS1 = true;
            //                    break;
            //                }
            //            }
            //            if (WS1)
            //                break;
            //        }
            //    }
            //}

            //return WS1;
        }

        /// <summary>
        /// Gibt zurück ob es sich um WS1 Holz System handelt oder nicht
        /// </summary>
        /// <param name="aunr"></param>
        /// <returns>Bool WS1 Fenster? JA/NEIN</returns>
        public static bool IstAuftragHT(string aunr)
        {
            using (MySqlConnection conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                bool ht = false;

                string SQLstr = "SELECT framewood.typ  FROM auftrag " +
                "JOIN Position on auftrag.auftragsnr = position.ordernr   " +
                "JOIN framewood on position.positionsid = framewood.positionsid " +
                "WHERE auftragsnr like '@AuNr%' OR sapnr like '@AuNr%'";

                using (MySqlCommand comm = new MySqlCommand(SQLstr, conn))
                {
                    comm.Parameters.Add("@AuNr", MySqlDbType.VarString).Value = aunr;
                    using (MySqlDataReader reader = comm.ExecuteReader())
                        while (reader.Read())
                            if (reader.GetString(0).Contains("HT"))
                            {
                                ht = true;
                                break;
                            }

                }
                return ht;
            }
        }

        /// <summary>
        /// Gibt zurück ob es sich um HST System handelt oder nicht
        /// </summary>
        /// <param name="aunr"></param>
        /// <returns>Bool HST Fenster? JA/NEIN</returns>
        public static bool IstAuftragHST(string aunr) // wieso ist diese Funktion private gewesen???? 22.05.2019 M.E. <- wurde bereits mehrfach wieder Public gestellt!!!!
        {
            bool HST = false;

            string sqlSortOrder = " group by position.positionsid";

            string sqlcomm = @"SELECT orderposnr, profilesystem, productiontype from auftragsstatus 
                    JOIN Auftrag on auftragsstatus.auftrag_id = auftrag.id
                    JOIN position on auftrag.auftragsnr = position.ordernr
                    join framewood on position.positionsid =framewood.positionsid
                    WHERE auftragsnummer  = '" + aunr + "' " + sqlSortOrder;

            string[] systemF = { "", "" };

            MySqlConnection conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring);
            conn.Open();
            MySqlCommand comm = new MySqlCommand(sqlcomm, conn);
            MySqlDataReader reader = comm.ExecuteReader();
            while (reader.Read())
            {
                if (!HST)
                {
                    string system = reader["profilesystem"].ToString();
                    string system2 = reader["productiontype"].ToString();

                    switch (system.Trim())
                    {
                        case "HO-HST":
                            {
                                HST = true;
                                break;
                            }
                        case "HO-2F-HST":
                            {
                                HST = true;
                                break;
                            }
                        case "HA-HST":
                            {
                                HST = true;
                                break;
                            }

                    }
                    switch (system2.Trim())
                    {
                        case "HST-HO 216":
                            {
                                HST = true;
                                break;
                            }
                        case "HST-HO/AL 216":
                            {
                                HST = true;
                                break;
                            }
                        case "HST-HO/AL XL":
                            {
                                HST = true;
                                break;
                            }
                    }
                }
                if (HST)
                    break;
            }
            reader.Close();
            conn.Close();


            return HST;

            ////bool EKA = false;
            ////string auf = "";

            //string SqlDB = Konstanten.V200ConnectionString;

            //string sqlstr = GetFertigungsTypeSelect(aunr, -1);

            //if (aunr.Contains("."))
            //{
            //    //EKA = true;
            //    SqlDB = Konstanten.EKAWertbauConnectionString;
            //    /*string au = aunr.Split('.')[0];
            //    string sub = aunr.Split('.')[1];

            //    sqlstr = "SELECT     ContenidoPAFSubModels.Numero, ContenidoPAFSubModels.Version, ContenidoPAFSubModels.Orden, ProductionTypes.TypeName,  " +
            //              "ContenidoPAF.SortOrder + 1 as Position , ContenidoPAF.System as Sys " +
            //            "FROM PAF " +
            //                "JOIN ContenidoPAFSubModels ON PAF.Numero = ContenidoPAFSubModels.Numero AND PAF.Version = ContenidoPAFSubModels.Version " +
            //            "INNER JOIN " +
            //                "ProductionTypes ON ContenidoPAFSubModels.ProductionType = ProductionTypes.RowId " +
            //            " INNER JOIN " +
            //                "ContenidoPAF ON ContenidoPAFSubModels.Numero = ContenidoPAF.Numero AND ContenidoPAFSubModels.Version = ContenidoPAF.Version AND " +
            //                "ContenidoPAFSubModels.Orden = ContenidoPAF.Orden " +
            //            "where PAF.CIP = '" + au + "' AND PAF.SubOrderNumber = " + sub + "; COMMIT;";*/
            //}
            //else
            //{
            //    /*sqlstr = "SELECT     ContenidoPAFSubModels.Numero, ContenidoPAFSubModels.Version, ContenidoPAFSubModels.Orden, ProductionTypes.TypeName, " +
            //            "ContenidoPAF.SortOrder + 1 as  Position , ContenidoPAF.System as Sys " +
            //            "FROM ContenidoPAFSubModels INNER JOIN " +
            //                  "ProductionTypes ON ContenidoPAFSubModels.ProductionType = ProductionTypes.RowId INNER JOIN " +
            //                  "ContenidoPAF ON ContenidoPAFSubModels.Numero = ContenidoPAF.Numero AND ContenidoPAFSubModels.Version = ContenidoPAF.Version AND " +
            //                  "ContenidoPAFSubModels.Orden = ContenidoPAF.Orden " +
            //            "where ContenidoPAF.Numero like '%" + (aunr.Length == "18007644".Length ? aunr : aunr.Remove(aunr.Length - 3, 3)) + "%' and ContenidoPAF.Version = 2" +
            //            "; COMMIT;";*/
            //}

            //using (var conn = new SqlConnection(SqlDB))
            //{
            //    conn.Open();

            //    using (var comm = new SqlCommand(sqlstr, conn))
            //    {
            //        SqlDataReader reader = comm.ExecuteReader();

            //        while (reader.Read())
            //        {
            //            if (!HST)
            //            {
            //                string system = reader["TypeName"].ToString();
            //                string system2 = reader["Sys"].ToString();

            //                switch (system.Trim())
            //                {          
            //                    case "HO-HST":
            //                        {
            //                            HST = true;
            //                            break;
            //                        }
            //                    case "HO-2F-HST":
            //                        {
            //                            HST = true;
            //                            break;
            //                        }                               
            //                    case "HA-HST":
            //                        {
            //                            HST = true;
            //                            break;
            //                        }

            //                }
            //                switch (system2.Trim())
            //                {
            //                    case "HST-HO 216":
            //                        {
            //                            HST = true;
            //                            break;
            //                        }
            //                    case "HST-HO/AL 216":
            //                        {
            //                            HST = true;
            //                            break;
            //                        }
            //                    case "HST-HO/AL XL":
            //                        {
            //                            HST = true;
            //                            break;
            //                        }
            //                }
            //            }
            //            if (HST)
            //                break;
            //        }
            //    }
            //}

            //return HST;
        }

        /// <summary>
        /// Gibt den FertigungsTypen aus vom Auftrag, object[] [0] = ProductionType [1] = ProfileSystem
        /// </summary>
        /// <param name="aunr">Beispiel 1801125.13 oder 18007644002 oder 18007644</param>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static object[] HoleFertigungstype(string aunr, int pos)
        {
            string sqlSortOrder = pos > 0 ? " AND orderposnr = " + (pos) + " group by position.positionsid " : " group by position.positionsid";

            string sqlcomm = @"SELECT orderposnr, profilesystem, productiontype from auftragsstatus 
                    JOIN Auftrag on auftragsstatus.auftrag_id = auftrag.id
                    JOIN position on auftrag.auftragsnr = position.ordernr
                    join framewood on position.positionsid =framewood.positionsid
                    WHERE auftragsnummer  = '" + aunr + "' " + sqlSortOrder;

            string[] systemF = { "", "" };

            MySqlConnection conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring);
            conn.Open();
            MySqlCommand comm = new MySqlCommand(sqlcomm, conn);
            MySqlDataReader reader = comm.ExecuteReader();
            while (reader.Read())
            {
                systemF[1] = reader["profilesystem"].ToString();
                systemF[0] = reader["productiontype"].ToString();
            }
            reader.Close();
            conn.Close();


            return systemF;


            //string SqlDB = Konstanten.V200ConnectionString;

            //string sqlstr = GetFertigungsTypeSelect(aunr, pos);

            //if (aunr.Contains("."))
            //{
            //    SqlDB = Konstanten.EKAWertbauConnectionString;
            //    /*string au = aunr.Split('.')[0];
            //    string sub = aunr.Split('.')[1];

            //    sqlstr = "SELECT     ContenidoPAFSubModels.Numero, ContenidoPAFSubModels.Version, ContenidoPAFSubModels.Orden, ProductionTypes.TypeName,  " +
            //              "ContenidoPAF.SortOrder + 1 as Position ,ContenidoPAF.System as Sys " +
            //            "FROM PAF " +
            //                "JOIN ContenidoPAFSubModels ON PAF.Numero = ContenidoPAFSubModels.Numero AND PAF.Version = ContenidoPAFSubModels.Version " +
            //            "INNER JOIN " +
            //                "ProductionTypes ON ContenidoPAFSubModels.ProductionType = ProductionTypes.RowId " +
            //            " INNER JOIN " +
            //                "ContenidoPAF ON ContenidoPAFSubModels.Numero = ContenidoPAF.Numero AND ContenidoPAFSubModels.Version = ContenidoPAF.Version AND " +
            //                "ContenidoPAFSubModels.Orden = ContenidoPAF.Orden " +
            //            "where PAF.CIP = '" + au + "' AND PAF.SubOrderNumber = " + sub + "  AND ContenidoPAF.SortOrder = '" + (pos - 1) + "'; COMMIT;";*/
            //}
            //else
            //{
            //    /*sqlstr = "SELECT     ContenidoPAFSubModels.Numero, ContenidoPAFSubModels.Version, ContenidoPAFSubModels.Orden, ProductionTypes.TypeName, " +
            //            "ContenidoPAF.SortOrder + 1 as  Position ,ContenidoPAF.System as Sys " +
            //            "FROM ContenidoPAFSubModels INNER JOIN " +
            //                  "ProductionTypes ON ContenidoPAFSubModels.ProductionType = ProductionTypes.RowId INNER JOIN " +
            //                  "ContenidoPAF ON ContenidoPAFSubModels.Numero = ContenidoPAF.Numero AND ContenidoPAFSubModels.Version = ContenidoPAF.Version AND " +
            //                  "ContenidoPAFSubModels.Orden = ContenidoPAF.Orden " +
            //            "where ContenidoPAF.Numero like '%" + (aunr.Length == "18007644".Length ? aunr : aunr.Remove(aunr.Length - 3, 3)) + "%' and ContenidoPAF.Version = 2" +
            //            " AND ContenidoPAF.SortOrder = '" + (pos - 1) + "'; COMMIT;";*/
            //}

            //using (var conn = new SqlConnection(SqlDB))
            //{
            //    conn.Open();

            //    using (var comm = new SqlCommand(sqlstr, conn))
            //    {
            //        SqlDataReader reader = comm.ExecuteReader();
            //        while (reader.Read())
            //        {
            //            string system = reader["TypeName"].ToString();
            //            ret[0] = system;
            //            system = reader["Sys"].ToString();
            //            ret[1] = system;
            //        }
            //    }
            //}

            //  return ret;
        }

        private static string GetFertigungsTypeSelect(string aunr, int pos)
        {
            string sqlstr = "";
            string sqlSortOrder = pos > 0 ? " AND Sortorder = '" + (pos - 1) + "' " : "";


            if (aunr.Contains("."))
            {
                string au = aunr.Split('.')[0];
                string sub = aunr.Split('.')[1];

                sqlstr = "SELECT TypeName, Sys FROM vwFertigungsType WHERE CIP = '" + au +
                    "' AND SubOrderNumber = " + sub + sqlSortOrder;
            }
            else
            {
                sqlstr = "SELECT TypeName, Sys FROM vwFertigungsType " +
                        "WHERE Numero like '%" + (aunr.Length == "18007644".Length ? aunr : aunr.Remove(aunr.Length - 3, 3)) +
                        "%' and Version = 2" + sqlSortOrder;
            }

            return sqlstr;
        }


        public static int[] GetBauverglasungPositions(string aunr, int version, bool isEk)
        {
            int[] positions = null;
            var tempAunr = aunr;
            var tempVersion = version.ToString();

            if (aunr.Contains("."))
            {
                var aunrArray = aunr.Split(new char[] { '.' });

                tempAunr = aunrArray[0];
                tempVersion = aunrArray[1];
            }

            string sql = @"SELECT DISTINCT (orden + 1) as position FROM PAF 
                Left JOIN MaterialesPAF ON PAF.Numero = MaterialesPAF.Numero 
                AND PAF.Version = MaterialesPAF.Version
                WHERE CIP = '" + tempAunr + @"' 
                AND SubOrderNumber = '" + tempVersion + @"' 
                AND GlassNumber > 0 
                AND Longitud > 0 
                AND Altura > 0  
                AND Composite = 1  
                AND Unmounted = 1;";

            var connectionString = isEk ? Konstanten.EKAWertbauConnectionString : Konstanten.V200ConnectionString;

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (var cmd = new SqlCommand(sql, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var posList = new List<int>();

                        while (reader.Read())
                        {
                            int position = reader.GetInt32(0);

                            posList.Add(position);
                        }

                        positions = posList.ToArray();
                    }
                }
            }

            return positions;
        }

        public static bool GetBauverglasung(string aunr, int version, int position, bool isEk)
        {
            bool isBauverglasung = false;

            var tempAunr = aunr;
            var tempVersion = version.ToString();

            if (aunr.Contains("."))
            {
                var aunrArray = aunr.Split(new char[] { '.' });

                tempAunr = aunrArray[0];
                tempVersion = aunrArray[1];
            }

            string sql = @"SELECT COUNT(DISTINCT (orden + 1)) as position FROM PAF 
                Left JOIN MaterialesPAF ON PAF.Numero = MaterialesPAF.Numero 
                AND PAF.Version = MaterialesPAF.Version
                WHERE CIP = '" + tempAunr + @"' 
                AND SubOrderNumber = '" + tempVersion + @"'
                AND (orden + 1) = " + position + @" 
                AND GlassNumber > 0 
                AND Longitud > 0 
                AND Altura > 0  
                AND Composite = 1  
                AND Unmounted = 1;";

            var connectionString = isEk ? Konstanten.EKAWertbauConnectionString : Konstanten.V200ConnectionString;

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (var cmd = new SqlCommand(sql, conn))
                {
                    var result = (int)cmd.ExecuteScalar();

                    if (result == 1)
                        isBauverglasung = true;
                }
            }

            return isBauverglasung;
        }

        public static Festverglasung GetFestverglasungTyp(string partIdFrameId)
        {
            if (!(partIdFrameId.Length == 23 || partIdFrameId.Length == 19))
            {
                Debug.WriteLine("Länge der übergeben ID ist nicht 23 oder 19, sondern " + partIdFrameId.Length);
                throw new Exception("Länge der übergeben ID ist nicht 23 oder 19, sondern " + partIdFrameId.Length);
            }

            string frameId = partIdFrameId;
            Festverglasung festverglasungTyp = Festverglasung.Rahmen;

            if (frameId.Length == 23)
                frameId = frameId.Remove(16, 4);

            var sql = @"SELECT fw.`InnenAussenVerglasung`
                FROM framewood fw
                WHERE FrameId = @FrameID";

            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@FrameID", MySqlDbType.VarString).Value = frameId;

                    var result = cmd.ExecuteScalar();
                    if (result == null || result is DBNull)
                        festverglasungTyp = Festverglasung.Rahmen;
                    else
                        festverglasungTyp = (Festverglasung)result;
                }
            }

            return festverglasungTyp;
        }

        private static string GetPartNameShort(string rfid)
        {
            string aunr = "";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                using (var cmd = new MySqlCommand("SELECT PartNameShort FROM partwood WHERE partid = '" + rfid + "' limit 1", conn))
                {
                    using (var reader = cmd.ExecuteReader())
                        if (reader.Read())
                        {
                            aunr = reader["PartNameShort"].ToString();
                        }
                }
            }

            return aunr;
        }

        public static PartIdFrameIdInfo GetPartIdFrameIdInfo(string partIdFrameId)
        {
            if (!(partIdFrameId.Length == 23 || partIdFrameId.Length == 19))
            {
                Debug.WriteLine("Länge der übergeben ID ist nicht 23 oder 19, sondern " + partIdFrameId.Length);
                throw new Exception("Länge der übergeben ID ist nicht 23 oder 19, sondern " + partIdFrameId.Length);
            }

            var ret = new PartIdFrameIdInfo()
            {
                AuftragsNummer = partIdFrameId.Substring(0, 11),
                Bnummer = HoleMySQLSapnrFromAunr(partIdFrameId.Substring(0, 11)),
                VersionsNummer = partIdFrameId.Substring(8, 3),
                Position = partIdFrameId.Substring(11, 3),
                Hoehle_IdQuadro = partIdFrameId.Substring(14, 2),
                PartNameShort = GetPartNameShort(partIdFrameId.Length == 23 ? partIdFrameId : "0")
            };


            if (partIdFrameId.Length == 23)
            {
                ret.MaterialId = partIdFrameId.Substring(16, 4);
                ret.Instanz = partIdFrameId.Substring(20, 3);

                ret.PartID = partIdFrameId;
                ret.FrameID = ret.AuftragsNummer + ret.Position + ret.Hoehle_IdQuadro + ret.Instanz;
            }
            else
            {
                ret.MaterialId = null;
                ret.Instanz = partIdFrameId.Substring(16, 3);

                ret.PartID = null;
                ret.FrameID = partIdFrameId;
            }

            ret.PositionID = ret.AuftragsNummer + ret.Position + ret.Instanz;

            return ret;
        }

        public static PartImages HolePositionsImagesVonPositionsId(string positionsId,
            bool metafile = true, bool horizontale = true, bool verticale = true)
        {
            PartImages partImages = new PartImages();

            if (string.IsNullOrEmpty(positionsId) || (!metafile && !horizontale && !verticale))
                return partImages;
            else
            {
                using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
                {
                    conn.Open();

                    string columns = "";
                    columns += metafile ? "metafile, " : "";
                    columns += horizontale ? "horizontale, " : "";
                    columns += verticale ? "verticale, " : "";

                    columns = columns.Remove(columns.LastIndexOf(','), 1); // letztes Komma entfernen

                    string sqlstr =
                       @"SELECT " + columns + @"
                    FROM position pos                     
                    LEFT JOIN positionblob pb 
	                    ON pb.position_positionsid = pos.positionsid 
                    WHERE pos.positionsid = @PositionsId;";

                    using (var cmd = new MySqlCommand(sqlstr, conn))
                    {
                        cmd.Parameters.Add("@PositionsId", MySqlDbType.VarString).Value = positionsId;

                        using (var reader = cmd.ExecuteReader())
                            while (reader.Read())
                            {

                                if (metafile)
                                    if (!reader.IsDBNull(reader.GetOrdinal("metafile")))
                                    {
                                        var MS = new MemoryStream((byte[])reader["metafile"]);
                                        partImages.Metafile = Image.FromStream(MS);
                                    }

                                if (horizontale)
                                    if (!reader.IsDBNull(reader.GetOrdinal("horizontale")))
                                    {
                                        var MS = new MemoryStream((byte[])reader["horizontale"]);
                                        partImages.Horizontale = Image.FromStream(MS);
                                    }

                                if (verticale)
                                    if (!reader.IsDBNull(reader.GetOrdinal("verticale")))
                                    {
                                        var MS = new MemoryStream((byte[])reader["verticale"]);
                                        partImages.Verticale = Image.FromStream(MS);
                                    }
                            }
                    }
                }
            }

            return partImages;
        }

        public static void HoleAuftragrueckstellung(string AuNr, MySqlConnection conn, out bool Rck, out string RckGrund)
        {
            HoleAuftragrueckstellung(AuNr, conn, out Rck, out RckGrund, out DateTime Datum);
        }

        public static void HoleAuftragrueckstellung(string AuNr, MySqlConnection conn, out bool Rck, out string RckGrund, out DateTime Datum)
        {
            Rck = false;
            RckGrund = "";
            Datum = DateTime.MinValue;
            Fertigungsbereich bereich = Fertigungsbereich.None;
            using (var cmd = new MySqlCommand(
                @"  SELECT aur.`Bereich`, aur.`Grund`, aur.`Datum`,aus.`Profilierung_ist`,aus.`Einzelstab_ist`,aus.`Eckverbindung_ist`, aus.`Oberflaeche_ist`, aus.`Endmontage_ist`
                    FROM `auftragsrueckstellung` as aur
                    LEFT JOIN `auftragsstatus` as aus
	                    ON aur.`Auftragsnummer` = aus.`Auftragsnummer`
                    WHERE aur.`Auftragsnummer` = @AuNr", conn))
            {
                cmd.Parameters.Add("@AuNR", MySqlDbType.VarString).Value = AuNr;
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        Fertigungsbereich new_Bereich = (Fertigungsbereich)reader.GetInt32("Bereich");
                        switch (new_Bereich)
                        {
                            #region "Profilierung"
                            case Fertigungsbereich.Profilierung:
                            case Fertigungsbereich.Profillierung_Geb6:
                                switch (bereich)
                                {
                                    case Fertigungsbereich.None:
                                    case Fertigungsbereich.Einzelstab:
                                    case Fertigungsbereich.Eckverbindung:
                                    case Fertigungsbereich.Oberflaeche:
                                    case Fertigungsbereich.Endmontage_Holz:
                                    case Fertigungsbereich.Endmontage_NGWA:
                                        if (reader.IsDBNull(reader.GetOrdinal("Profilierung_ist"))
                                            && reader.IsDBNull(reader.GetOrdinal("Einzelstab_ist"))
                                            && reader.IsDBNull(reader.GetOrdinal("Eckverbindung_ist"))
                                            && reader.IsDBNull(reader.GetOrdinal("Oberflaeche_ist"))
                                            && reader.IsDBNull(reader.GetOrdinal("Endmontage_ist")))
                                        {
                                            bereich = new_Bereich;
                                            Rck = true;
                                            RckGrund = "Profilierung:" + reader.GetString("Grund");
                                            Datum = reader.GetDateTime("Datum");
                                        }
                                        break;
                                }
                                break;
                            #endregion
                            #region "Einzelstab"
                            case Fertigungsbereich.Einzelstab:
                                switch (bereich)
                                {
                                    case Fertigungsbereich.None:
                                    case Fertigungsbereich.Eckverbindung:
                                    case Fertigungsbereich.Oberflaeche:
                                    case Fertigungsbereich.Endmontage_Holz:
                                    case Fertigungsbereich.Endmontage_NGWA:
                                        if (reader.IsDBNull(reader.GetOrdinal("Einzelstab_ist"))
                                            && reader.IsDBNull(reader.GetOrdinal("Eckverbindung_ist"))
                                            && reader.IsDBNull(reader.GetOrdinal("Oberflaeche_ist"))
                                            && reader.IsDBNull(reader.GetOrdinal("Endmontage_ist")))
                                        {
                                            bereich = new_Bereich;
                                            Rck = true;
                                            RckGrund = "Einzelstab:" + reader.GetString("Grund");
                                            Datum = reader.GetDateTime("Datum");
                                        }
                                        break;
                                }
                                break;
                            #endregion
                            #region "Eckverbindung"
                            case Fertigungsbereich.Eckverbindung:
                                switch (bereich)
                                {
                                    case Fertigungsbereich.None:
                                    case Fertigungsbereich.Oberflaeche:
                                    case Fertigungsbereich.Endmontage_Holz:
                                    case Fertigungsbereich.Endmontage_NGWA:
                                        if (reader.IsDBNull(reader.GetOrdinal("Eckverbindung_ist"))
                                            && reader.IsDBNull(reader.GetOrdinal("Oberflaeche_ist"))
                                            && reader.IsDBNull(reader.GetOrdinal("Endmontage_ist")))
                                        {
                                            bereich = new_Bereich;
                                            Rck = true;
                                            RckGrund = "Eckverbindung:" + reader.GetString("Grund");
                                            Datum = reader.GetDateTime("Datum");
                                        }
                                        break;
                                }
                                break;
                            #endregion
                            #region "Oberflaeche"
                            case Fertigungsbereich.Oberflaeche:
                                switch (bereich)
                                {
                                    case Fertigungsbereich.None:
                                    case Fertigungsbereich.Endmontage_Holz:
                                    case Fertigungsbereich.Endmontage_NGWA:
                                        if (reader.IsDBNull(reader.GetOrdinal("Oberflaeche_ist"))
                                            && reader.IsDBNull(reader.GetOrdinal("Endmontage_ist")))
                                        {
                                            bereich = new_Bereich;
                                            Rck = true;
                                            RckGrund = "Oberfläche:" + reader.GetString("Grund");
                                            Datum = reader.GetDateTime("Datum");
                                        }
                                        break;
                                }
                                break;
                            #endregion
                            #region "Endmontage"
                            case Fertigungsbereich.Endmontage_NGWA:
                            case Fertigungsbereich.Endmontage_Holz:
                                switch (bereich)
                                {
                                    case Fertigungsbereich.None:
                                        if (reader.IsDBNull(reader.GetOrdinal("Endmontage_ist")))
                                        {
                                            bereich = new_Bereich;
                                            Rck = true;
                                            RckGrund = "Endmontage:" + reader.GetString("Grund");
                                            Datum = reader.GetDateTime("Datum");
                                        }
                                        break;
                                }
                                break;
                                #endregion
                        }
                    }
            }
        }


        public static List<string> GetZeichnungPaths(string aunr)
        {
            var zeichnungen = new List<string>();
            try
            {

                if (!string.IsNullOrEmpty(aunr))
                {
                    var PPS_AuNr = Wertbau.AuftragMySqlToPPS(aunr, aunr.Length == "18009676".Length ? false : true);
                    if (int.TryParse(PPS_AuNr, out int PPS_AuNr_int))
                    {
                        using (var cmd = new SqlCommand(
                            @"  SELECT EF.FilePath 
                        FROM PAF  
                        LEFT JOIN ExternalFiles  AS EF
                         ON PAF.RowId = EF.DocId 
                        WHERE  (PAF.Numero = @Numero) 
                        GROUP BY EF.FilePath;"))
                        {
                            cmd.Parameters.Add("@Numero", SqlDbType.Int).Value = PPS_AuNr_int;
                            using (var conn = new SqlConnection(Konstanten.EKAWertbauConnectionString))
                            {
                                conn.Open();
                                cmd.Connection = conn;
                                using (var reader = cmd.ExecuteReader())
                                    while (reader.Read())
                                    {
                                        if (!reader.IsDBNull(0))
                                            zeichnungen.Add(reader.GetString(0));
                                    }
                            }
                            using (var conn = new SqlConnection(Konstanten.V200ConnectionString))
                            {
                                conn.Open();
                                cmd.Connection = conn;
                                using (var reader = cmd.ExecuteReader())
                                    while (reader.Read())
                                    {
                                        if (!reader.IsDBNull(0))
                                            zeichnungen.Add(reader.GetString(0));
                                    }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
            }

            zeichnungen.Sort();

            return zeichnungen;
        }

        public static DateTime DateTimeVonReader(MySqlDataReader reader, int column)
        {
            DateTime returning = DateTime.Parse("01.01.1901");

            if (!reader.IsDBNull(column))
            {
                try
                {
                    returning = reader.GetDateTime(column);
                }
                catch (Exception)
                {
                    Debug.WriteLine("Datum konnte nicht validiert werden!");
                    throw new Exception("Datum konnte nicht validiert werden!");
                }
            }

            return returning;
        }

        public static DateTime DateTimeVonReader(SqlDataReader reader, int column)
        {
            DateTime returning = DateTime.Parse("01.01.1901");

            if (!reader.IsDBNull(column))
            {
                try
                {
                    returning = reader.GetDateTime(column);
                }
                catch (Exception)
                {
                    Debug.WriteLine("Datum konnte nicht validiert werden!");
                    throw new Exception("Datum konnte nicht validiert werden!");
                }
            }

            return returning;
        }

        public static void AktiviereDoubleBuffer(DataGridView dgv)
        {
            //Set Double buffering on the Grid using reflection and the bindingflags enum.
            typeof(DataGridView).InvokeMember("DoubleBuffered", BindingFlags.NonPublic |
            BindingFlags.Instance | BindingFlags.SetProperty, null,
            dgv, new object[] { true });
        }

        public static void DeaktiviereSortierungDGV(DataGridView dgv)
        {
            foreach (DataGridViewColumn column in dgv.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        public static void ThreadReadyInvoke(Thread Thr)
        {
            if (Konstanten.MaxTH > 0)
            {
                Konstanten.CountTH++;

                var val = Konstanten.CountTH * 100 / Konstanten.MaxTH;
                if (val > 100)
                    val = 100;

                /*if (progressbarForm != null)
                    progressbarForm.WBProgressbar.Value = val;*/

                //System.Diagnostics.Debug.WriteLine(@"Threads: {0} / {1}", CountTH,MaxTH)

            }
            Konstanten.ListTH.Remove(Thr);
        }

        #region "HoleAuftragsdaten"


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapnr">MySQL SAPNR</param>
        /// <returns></returns>
        public static string HoleMySQLAuNrFromSAPNR(string sapnr)
        {
            if (!sapnr.Contains("."))
                return sapnr;

            string aunr = "";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                using (var cmd = new MySqlCommand("SELECT Auftragsnr FROM Auftrag WHERE sapnr like '%" + sapnr + "%' limit 1", conn))
                {
                    using (var reader = cmd.ExecuteReader())
                        if (reader.Read())
                        {
                            aunr = reader["Auftragsnr"].ToString();
                        }
                }
            }

            return aunr;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Aunr">MySQL SAPNR</param>
        /// <returns></returns>
        public static string HoleMySQLSapnrFromAunr(string Aunr)
        {
            string aunr = "";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                using (var cmd = new MySqlCommand("SELECT Sapnr FROM Auftrag WHERE auftragsnr like '%" + Aunr + "%' limit 1", conn))
                {
                    using (var reader = cmd.ExecuteReader())
                        if (reader.Read())
                        {
                            aunr = reader["Sapnr"].ToString();
                        }
                }
            }

            return aunr;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aunr">Direkte PPSAuftragsnummer</param>
        /// <returns></returns>
        public static string HoleKennzeichen(string aunr)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return "";
            else
            {
                string sqlstr = "SELECT KZ from Auftrag WHERE AUNR = '" + aunr + "'";

                using (var conn = new SqlConnection(Konstanten.PpsDatenConnectionstring))
                {
                    conn.Open();

                    using (var comm = new SqlCommand(sqlstr, conn))
                    {
                        var ret = comm.ExecuteScalar();

                        if (ret == null || ret is DBNull)
                            return "";
                        else
                            return ret.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aunr">MySqlAuftragsnummer</param>
        /// <returns></returns>
        public static string HoleKennzeichenMySQL(string aunr)
        {
            if (string.IsNullOrEmpty(aunr))
                return string.Empty;
            else
            {
                using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand("SELECT AuS.`Kennzeichen` FROM `auftragsstatus` AS AuS WHERE AuS.`Auftragsnummer` LIKE CONCAT(@AuNr, '%')", conn))
                    {
                        cmd.Parameters.Add("@AuNr", MySqlDbType.VarString).Value = aunr;
                        using (var reader = cmd.ExecuteReader())
                            if (reader.Read())
                                return reader.GetString("Kennzeichen");
                            else
                                return string.Empty;
                    }
                }
            }
        }

        #region "HoleAufträge"

        public static IList<string> HoleAuftraegeSollProfilierung(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Profilierung_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND `Kennzeichen` IN ('NGWA','HO','HA')";
            if (FilterFertig)
                sql += " AND `Profilierung_ist` IS NULL";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        public static IList<string> HoleAuftraegeSollEinzelstab(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Einzelstab_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND `Kennzeichen` IN ('NGWA','HO','HA')";
            if (FilterFertig)
                sql += " AND `Einzelstab_ist` IS NULL";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        public static IList<string> HoleAuftraegeSollEckverbindung(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Eckverbindung_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND `Kennzeichen` IN ('NGWA','HO','HA')";
            if (FilterFertig)
                sql += " AND `Eckverbindung_ist` IS NULL";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        public static IList<string> HoleAuftraegeSollOberflaeche(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Oberflaeche_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND `Kennzeichen` IN ('NGWA','HO','HA')";
            if (FilterFertig)
                sql += " AND `Oberflaeche_ist` IS NULL";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        public static IList<string> HoleAuftraegeSollAluschale(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql;
            if (FilterFertig)
                sql = @"SELECT `Auftragsnummer`
                    FROM `auftragsstatus` AS AuS 
                    JOIN `auftrag` AS Au
                      ON au.`Id` = AuS.`auftrag_Id` 
                    JOIN `position` AS pos  
                      ON pos.`OrderNr` = au.`AuftragsNr` 	
                    WHERE DATE(`Oberflaeche_soll`) BETWEEN @von AND @bis 
	                    AND `Loeschkennzeichen` = FALSE 
                        AND `Kennzeichen` IN ('NGWA','HA') 
 	                    AND (pos.`AluFluegelFertig` < 2 OR pos.`AluRahmenFertig`< 2)
                    GROUP BY au.`Id`";
            else
                sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Oberflaeche_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND `Kennzeichen` IN ('NGWA','HA')";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        public static IList<string> HoleAuftraegeSollEndNGWA(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Endmontage_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND `Kennzeichen` IN ('NGWA','HA')";
            if (FilterFertig)
                sql += " AND `Endmontage_ist` IS NULL";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        public static IList<string> HoleAuftraegeSollEndHolz(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Endmontage_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND `Kennzeichen` IN ('HO')";
            if (FilterFertig)
                sql += " AND `Endmontage_ist` IS NULL";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        public static IList<string> HoleAuftraegeSollHST(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Endmontage_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND `HST` = TRUE";
            if (FilterFertig)
                sql += " AND `Endmontage_ist` IS NULL";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        #endregion

        #region "HoleDatum"

        #region "HoleAnschlagDatum"
        /// <summary>
        /// Lädt das Anschlagsdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <returns></returns>
        public static string HoleAnschlagDatum(string aunr)
        {
            return HoleAnschlagDatum(aunr, true);
        }
        /// <summary>
        /// Lädt das Anschlagsdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static string HoleAnschlagDatum(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return "";
            else
            {
                var dat = HoleAnschlagDatumDT(aunr, SollDatum);
                if (dat.HasValue)
                    return dat.Value.ToString();
                else
                    return "";
            }
        }
        /// <summary>
        /// Lädt das Anschlagsdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <returns></returns>
        public static DateTime? HoleAnschlagDatumDT(string aunr)
        {
            return HoleAnschlagDatumDT(aunr, true);
        }
        /// <summary>
        /// Lädt das Anschlagsdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static DateTime? HoleAnschlagDatumDT(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return null;
            else
            {
                using (var conn = new SqlConnection(Konstanten.PpsDatenConnectionstring))
                {
                    conn.Open();

                    string sqlstr;
                    if (SollDatum)
                        sqlstr = "SELECT ansdatFertigmeldung from Auftrag WHERE AUNR = '" + aunr + "'";
                    else
                        sqlstr = "SELECT ansdat from Auftrag WHERE AUNR = '" + aunr + "'";

                    using (var comm = new SqlCommand(sqlstr, conn))
                    {
                        var ret = comm.ExecuteScalar();

                        if (ret == null || ret is DBNull)
                            return null;
                        else
                            return (DateTime)ret;
                    }
                }
            }
        }

        /// <summary>
        /// Lädt das Anschlagsdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <returns></returns>
        public static string HoleAnschlagDatumMySql(string aunr)
        {
            return HoleAnschlagDatumMySql(aunr, true);
        }
        /// <summary>
        /// Lädt das Anschlagsdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static string HoleAnschlagDatumMySql(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return "";
            else
            {
                var dat = HoleAnschlagDatumDTMySql(aunr, SollDatum);
                if (dat.HasValue)
                    return dat.Value.ToString();
                else
                    return "";
            }
        }
        /// <summary>
        /// Lädt das Anschlagsdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <returns></returns>
        public static DateTime? HoleAnschlagDatumDTMySql(string aunr)
        {
            return HoleAnschlagDatumDTMySql(aunr, true);
        }
        /// <summary>
        /// Lädt das Anschlagsdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static DateTime? HoleAnschlagDatumDTMySql(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return null;
            else
            {
                using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
                {
                    conn.Open();

                    string sqlstr;
                    if (SollDatum)
                        sqlstr = "SELECT `Endmontage_soll` FROM `auftragsstatus` WHERE `Auftragsnummer` = '" + aunr + "'";
                    else
                        sqlstr = "SELECT `Endmontage_start` FROM `auftragsstatus` WHERE `Auftragsnummer` = '" + aunr + "'";

                    using (var comm = new MySqlCommand(sqlstr, conn))
                    {
                        var ret = comm.ExecuteScalar();

                        if (ret == null || ret is DBNull)
                            return null;
                        else
                            return (DateTime)ret;
                    }
                }
            }
        }
        #endregion

        #region "HoleProfilierDatum"
        /// <summary>
        /// Lädt das Profilierdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <returns></returns>
        public static string HoleProfilierDatum(string aunr)
        {
            return HoleProfilierDatum(aunr, true);
        }
        /// <summary>
        /// Lädt das Profilierdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static string HoleProfilierDatum(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return "";
            else
            {
                var dat = HoleProfilierDatumDT(aunr, SollDatum);
                if (dat.HasValue)
                    return dat.Value.ToString();
                else
                    return "";
            }
        }

        /// <summary>
        /// Lädt das Profilierdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <returns></returns>
        public static DateTime? HoleProfilierDatumDT(string aunr)
        {
            return HoleProfilierDatumDT(aunr, true);
        }
        /// <summary>
        /// Lädt das Profilierdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static DateTime? HoleProfilierDatumDT(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return null;
            else
            {
                using (var PPSconn = new SqlConnection(Konstanten.PpsDatenConnectionstring))
                {
                    PPSconn.Open();

                    string sqlstr;
                    if (SollDatum)
                        sqlstr = "SELECT anldatFertigmeldung from Auftrag WHERE AUNR = '" + aunr + "'";
                    else
                        sqlstr = "SELECT anldat from Auftrag WHERE AUNR = '" + aunr + "'";
                    using (var comm = new SqlCommand(sqlstr, PPSconn))
                    {
                        var ret = comm.ExecuteScalar();

                        if (ret == null || ret is DBNull)
                            return null;
                        else
                            return (DateTime)ret;
                    }
                }
            }
        }

        /// <summary>
        /// Lädt das Profilierdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <returns></returns>
        public static string HoleProfilierDatumMySql(string aunr)
        {
            return HoleProfilierDatumMySql(aunr, true);
        }
        /// <summary>
        /// Lädt das Profilierdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static string HoleProfilierDatumMySql(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return "";
            else
            {
                var dat = HoleProfilierDatumDTMySql(aunr, SollDatum);
                if (dat.HasValue)
                    return dat.Value.ToString();
                else
                    return "";
            }
        }

        /// <summary>
        /// Lädt das Profilierdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <returns></returns>
        public static DateTime? HoleProfilierDatumDTMySql(string aunr)
        {
            return HoleProfilierDatumDTMySql(aunr, true);
        }
        /// <summary>
        /// Lädt das Profilierdatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static DateTime? HoleProfilierDatumDTMySql(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return null;
            else
            {
                using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
                {
                    conn.Open();

                    string sqlstr;
                    if (SollDatum)
                        sqlstr = " SELECT `Profilierung_soll` FROM `auftragsstatus` WHERE `Auftragsnummer` = '" + aunr + "'";
                    else
                        sqlstr = " SELECT `Profilierung_start` FROM `auftragsstatus` WHERE `Auftragsnummer` = '" + aunr + "'";
                    using (var comm = new MySqlCommand(sqlstr, conn))
                    {
                        var ret = comm.ExecuteScalar();

                        if (ret == null || ret is DBNull)
                            return null;
                        else
                            return (DateTime)ret;
                    }
                }
            }
        }
        #endregion

        #region "HoleEinzelstabDatum"
        ///// <summary>
        ///// Lädt das EinzelstabDatum zu einem Auftrag
        ///// </summary>
        ///// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        ///// <returns></returns>
        //public static string HoleEinzelstabDatum(string aunr)
        //{
        //    return HoleEinzelstabDatum(aunr, true);
        //}
        ///// <summary>
        ///// Lädt das EinzelstabDatum zu einem Auftrag
        ///// </summary>
        ///// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        ///// <param name="SollDatum"></param>
        ///// <returns></returns>
        //public static string HoleEinzelstabDatum(string aunr, bool SollDatum)
        //{
        //    if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
        //        return "";
        //    else
        //    {
        //        var dat = HoleEinzelstabDatumDT(aunr, SollDatum);
        //        if (dat.HasValue)
        //            return dat.Value.ToString();
        //        else
        //            return "";
        //    }
        //}

        ///// <summary>
        ///// Lädt das EinzelstabDatum zu einem Auftrag
        ///// </summary>
        ///// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        ///// <returns></returns>
        //public static DateTime? HoleEinzelstabDatumDT(string aunr)
        //{
        //    return HoleEinzelstabDatumDT(aunr, true);
        //}
        ///// <summary>
        ///// Lädt das EinzelstabDatum zu einem Auftrag
        ///// </summary>
        ///// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        ///// <param name="SollDatum"></param>
        ///// <returns></returns>
        //public static DateTime? HoleEinzelstabDatumDT(string aunr, bool SollDatum)
        //{
        //    if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
        //        return null;
        //    else
        //    {
        //        using (var PPSconn = new SqlConnection(Konstanten.PpsDatenConnectionstring))
        //        {
        //            PPSconn.Open();

        //            string sqlstr;
        //            if (SollDatum)
        //                sqlstr = "SELECT anldatFertigmeldung from Auftrag WHERE AUNR = '" + aunr + "'";
        //            else
        //                sqlstr = "SELECT anldat from Auftrag WHERE AUNR = '" + aunr + "'";
        //            using (var comm = new SqlCommand(sqlstr, PPSconn))
        //            {
        //                var ret = comm.ExecuteScalar();

        //                if (ret == null || ret is DBNull)
        //                    return null;
        //                else
        //                    return (DateTime)ret;
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Lädt das EinzelstabDatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <returns></returns>
        public static string HoleEinzelstabDatumMySql(string aunr)
        {
            return HoleEinzelstabDatumMySql(aunr, true);
        }
        /// <summary>
        /// Lädt das EinzelstabDatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static string HoleEinzelstabDatumMySql(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return "";
            else
            {
                var dat = HoleEinzelstabDatumDTMySql(aunr, SollDatum);
                if (dat.HasValue)
                    return dat.Value.ToString();
                else
                    return "";
            }
        }

        /// <summary>
        /// Lädt das EinzelstabDatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <returns></returns>
        public static DateTime? HoleEinzelstabDatumDTMySql(string aunr)
        {
            return HoleEinzelstabDatumDTMySql(aunr, true);
        }
        /// <summary>
        /// Lädt das EinzelstabDatum zu einem Auftrag
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static DateTime? HoleEinzelstabDatumDTMySql(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return null;
            else
            {
                using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
                {
                    conn.Open();

                    string sqlstr;
                    if (SollDatum)
                        sqlstr = " SELECT `Einzelstab_soll` FROM `auftragsstatus` WHERE `Auftragsnummer` = '" + aunr + "'";
                    else
                        sqlstr = " SELECT `Einzelstab_start` FROM `auftragsstatus` WHERE `Auftragsnummer` = '" + aunr + "'";
                    using (var comm = new MySqlCommand(sqlstr, conn))
                    {
                        var ret = comm.ExecuteScalar();

                        if (ret == null || ret is DBNull)
                            return null;
                        else
                            return (DateTime)ret;
                    }
                }
            }
        }
        #endregion

        #region "HoleEckverbundDatum"
        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <returns></returns>
        public static string HoleEckverbundDatum(string aunr)
        {
            return HoleEckverbundDatum(aunr, true);
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static string HoleEckverbundDatum(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return "";
            else
            {
                var dat = HoleEckverbundDatumDT(aunr, SollDatum);
                if (dat.HasValue)
                    return dat.Value.ToString();
                else
                    return "";
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <returns></returns>
        public static DateTime? HoleEckverbundDatumDT(string aunr)
        {
            return HoleEckverbundDatumDT(aunr, true);
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static DateTime? HoleEckverbundDatumDT(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return null;
            else
            {
                using (var conn = new SqlConnection(Konstanten.PpsDatenConnectionstring))
                {
                    conn.Open();

                    string sqlstr;
                    if (SollDatum)
                        sqlstr = "SELECT verdatFertigmeldung from Auftrag WHERE AUNR = '" + aunr + "'";
                    else
                        sqlstr = "SELECT verdat from Auftrag WHERE AUNR = '" + aunr + "'";

                    using (var comm = new SqlCommand(sqlstr, conn))
                    {
                        var ret = comm.ExecuteScalar();

                        if (ret == null || ret is DBNull)
                            return null;
                        else
                            return (DateTime)ret;
                    }
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <returns></returns>
        public static string HoleEckverbundDatumMySql(string aunr)
        {
            return HoleEckverbundDatumMySql(aunr, true);
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static string HoleEckverbundDatumMySql(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return "";
            else
            {
                var dat = HoleEckverbundDatumDTMySql(aunr, SollDatum);
                if (dat.HasValue)
                    return dat.Value.ToString();
                else
                    return "";
            }
        }



        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <returns></returns>
        public static DateTime? HoleEckverbundDatumDTMySql(string aunr)
        {
            return HoleEckverbundDatumDTMySql(aunr, true);
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static DateTime? HoleEckverbundDatumDTMySql(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return null;
            else
            {
                using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
                {
                    conn.Open();

                    string sqlstr;
                    if (SollDatum)
                        sqlstr = "SELECT `Eckverbindung_soll` FROM `auftragsstatus` WHERE `Auftragsnummer` = '" + aunr + "'";
                    else
                        sqlstr = "SELECT `Eckverbindung_start` FROM `auftragsstatus` WHERE `Auftragsnummer` = '" + aunr + "'";

                    using (var comm = new MySqlCommand(sqlstr, conn))
                    {
                        var ret = comm.ExecuteScalar();

                        if (ret == null || ret is DBNull)
                            return null;
                        else
                            return (DateTime)ret;
                    }
                }
            }
        }
        #endregion

        #region "HoleOberflaecheDatum"
        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <returns></returns>
        public static string HoleOberflaecheDatum(string aunr)
        {
            return HoleOberflaecheDatum(aunr, true);
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static string HoleOberflaecheDatum(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return "";
            else
            {
                var dat = HoleOberflaecheDatumDT(aunr, SollDatum);
                if (dat.HasValue)
                    return dat.Value.ToString();
                else
                    return "";
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <returns></returns>
        public static DateTime? HoleOberflaecheDatumDT(string aunr)
        {
            return HoleOberflaecheDatumDT(aunr, true);
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> Direkte PPSAuftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static DateTime? HoleOberflaecheDatumDT(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return null;
            else
            {
                using (var conn = new SqlConnection(Konstanten.PpsDatenConnectionstring))
                {
                    conn.Open();

                    string sqlstr;
                    if (SollDatum)
                        sqlstr = "SELECT fludatFertigmeldung from Auftrag WHERE AUNR = '" + aunr + "'";
                    else
                        sqlstr = "SELECT fludat from Auftrag WHERE AUNR = '" + aunr + "'";

                    using (var comm = new SqlCommand(sqlstr, conn))
                    {
                        var ret = comm.ExecuteScalar();

                        if (ret == null || ret is DBNull)
                            return null;
                        else
                            return (DateTime)ret;
                    }
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <returns></returns>
        public static string HoleOberflaecheDatumMySql(string aunr)
        {
            return HoleOberflaecheDatumMySql(aunr, true);
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static string HoleOberflaecheDatumMySql(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return "";
            else
            {
                var dat = HoleOberflaecheDatumDTMySql(aunr, SollDatum);
                if (dat.HasValue)
                    return dat.Value.ToString();
                else
                    return "";
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <returns></returns>
        public static DateTime? HoleOberflaecheDatumDTMySql(string aunr)
        {
            return HoleOberflaecheDatumDTMySql(aunr, true);
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="aunr"> MySql-Auftragsnummer</param>
        /// <param name="SollDatum"></param>
        /// <returns></returns>
        public static DateTime? HoleOberflaecheDatumDTMySql(string aunr, bool SollDatum)
        {
            if (string.IsNullOrEmpty(aunr) || aunr.Length < 2)
                return null;
            else
            {
                using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
                {
                    conn.Open();

                    string sqlstr;
                    if (SollDatum)
                        sqlstr = "SELECT `Oberflaeche_soll` FROM `auftragsstatus` WHERE `Auftragsnummer` = '" + aunr + "'";
                    else
                        sqlstr = "SELECT `Oberflaeche_start` FROM `auftragsstatus` WHERE `Auftragsnummer` = '" + aunr + "'";

                    using (var comm = new MySqlCommand(sqlstr, conn))
                    {
                        var ret = comm.ExecuteScalar();

                        if (ret == null || ret is DBNull)
                            return null;
                        else
                            return (DateTime)ret;
                    }
                }
            }
        }
        #endregion

        #endregion

        #endregion

        #region "FramewoodStatus"
        public class Cla_FramewoodStatus
        {
            public int ID { get; set; }
            public string FrameID { get; set; }
            public int Station_ID { get; set; }
            public int Gutteil { get; set; }
            public DateTime Von { get; set; }
            public DateTime Bis { get; set; }
            public string WagenNr { get; set; }
            public int Status { get; set; }
            public int StationStatus { get; set; }
            public int StationsNr { get; set; }
            public int StationsGruppe { get; set; }
        }

        public class Cla_FramewoodStatus_by_Frame
        {
            public string FrameID { get; set; }
            public IList<Cla_FramewoodStatus> Status { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="AuNr">MySQL-AuftragsNr (NICHT SAPNR)</param>
        /// <param name="Position"></param>
        /// <param name="Instanz"></param>
        /// <param name="StationID"></param>
        /// <returns></returns>
        public static IList<Cla_FramewoodStatus_by_Frame> HoleFramewoodStatusByPositionStation(string AuNr, int Position, int Instanz, int StationID)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();
                return HoleFramewoodStatusByPositionStation(AuNr, Position, Instanz, StationID, conn);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="AuNr">MySQL-AuftragsNr (NICHT SAPNR)</param>
        /// <param name="Position"></param>
        /// <param name="Instanz"></param>
        /// <param name="StationID"></param>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static IList<Cla_FramewoodStatus_by_Frame> HoleFramewoodStatusByPositionStation(string AuNr, int Position, int Instanz, int StationID, MySqlConnection conn)
        {
            List<Cla_FramewoodStatus_by_Frame> ret = new List<Cla_FramewoodStatus_by_Frame>();
            List<Cla_FramewoodStatus> statuses = null;
            Cla_FramewoodStatus_by_Frame prev_status = null;
            using (var cmd = new MySqlCommand(
                @"  SELECT fws.`Id`,fw.`FrameId` AS Framewood_FrameId,fws.`Station_Id`,fws.`Gutteil`,fws.`Von`,fws.`Bis`,fws.`WagenNr`,fws.`Status`,fws.`StationsStatus`,fws.`StationsNR`,fws.`StationGruppe` 		
                    FROM `framewood` AS fw
                    JOIN `position` AS pos
	                    ON pos.`Id` = fw.`Position_Id`
                    LEFT JOIN `framewoodstatus` AS fws
	                    ON fws.`Station_Id` = @Station_Id
                        AND fw.`FrameId` = fws.`Framewood_FrameId`
                    WHERE pos.`OrderNr` =@AuNr  
                      AND pos.`OrderPosNr` = @Pos 
                      AND pos.`Instanz` = @Inst
                    ORDER BY fw.`FrameId`;", conn))
            {
                cmd.Parameters.Add("@Station_Id", MySqlDbType.Int32).Value = StationID;
                cmd.Parameters.Add("@AuNr", MySqlDbType.VarString).Value = AuNr;
                cmd.Parameters.Add("@Pos", MySqlDbType.Int32).Value = Position;
                cmd.Parameters.Add("@Inst", MySqlDbType.Int32).Value = Instanz;
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        if (prev_status == null || prev_status.FrameID != reader.GetString("Framewood_FrameId"))
                        {
                            statuses = new List<Cla_FramewoodStatus>();
                            prev_status = new Cla_FramewoodStatus_by_Frame()
                            {
                                FrameID = reader.GetString("Framewood_FrameId"),
                                Status = statuses
                            };
                            ret.Add(prev_status);
                        }
                        if (!reader.IsDBNull(0))
                        {
                            statuses.Add(new Cla_FramewoodStatus()
                            {
                                ID = reader.GetInt32("Id"),
                                FrameID = reader.GetString("Framewood_FrameId"),
                                Station_ID = reader.GetInt32("Station_Id"),
                                Gutteil = reader.GetInt32("Gutteil"),
                                Von = reader.GetDateTime("Von"),
                                Bis = reader.GetDateTime("Bis"),
                                WagenNr = reader.GetString("WagenNr"),
                                Status = reader.GetInt32("Status"),
                                StationStatus = reader.GetInt32("StationsStatus"),
                                StationsNr = reader.GetInt32("StationsNR"),
                                StationsGruppe = reader.GetInt32("StationGruppe")
                            });
                        }
                    }
            }
            return ret;
        }

        public static IList<Cla_FramewoodStatus> HoleFramewoodStatusByStationFrame(int Station_ID, string FrameID)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();
                return HoleFramewoodStatusByStationFrame(Station_ID, FrameID, conn);
            }
        }
        public static IList<Cla_FramewoodStatus> HoleFramewoodStatusByStationFrame(int Station_ID, string FrameID, MySqlConnection conn)
        {
            List<Cla_FramewoodStatus> ret = new List<Cla_FramewoodStatus>();
            using (var cmd = new MySqlCommand(
                @"  SELECT `Id`,`Framewood_FrameId`,`Station_Id`,`Gutteil`,`Von`,`Bis`,`WagenNr`,`Status`,`StationsStatus`,`StationsNR`,`StationGruppe` 
                    FROM `framewoodstatus` 
                    WHERE `Station_Id` = @Station_ID 
                      AND `Framewood_FrameId` = @FrameID;", conn))
            {
                cmd.Parameters.Add("@Station_ID", MySqlDbType.Int32).Value = Station_ID;
                cmd.Parameters.Add("@FrameID", MySqlDbType.VarString).Value = FrameID;
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        ret.Add(new Cla_FramewoodStatus()
                        {
                            ID = reader.GetInt32("Id"),
                            FrameID = reader.GetString("Framewood_FrameId"),
                            Station_ID = reader.GetInt32("Station_Id"),
                            Gutteil = reader.GetInt32("Gutteil"),
                            Von = reader.GetDateTime("Von"),
                            Bis = reader.GetDateTime("Bis"),
                            WagenNr = reader.GetString("WagenNr"),
                            Status = reader.GetInt32("Status"),
                            StationStatus = reader.GetInt32("StationsStatus"),
                            StationsNr = reader.GetInt32("StationsNR"),
                            StationsGruppe = reader.GetInt32("StationGruppe")
                        });
                    }
            }
            return ret;
        }

        public static void InsertUpdateFramewoodStatusByStationFrame(int Station_ID, string FrameID, int Gutteil)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();
                InsertUpdate_Framewood_Status_by_Station_Frame(Station_ID, FrameID, Gutteil, conn);
            }
        }
        public static void InsertUpdate_Framewood_Status_by_Station_Frame(int Station_ID, string FrameID, int Gutteil, MySqlConnection conn)
        {

            var ele = HoleFramewoodStatusByStationFrame(Station_ID, FrameID, conn);

            if (ele.Count < 1)
            {
                int StationsNR = -1;
                int StationGruppe = -1;
                using (var cmd = new MySqlCommand("SELECT `StationNr`, `StationGruppe` FROM `station` WHERE `Id` = @S_ID;", conn))
                {
                    cmd.Parameters.Add("@S_ID", MySqlDbType.Int32).Value = Station_ID;
                    using (var reader = cmd.ExecuteReader())
                        if (reader.Read())
                        {
                            StationsNR = reader.GetInt32("StationNr");
                            StationGruppe = reader.GetInt32("StationGruppe");
                        }
                }

                using (var cmd = new MySqlCommand(
                    @"INSERT INTO `framewoodstatus`(`Framewood_FrameId`,`Station_Id`,`Gutteil`,`AngelName`,`ChangeName`,`StationsNR`,`StationGruppe`)
                     VALUES						   (@Framewood_FrameId, @Station_Id, @Gutteil, @Name,	  @Name,	   @StationsNR, @StationGruppe);", conn))
                {
                    cmd.Parameters.Add("@Framewood_FrameId", MySqlDbType.VarString).Value = FrameID;
                    cmd.Parameters.Add("@Station_Id", MySqlDbType.Int32).Value = Station_ID;
                    cmd.Parameters.Add("@Gutteil", MySqlDbType.Int32).Value = Gutteil;
                    cmd.Parameters.Add("@Name", MySqlDbType.VarString).Value = Environment.UserName;
                    cmd.Parameters.Add("@StationsNR", MySqlDbType.Int32).Value = StationsNR;
                    cmd.Parameters.Add("@StationGruppe", MySqlDbType.Int32).Value = StationGruppe;

                    cmd.ExecuteNonQuery();
                }
            }
            else
            {
                using (var cmd = new MySqlCommand(
                    @"UPDATE `framewoodstatus`
                    SET `Gutteil` = @Gutteil,
	                    `ChangeName` = @Name
                    WHERE `Id` = @Id;", conn))
                {
                    cmd.Parameters.Add("@Gutteil", MySqlDbType.Int32).Value = Gutteil;
                    cmd.Parameters.Add("@Name", MySqlDbType.VarString).Value = Environment.UserName;
                    var para_ID = cmd.Parameters.Add("@Id", MySqlDbType.Int32);
                    foreach (var el in ele)
                    {
                        para_ID.Value = el.ID;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }
        #endregion

        #region "StringBuilderExtension"
        public static StringBuilder TrimEnd(this StringBuilder sb)
        {
            if (sb == null || sb.Length == 0) return sb;

            int i;
            for (i = sb.Length - 1; i >= 0; i--)
                if (!char.IsWhiteSpace(sb[i]))
                    break;

            if (i < sb.Length - 1)
                sb.Length = i + 1;

            return sb;
        }

        public static StringBuilder TrimStart(this StringBuilder sb)
        {
            if (sb == null || sb.Length == 0) return sb;

            int i;
            for (i = 0; i < sb.Length; i++)
                if (!char.IsWhiteSpace(sb[i]))
                    break;

            if (i > 0)
                sb.Remove(0, i);

            return sb;
        }


        public static StringBuilder Trim(this StringBuilder sb)
        {
            return sb.TrimStart().TrimEnd();
        }
        #endregion

        #region "DatetimeExtension"
        public static int WeekOfYear(this DateTime Date)
        {
            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            return dfi.Calendar.GetWeekOfYear(Date, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
        }

        public static int WeekOfYear(this DateTime Date, CalendarWeekRule calendarWeekRule)
        {
            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            return dfi.Calendar.GetWeekOfYear(Date, calendarWeekRule, dfi.FirstDayOfWeek);
        }

        public static int WeekOfYear(this DateTime Date, CalendarWeekRule calendarWeekRule, DayOfWeek FirstDayOfWeek)
        {
            return DateTimeFormatInfo.CurrentInfo.Calendar.GetWeekOfYear(Date, calendarWeekRule, FirstDayOfWeek);
        }
        #endregion

    }
}
