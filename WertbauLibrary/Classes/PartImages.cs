﻿using System.Drawing;

namespace WertbauLibrary.Classes
{
    public class PartImages
    {
        public string PartId { get; set; }
        public Image Metafile { get; set; }
        public Image Horizontale { get; set; }
        public Image Verticale { get; set; }
        public Image Label { get; set; }
        public Image Schnittzeichnung { get; set; }
    }
}
