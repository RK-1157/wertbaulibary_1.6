﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WertbauLibrary.Types;

namespace WertbauLibrary.Classes
{
    public class ButtonpressedEventArgs : EventArgs
    {
        public ButtonPressed ButtonPressed { get; }

        public ButtonpressedEventArgs(ButtonPressed buttonPressed)
        {
            ButtonPressed = buttonPressed;
        }
    }
}
