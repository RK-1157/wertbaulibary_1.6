﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WertbauLibrary.Classes
{
    public class PositionSoll
    {
        public string AuNr { get; set; }
        public int Pos_ID { get; set; }
        public string Pos_PositionsId { get; set; }
        public int SollKanteln { get; set; }
        public decimal Sollm2 { get; set; }
        public int KantelnSchlecht { get; set; }
        public int KantelnRep { get; set; }
        public int KantelnIst { get; set; }
        public DateTime? FertigDatum { get; set; }
    }
}
