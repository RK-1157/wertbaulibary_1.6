﻿using System.Drawing;

namespace WertbauLibrary.Classes
{
    public static class Farben
    {
        public static Color Rot { get => Color.FromArgb(0xFF, 0x3F, 0x3F); }
        public static Color OckerGelb { get => Color.FromArgb(0xFD, 0xFF, 0xA2); }
        public static Color Orange { get => Color.FromArgb(0xFC, 0xB4, 0x47); }
        public static Color Gruen { get => Color.FromArgb(0x48, 0xD8, 0x4D); }
        public static Color Blau { get => Color.FromArgb(0x5D, 0x71, 0xF6); }

        public static Color NichtVorhanden { get => Status.NichtVorhanden.StatusFarbe(); }
        public static Color TeilweiseVorhanden { get => Status.TeilweiseVorhanden.StatusFarbe(); }
        public static Color BereitsBegonnen { get => Status.BereitsBegonnen.StatusFarbe(); }
        public static Color VollstaendigVorhanden { get => Status.VollstaendigVorhanden.StatusFarbe(); }

        public static Color SollMontag { get => Color.Yellow; }
        public static Color SollDienstag{ get => Color.CadetBlue; }
        public static Color SollMittwoch { get => Color.Orange; }
        public static Color SollDonnerstag { get => Color.Green; }
        public static Color SollFreitag { get => Color.Magenta; }

        public static Color Rueckstellen { get => Rot; }

        public enum Status
        {
            NichtVorhanden = 0,
            TeilweiseVorhanden,
            BereitsBegonnen,
            VollstaendigVorhanden,
            Nachfahrt
        }

        public static Image StatusImage(this Status status)
        {
            switch (status)
            {
                case Status.NichtVorhanden:
                    return Properties.Resources.red;
                case Status.TeilweiseVorhanden:
                    return Properties.Resources.blue;
                case Status.BereitsBegonnen:
                    return Properties.Resources.orange;
                case Status.VollstaendigVorhanden:
                    return Properties.Resources.green;
                case Status.Nachfahrt:
                    return Properties.Resources.orangeNF;
                default:
                    return Properties.Resources.violett;
            }
        }

        public static Color StatusFarbe(this Status status)
        {
            switch (status)
            {
                case Status.NichtVorhanden:
                    return Rot;
                case Status.TeilweiseVorhanden:
                    return Blau;
                case Status.BereitsBegonnen:
                    return Orange;
                case Status.VollstaendigVorhanden:
                    return Gruen;
                case Status.Nachfahrt:
                    return OckerGelb;
                default:
                    return Color.Transparent;
            }
        }
    }
}
