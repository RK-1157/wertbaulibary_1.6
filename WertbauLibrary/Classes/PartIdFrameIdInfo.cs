﻿namespace WertbauLibrary.Classes
{
    public class PartIdFrameIdInfo
    {
        public string AuftragsNummer { get; set; }
        public string VersionsNummer { get; set; }
        public string Bnummer { get; set; }
        public string Position { get; set; }
        public string Hoehle_IdQuadro { get; set; }
        public string MaterialId { get; set; }
        public string Instanz { get; set; }

        public string PositionID { get; set; }
        public string FrameID { get;set; }
        public string PartID { get; set; }
        public string PartNameShort { get; set; }
    }
}
