﻿namespace WertbauLibrary.Controls
{
    partial class KalenderObjekt
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.PanelView = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.JahresText = new System.Windows.Forms.Label();
            this.BtVor = new System.Windows.Forms.Button();
            this.BtZurück = new System.Windows.Forms.Button();
            this.TextLab = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.PanelView, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(400, 459);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // PanelView
            // 
            this.PanelView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelView.Location = new System.Drawing.Point(0, 40);
            this.PanelView.Margin = new System.Windows.Forms.Padding(0);
            this.PanelView.Name = "PanelView";
            this.PanelView.Size = new System.Drawing.Size(400, 419);
            this.PanelView.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.TextLab);
            this.panel1.Controls.Add(this.JahresText);
            this.panel1.Controls.Add(this.BtVor);
            this.panel1.Controls.Add(this.BtZurück);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 40);
            this.panel1.TabIndex = 0;
            // 
            // JahresText
            // 
            this.JahresText.AutoSize = true;
            this.JahresText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JahresText.Location = new System.Drawing.Point(169, 12);
            this.JahresText.Name = "JahresText";
            this.JahresText.Size = new System.Drawing.Size(127, 20);
            this.JahresText.TabIndex = 5;
            this.JahresText.Text = "Dezember, 2018";
            // 
            // BtVor
            // 
            this.BtVor.BackgroundImage = global::WertbauLibrary.Properties.Resources.Arrow_Right;
            this.BtVor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BtVor.Location = new System.Drawing.Point(363, 3);
            this.BtVor.Name = "BtVor";
            this.BtVor.Size = new System.Drawing.Size(34, 32);
            this.BtVor.TabIndex = 4;
            this.BtVor.UseVisualStyleBackColor = true;
            this.BtVor.Click += new System.EventHandler(this.BTVor);
            // 
            // BtZurück
            // 
            this.BtZurück.BackgroundImage = global::WertbauLibrary.Properties.Resources.Arrow_Left;
            this.BtZurück.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BtZurück.Location = new System.Drawing.Point(3, 5);
            this.BtZurück.Name = "BtZurück";
            this.BtZurück.Size = new System.Drawing.Size(34, 32);
            this.BtZurück.TabIndex = 3;
            this.BtZurück.UseVisualStyleBackColor = true;
            this.BtZurück.Click += new System.EventHandler(this.BTZurück);
            // 
            // TextLab
            // 
            this.TextLab.AutoSize = true;
            this.TextLab.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextLab.Location = new System.Drawing.Point(43, 12);
            this.TextLab.Name = "TextLab";
            this.TextLab.Size = new System.Drawing.Size(0, 20);
            this.TextLab.TabIndex = 6;
            // 
            // KalenderObjekt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "KalenderObjekt";
            this.Size = new System.Drawing.Size(400, 459);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel PanelView;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BtVor;
        private System.Windows.Forms.Button BtZurück;
        private System.Windows.Forms.Label JahresText;
        private System.Windows.Forms.Label TextLab;
    }
}
