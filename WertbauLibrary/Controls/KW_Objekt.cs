﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace WertbauLibrary.Controls
{
    public partial class KW_Objekt : UserControl
    {

        public delegate void KwSelected(object source, SelectedKWEventArgs e);
        public event KwSelected KWSelectedChanged; // Event schickt client an Hörer

        public int KW = 0;
        public int LastKW = 0;
        public bool KWSuche = false;
        public Button LastClickedBT;

        public List<Button> KWButtonListe = new List<Button>();
        public KW_Objekt()
        {
            InitializeComponent();
        }

        private void KW_Objekt_Load(object sender, EventArgs e)
        {
            KW = GetCalendarWeek(DateTime.Now);
            LastKW = KW;

            KWButtonListe.Add(Button1);
            KWButtonListe.Add(Button2);
            KWButtonListe.Add(Button3);
            KWButtonListe.Add(Button4);
            KWButtonListe.Add(Button5);
            KWButtonListe.Add(Button6);
            KWButtonListe.Add(Button7);
            KWButtonListe.Add(Button8);
            SetupKWButton();
        }

        // etwas gemehre... jedoch ganz sinnvoll meiner Überlegung nach #
        void SetupKWButton()
        {
            Button5.Text = KW.ToString();
            Button5.Name = "J" + DateTime.Now.Year;
            Button5.BackColor = Color.Orange;

            int last = GetCalendarWeek(DateTime.Now.AddDays(-7));
            Button4.Text = last.ToString();
            // Jahr zurück gehen ...
            if (last > KW)
                Button4.Name = "J" + (DateTime.Now.Year - 1);
            else
                Button4.Name = "J" + DateTime.Now.Year;

            last = GetCalendarWeek(DateTime.Now.AddDays(-14));
            Button3.Text = last.ToString();
            if (last > KW)
                Button3.Name = "J" + (DateTime.Now.Year - 1);
            else
                Button3.Name = "J" + DateTime.Now.Year;

            last = GetCalendarWeek(DateTime.Now.AddDays(-21));
            Button2.Text = last.ToString();
            if (last > KW)
                Button2.Name = "J" + (DateTime.Now.Year - 1);
            else
                Button2.Name = "J" + DateTime.Now.Year;

            last = GetCalendarWeek(DateTime.Now.AddDays(-28));
            Button1.Text = last.ToString();
            if (last > KW)
                Button1.Name = "J" + (DateTime.Now.Year - 1);
            else
                Button1.Name = "J" + DateTime.Now.Year;





            last = GetCalendarWeek(DateTime.Now.AddDays(7));
            Button6.Text = last.ToString();
            // Jahr zurück gehen ...
            if (last < KW)
                Button6.Name = "J" + (DateTime.Now.Year + 1);
            else
                Button6.Name = "J" + DateTime.Now.Year;


            last = GetCalendarWeek(DateTime.Now.AddDays(14));
            Button7.Text = last.ToString();
            // Jahr zurück gehen ...
            if (last < KW)
                Button7.Name = "J" + (DateTime.Now.Year + 1);
            else
                Button7.Name = "J" + DateTime.Now.Year;

            last = GetCalendarWeek(DateTime.Now.AddDays(21));
            Button8.Text = last.ToString();
            // Jahr zurück gehen ...
            if (last < KW)
                Button8.Name = "J" + (DateTime.Now.Year + 1);
            else
                Button8.Name = "J" + DateTime.Now.Year;
        }

        int GetCalendarWeek(DateTime date)
        {
            CultureInfo currentCulture = CultureInfo.CurrentCulture;

            Calendar calendar = currentCulture.Calendar;

            int calendarWeek = calendar.GetWeekOfYear(date,
               currentCulture.DateTimeFormat.CalendarWeekRule,
               currentCulture.DateTimeFormat.FirstDayOfWeek);

            if (calendarWeek > 52)
            {
                date = date.AddDays(7);
                int testCalendarWeek = calendar.GetWeekOfYear(date,
                   currentCulture.DateTimeFormat.CalendarWeekRule,
                   currentCulture.DateTimeFormat.FirstDayOfWeek);
                if (testCalendarWeek == 2)
                    calendarWeek = 1;
            }

            return calendarWeek;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Button bt = (Button)sender;

            // if (bt == LastClickedBT) return;


            DateTime dt = DateTime.Now;
            if (KWSuche) return;

            KWSuche = true;

            if (LastClickedBT != null)
            {
                if (bt.Text != KW.ToString())
                    bt.BackColor = Color.Green;
                if (LastClickedBT.Text != KW.ToString())
                    LastClickedBT.BackColor = SystemColors.Control;
            }
            else
            if (bt.Text != KW.ToString())
                bt.BackColor = Color.Green;

            LastClickedBT = bt;


            KWSelectedChanged?.Invoke(this, new SelectedKWEventArgs(int.Parse(bt.Text), int.Parse(bt.Name.Replace("J", ""))));

            KWSuche = false;
        }

        public class SelectedKWEventArgs : EventArgs
        {
            private int _Kw;
            private int _jahr;
            public SelectedKWEventArgs(int kw, int jahr)
            {
                _Kw = kw;
                _jahr = jahr;
            }
            public int GetKW()
            {
                return _Kw;
            }
            public int GetJahr()
            {
                return _jahr;
            }
        }

    }
}
