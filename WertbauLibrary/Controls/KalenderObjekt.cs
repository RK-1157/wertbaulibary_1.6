﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WertbauLibrary.Controls
{
    public partial class KalenderObjekt : UserControl
    {
        DateTime GesetztesDatum = DateTime.Now;

        public delegate void DatumChanged(object source, DatumChangedEvent e);
        public event DatumChanged DatumGeändert; // Event schickt client an Hörer

        public class DatumChangedEvent : EventArgs
        {
            private readonly DateTime dateF = DateTime.Now;
            public DatumChangedEvent(DateTime verb)
            {
                dateF = verb;
            }
            public DateTime GetKalender()
            {
                return dateF;
            }
        }

        public KalenderObjekt()
        {
            InitializeComponent();

            this.Dock = DockStyle.Fill;

            JahresText.Text = DateTime.Now.ToString("MMMM, yyyy");
            SetzeKalenderPanels();
        }

        private void BTZurück(object sender, EventArgs e)
        {
            GesetztesDatum = GesetztesDatum.AddMonths(-1);
            JahresText.Text = GesetztesDatum.ToString("MMMM, yyyy");

            SetzeKalenderPanels();
        }

        private void BTVor(object sender, EventArgs e)
        {
            GesetztesDatum = GesetztesDatum.AddMonths(1);
            JahresText.Text = GesetztesDatum.ToString("MMMM, yyyy");

            SetzeKalenderPanels();
        }

        private void SetzeKalenderPanels()
        {
            PanelView.Controls.Clear();

            int x = 5;
            int y = 5;

            for (int i = 1; i <= DateTime.DaysInMonth(GesetztesDatum.Year, GesetztesDatum.Month); i++)
            {
                Panel p = new Panel
                {
                    Width = 65,
                    Height = 65,
                    BackColor = Color.LightGray,
                    Name = "D" + i
                };
                p.Click += P_Click;

                if (GesetztesDatum.Month == DateTime.Now.Month && GesetztesDatum.Year == DateTime.Now.Year)
                {
                    if (i == DateTime.Now.Day)
                    {
                        p.BackColor = Color.CadetBlue; // derzeitiges dtum einfach anders darstellen damit man es besser erkennen kann.
                    }
                }

                Label lab = new Label
                {
                    Name = "D" + i
                };
                lab.Click += Lab_Click;
                lab.Text = i.ToString();                
                lab.Font = new Font("Arial", 12);
                lab.Location = new Point(10, 10);
                p.Controls.Add(lab);

                p.Location = new Point(x, y);

                PanelView.Controls.Add(p);

                x += 80;
                if (x > 350)
                {
                    x = 5;
                    y += 70;
                }

            }
        }

        private void P_Click(object sender, EventArgs e)
        {
            SetzeNewDatum(((Panel)sender).Name.Replace("D", ""));
        }

        private void Lab_Click(object sender, EventArgs e)
        {
            SetzeNewDatum(((Label)sender).Text.Replace("D", ""));
        }

        private void SetzeNewDatum(string tag)
        {
            DateTime newDate = DateTime.Parse(tag + "" + GesetztesDatum.ToString(".MM.yyyy"));
            GesetztesDatum = newDate;

            DatumGeändert?.Invoke(this, new DatumChangedEvent(GesetztesDatum));
        }

        public void SetzeText(string n)
        {
            TextLab.Text = n;
        }
    }
}
