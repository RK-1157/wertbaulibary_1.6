﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WertbauLibrary.Controls
{

    [DefaultEvent("CheckedChanged")]
    public partial class UC_SizeableCheckbox : UserControl
    {
        public UC_SizeableCheckbox()
        {
            InitializeComponent();
        }

        private bool _checked = false;
        private bool _radiobutton = false;

        public event EventHandler CheckedChanged;

        [DefaultValue(false)]
        public bool Checked
        {
            get => _checked;
            set
            {
                if (_checked != value)
                {
                    _checked = value;
                    this.Invalidate();
                    CheckedChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        [DefaultValue(false)]
        public bool Radiobutton
        {
            get { return _radiobutton; }
            set
            {
                if (_radiobutton != value)
                {
                    _radiobutton = value;
                    this.Invalidate();
                }
            }
        }

        private void UC_SizeableCheckbox_Paint(object sender, PaintEventArgs e)
        {
            ButtonState state = ButtonState.Normal;
            if (!this.Enabled)
                state |= ButtonState.Inactive;
            if (this.Checked)
                state |= ButtonState.Checked;
            //if (this.Focused)
            //    state |= ButtonState.Pushed;
            if (_radiobutton)
                ControlPaint.DrawRadioButton(e.Graphics, this.ClientRectangle, state);
            else
                ControlPaint.DrawCheckBox(e.Graphics, this.ClientRectangle, state);
           
        }

        private void UC_SizeableCheckbox_Click(object sender, EventArgs e)
        {
            this.Checked = !this.Checked;
        }
    }
}
