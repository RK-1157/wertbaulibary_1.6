﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WertbauLibrary.Controls
{
    public class UC_FileEnder:Timer
    {
        public UC_FileEnder():base()
        {
            Filename = "updateP5";
            Parent = null;
            this.Tick += UC_FileRestarter_Tick;
        }

        private void UC_FileRestarter_Tick(object sender, EventArgs e)
        {
            this.Stop();
            if(File.Exists(Filename) && Parent != null)
            {
                Parent.Close();
            }
            this.Start();
        }

        [DefaultValue("updateP5")]
        public string Filename { get; set; }

        [DefaultValue( typeof(Form),null)]
        public Form Parent { get; set; }

        
    }
}
