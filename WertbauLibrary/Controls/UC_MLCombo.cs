﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Design;

namespace TestFrom
{
    [DefaultEvent(nameof(SelectedIndexChanged))]
    public partial class UC_MLCombo : UserControl
    {
        public UC_MLCombo()
        {
            InitializeComponent();
            Text = "";
            FixedText = false;
            SF.Alignment = StringAlignment.Center;
            SF.LineAlignment = StringAlignment.Center;

            ButtonWidth = _ButtonWidth;
            ForeBrush = new SolidBrush(this.ForeColor);
        }

        protected Brush ForeBrush;

        public event EventHandler SelectedIndexChanged
        {
            add { cbo_Main.SelectedIndexChanged += value; }
            remove { cbo_Main.SelectedIndexChanged -= value; }
        }

        public override Color ForeColor
        {
            get => base.ForeColor;
            set
            {
                base.ForeColor = value;
                ForeBrush = new SolidBrush(value);
            }

        }

        private StringFormat SF = new StringFormat();

        [DefaultValue("")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Bindable(true)]
        public override string Text { get => base.Text; set => base.Text = value; }
        private int _ButtonWidth =30;
        [DefaultValue(30)]
        public int ButtonWidth
        {
            get => _ButtonWidth;
            set
            {
                 Pan_Btn.Width = _ButtonWidth = value;
                this.Invalidate(true);
            }
        }

        [DefaultValue(false)]
        public bool FixedText { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Editor("System.Windows.Forms.Design.ListControlStringCollectionEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [Localizable(true)]
        [MergableProperty(false)]
        public ComboBox.ObjectCollection Items { get => cbo_Main.Items; }

        [AttributeProvider(typeof(IListSource))]
        [DefaultValue(null)]
        [RefreshProperties(RefreshProperties.Repaint)]
        public object DataSource { get => cbo_Main.DataSource; set => cbo_Main.DataSource = value; }

        [DefaultValue("")]
        [Editor("System.Windows.Forms.Design.DataMemberFieldEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [TypeConverter("System.Windows.Forms.Design.DataMemberFieldConverter, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
        public string DisplayMember { get => cbo_Main.DisplayMember; set => cbo_Main.DisplayMember = value; }

        public int DropDownWidth { get => cbo_Main.DropDownWidth; set => cbo_Main.DropDownWidth = value; }

        [Browsable(true)]
        [DefaultValue(106)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public int DropDownHeight { get => cbo_Main.DropDownHeight; set => cbo_Main.DropDownHeight = value; }

        [Bindable(true)]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public object SelectedItem { get => cbo_Main.SelectedItem; set => cbo_Main.SelectedItem = value; }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelectedIndex { get => cbo_Main.SelectedIndex; set => cbo_Main.SelectedIndex = value; }

        private void UC_MLCombo_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void Pan_Btn_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawComboButton(e.Graphics, e.ClipRectangle, ButtonState.Normal);
        }

        private void Pan_text_Paint(object sender, PaintEventArgs e)
        {            
            e.Graphics.DrawString((FixedText? this.Text: this.cbo_Main.SelectedText), this.Font, this.ForeBrush, e.ClipRectangle,SF);
        }

        private void UC_MLCombo_Click(object sender, EventArgs e)
        {
            cbo_Main.Focus();
            cbo_Main.DroppedDown = true;
        }

        private void UC_MLCombo_Load(object sender, EventArgs e)
        {
            
        }
    }
}
