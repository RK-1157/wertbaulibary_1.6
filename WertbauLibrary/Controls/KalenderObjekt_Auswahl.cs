﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace WertbauLibrary.Controls
{
    public partial class KalenderObjekt_Auswahl : UserControl
    {
        public DateTime Value = DateTime.Now;

        bool CanResize = false;

        int NormalWidth = 260;
        int NormalHeight = 36;

        int MaximumWidth = 410;
        int MaximumHeight = 580;

        public delegate void OpenKalender(object source, KalenderOffen e);
        public event OpenKalender KalenderÖffnen; // Event schickt client an Hörer

        public delegate void CloseKalender(object source, CloseKalenderEvent e);
        public event CloseKalender KalenderSchließen; // Event schickt client an Hörer

        public delegate void DatumChanged(object source, DatumChangedEvent e);
        public event DatumChanged DatumGeändert; // Event schickt client an Hörer

        public class DatumChangedEvent : EventArgs
        {
            private DateTime dateF = DateTime.Now;
            public DatumChangedEvent(DateTime verb)
            {
                dateF = verb;
            }
            public DateTime GetKalender()
            {
                return dateF;
            }
        }

        public class KalenderOffen : EventArgs
        {
            private WertbauLibrary.Controls.KalenderObjekt Kalender;
            public KalenderOffen(KalenderObjekt verb)
            {
                Kalender = verb;
            }
            public KalenderObjekt GetKalender()
            {
                return Kalender;
            }
        }

        public class CloseKalenderEvent : EventArgs
        {
            private WertbauLibrary.Controls.KalenderObjekt Kalender;
            public CloseKalenderEvent(KalenderObjekt verb)
            {
                Kalender = verb;
            }
            public KalenderObjekt GetKalender()
            {
                return Kalender;
            }
        }

        KalenderObjekt Kalender = new KalenderObjekt();

        public KalenderObjekt_Auswahl()
        {
            InitializeComponent();

            SetzeDatum(DateTime.Now);

            Kalender.DatumGeändert += Kalender_DatumGeändert;
        }

        private void Kalender_DatumGeändert(object source, KalenderObjekt.DatumChangedEvent e)
        {
            DatumGeändert?.Invoke(Kalender, new DatumChangedEvent(e.GetKalender()));           
            SetzeDatum(e.GetKalender());

            KalenderSchließen?.Invoke(Kalender, new CloseKalenderEvent(Kalender));

            MaximumTable.Controls.Remove(Kalender);
            this.Height = NormalHeight;
            this.Width = NormalWidth;
        }

        private void BTKalenderÖffnen(object sender, EventArgs e)
        {
            if (CanResize)
            {
                this.Height = MaximumHeight;
                this.Width = MaximumWidth;               
                MaximumTable.Controls.Add(Kalender);
            }
            else
            {
                KalenderÖffnen?.Invoke(this, new KalenderOffen(Kalender));
            }


        }

        public void SetzeDatum(DateTime datm)
        {
            DatumText.Text = datm.ToString("dd. MMMM, yyyy");

            Value = datm;
        }

        public void InitKalender(bool cansize = false)
        {
            CanResize = cansize;
            if (!cansize)
            {
                Kalender.Dock = DockStyle.None;
                Kalender.Height = MaximumHeight;
            }
        }

        private void DatumText_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        public void SchließeKalender()
        {
            KalenderSchließen?.Invoke(Kalender, new CloseKalenderEvent(Kalender));
        }
    }
}
