﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WertbauLibrary.Controls
{
    public partial class DoubleBufferedFlawLayoutPanel : FlowLayoutPanel
    {
        public DoubleBufferedFlawLayoutPanel()
        {
            this.DoubleBuffered = true;
        }
        protected override void OnScroll(ScrollEventArgs se)
        {
            this.Invalidate();
            base.OnScroll(se);
        }
    }
}
