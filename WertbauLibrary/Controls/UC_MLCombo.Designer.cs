﻿namespace TestFrom
{
    partial class UC_MLCombo
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbo_Main = new System.Windows.Forms.ComboBox();
            this.Pan_Btn = new System.Windows.Forms.Panel();
            this.Pan_Text = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // cbo_Main
            // 
            this.cbo_Main.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbo_Main.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_Main.FormattingEnabled = true;
            this.cbo_Main.Location = new System.Drawing.Point(0, 0);
            this.cbo_Main.Name = "cbo_Main";
            this.cbo_Main.Size = new System.Drawing.Size(335, 24);
            this.cbo_Main.TabIndex = 0;
            // 
            // Pan_Btn
            // 
            this.Pan_Btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.Pan_Btn.Location = new System.Drawing.Point(301, 0);
            this.Pan_Btn.Name = "Pan_Btn";
            this.Pan_Btn.Size = new System.Drawing.Size(34, 24);
            this.Pan_Btn.TabIndex = 1;
            this.Pan_Btn.Click += new System.EventHandler(this.UC_MLCombo_Click);
            this.Pan_Btn.Paint += new System.Windows.Forms.PaintEventHandler(this.Pan_Btn_Paint);
            // 
            // Pan_Text
            // 
            this.Pan_Text.BackColor = System.Drawing.SystemColors.Window;
            this.Pan_Text.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Pan_Text.Location = new System.Drawing.Point(0, 0);
            this.Pan_Text.Name = "Pan_Text";
            this.Pan_Text.Size = new System.Drawing.Size(301, 24);
            this.Pan_Text.TabIndex = 2;
            this.Pan_Text.Click += new System.EventHandler(this.UC_MLCombo_Click);
            this.Pan_Text.Paint += new System.Windows.Forms.PaintEventHandler(this.Pan_text_Paint);
            // 
            // UC_MLCombo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Pan_Text);
            this.Controls.Add(this.Pan_Btn);
            this.Controls.Add(this.cbo_Main);
            this.Name = "UC_MLCombo";
            this.Size = new System.Drawing.Size(335, 24);
            this.Load += new System.EventHandler(this.UC_MLCombo_Load);
            this.Click += new System.EventHandler(this.UC_MLCombo_Click);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.UC_MLCombo_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbo_Main;
        private System.Windows.Forms.Panel Pan_Btn;
        private System.Windows.Forms.Panel Pan_Text;
    }
}
