﻿namespace WertbauLibrary.Controls
{
    partial class UC_Scroll_Buttons_DatagridView
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlp_Scroll = new System.Windows.Forms.TableLayoutPanel();
            this.cmd_bottom = new System.Windows.Forms.Button();
            this.cmd_Down = new System.Windows.Forms.Button();
            this.cmd_Up = new System.Windows.Forms.Button();
            this.cmd_Top = new System.Windows.Forms.Button();
            this.tlp_Scroll.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlp_Scroll
            // 
            this.tlp_Scroll.ColumnCount = 1;
            this.tlp_Scroll.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_Scroll.Controls.Add(this.cmd_bottom, 0, 3);
            this.tlp_Scroll.Controls.Add(this.cmd_Down, 0, 2);
            this.tlp_Scroll.Controls.Add(this.cmd_Up, 0, 1);
            this.tlp_Scroll.Controls.Add(this.cmd_Top, 0, 0);
            this.tlp_Scroll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp_Scroll.Location = new System.Drawing.Point(0, 0);
            this.tlp_Scroll.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tlp_Scroll.Name = "tlp_Scroll";
            this.tlp_Scroll.RowCount = 4;
            this.tlp_Scroll.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp_Scroll.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp_Scroll.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp_Scroll.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp_Scroll.Size = new System.Drawing.Size(55, 624);
            this.tlp_Scroll.TabIndex = 0;
            // 
            // cmd_bottom
            // 
            this.cmd_bottom.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmd_bottom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cmd_bottom.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_bottom.Location = new System.Drawing.Point(0, 470);
            this.cmd_bottom.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.cmd_bottom.Name = "cmd_bottom";
            this.cmd_bottom.Size = new System.Drawing.Size(55, 154);
            this.cmd_bottom.TabIndex = 3;
            this.cmd_bottom.Text = "⤓";
            this.cmd_bottom.UseVisualStyleBackColor = true;
            this.cmd_bottom.Click += new System.EventHandler(this.cmd_bottom_Click);
            // 
            // cmd_Down
            // 
            this.cmd_Down.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmd_Down.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cmd_Down.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_Down.Location = new System.Drawing.Point(0, 314);
            this.cmd_Down.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.cmd_Down.Name = "cmd_Down";
            this.cmd_Down.Size = new System.Drawing.Size(55, 152);
            this.cmd_Down.TabIndex = 2;
            this.cmd_Down.Text = "↓";
            this.cmd_Down.UseVisualStyleBackColor = true;
            this.cmd_Down.Click += new System.EventHandler(this.cmd_Down_Click);
            // 
            // cmd_Up
            // 
            this.cmd_Up.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmd_Up.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cmd_Up.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_Up.Location = new System.Drawing.Point(0, 158);
            this.cmd_Up.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.cmd_Up.Name = "cmd_Up";
            this.cmd_Up.Size = new System.Drawing.Size(55, 152);
            this.cmd_Up.TabIndex = 1;
            this.cmd_Up.Text = "↑";
            this.cmd_Up.UseVisualStyleBackColor = true;
            this.cmd_Up.Click += new System.EventHandler(this.cmd_Up_Click);
            // 
            // cmd_Top
            // 
            this.cmd_Top.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmd_Top.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cmd_Top.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_Top.Location = new System.Drawing.Point(0, 0);
            this.cmd_Top.Margin = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.cmd_Top.Name = "cmd_Top";
            this.cmd_Top.Size = new System.Drawing.Size(55, 154);
            this.cmd_Top.TabIndex = 0;
            this.cmd_Top.Text = "⤒";
            this.cmd_Top.UseVisualStyleBackColor = true;
            this.cmd_Top.Click += new System.EventHandler(this.cmd_Top_Click);
            // 
            // UC_Scroll_Buttons_DatagridView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tlp_Scroll);
            this.Name = "UC_Scroll_Buttons_DatagridView";
            this.Size = new System.Drawing.Size(55, 624);
            this.tlp_Scroll.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tlp_Scroll;
        private System.Windows.Forms.Button cmd_bottom;
        private System.Windows.Forms.Button cmd_Down;
        private System.Windows.Forms.Button cmd_Up;
        private System.Windows.Forms.Button cmd_Top;
    }
}
