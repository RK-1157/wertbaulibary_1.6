﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WertbauLibrary.Classes;
using WertbauLibrary.Types;

namespace WertbauLibrary.Controls
{
    [DefaultEvent(nameof(Buttonpressed))]
    public partial class UC_Scroll_Buttons_DatagridView : UserControl
    {
        public UC_Scroll_Buttons_DatagridView()
        {
            InitializeComponent();
            RowScrollMenge = 3;
            ArrowsAsText = false;
        }

        public DataGridView DataGridView { get; set; }

        [DefaultValue(3)]
        public int RowScrollMenge { get; set; }

        [DefaultValue(false)]
        public bool ArrowsAsText
        {
            get { return cmd_Top.Text == "⤒"; }
            set
            {
                if(value)
                {
                    cmd_Top.Text = "⤒";
                    cmd_Up.Text = "↑";
                    cmd_Down.Text = "↓";
                    cmd_bottom.Text = "⤓";
                    cmd_Top.BackgroundImage = null;
                    cmd_Up.BackgroundImage = null;
                    cmd_Down.BackgroundImage = null;
                    cmd_bottom.BackgroundImage = null;
                }
                else
                {
                    cmd_Top.Text = "";
                    cmd_Up.Text = "";
                    cmd_Down.Text = "";
                    cmd_bottom.Text = "";
                    cmd_Top.BackgroundImage = Properties.Resources.Arrow_Top;
                    cmd_Up.BackgroundImage = Properties.Resources.Arrow_Up;
                    cmd_Down.BackgroundImage = Properties.Resources.Arrow_Down;
                    cmd_bottom.BackgroundImage = Properties.Resources.Arrow_Bottom;
                }
            }
        }

        public event EventHandler<ButtonpressedEventArgs> Buttonpressed;

        public void Top_Click() => cmd_Top_Click(cmd_Top, EventArgs.Empty);
        private void cmd_Top_Click(object sender, EventArgs e)
        {
            Buttonpressed?.Invoke(this, new ButtonpressedEventArgs(ButtonPressed.Top));

            if (this.DataGridView != null)
            {
                if (DataGridView.Rows.Count > 0)
                    DataGridView.FirstDisplayedScrollingRowIndex = 0;
            }
        }

        public void Up_Click() => cmd_Up_Click(cmd_Up, EventArgs.Empty);
        private void cmd_Up_Click(object sender, EventArgs e)
        {
            Buttonpressed?.Invoke(this, new ButtonpressedEventArgs(ButtonPressed.Up));

            if (this.DataGridView != null)
            {
                if (DataGridView.FirstDisplayedScrollingRowIndex > RowScrollMenge)
                    DataGridView.FirstDisplayedScrollingRowIndex -= RowScrollMenge;
                else if (DataGridView.Rows.Count > 0)
                    DataGridView.FirstDisplayedScrollingRowIndex = 0;
            }
        }

        public void Down_Click() => cmd_Down_Click(cmd_Down, EventArgs.Empty);
        private void cmd_Down_Click(object sender, EventArgs e)
        {
            Buttonpressed?.Invoke(this, new ButtonpressedEventArgs(ButtonPressed.Down));
            if (this.DataGridView != null)
            {
                if (DataGridView.FirstDisplayedScrollingRowIndex + RowScrollMenge < DataGridView.Rows.Count)
                    DataGridView.FirstDisplayedScrollingRowIndex += RowScrollMenge;
                else if (DataGridView.Rows.Count > RowScrollMenge)
                    DataGridView.FirstDisplayedScrollingRowIndex = DataGridView.Rows.Count - RowScrollMenge;
                else if (DataGridView.Rows.Count>0)
                    DataGridView.FirstDisplayedScrollingRowIndex = DataGridView.Rows.Count;
            }
        }

        public void bottom_Click() => cmd_bottom_Click(cmd_bottom, EventArgs.Empty);
        private void cmd_bottom_Click(object sender, EventArgs e)
        {
            Buttonpressed?.Invoke(this, new ButtonpressedEventArgs(ButtonPressed.Bottom));
            if (this.DataGridView != null)
            {
                if (DataGridView.Rows.Count > 0)
                    DataGridView.FirstDisplayedScrollingRowIndex = Math.Max(DataGridView.Rows.Count - RowScrollMenge, 0);
            }
        }
    }

    
    
}
