﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace WertbauLibrary.Controls
{
    public partial class DoubleBufferedPanel : UserControl
    {
        private Timer TimerF;
        int counter = 0;

        public delegate void SucheAbbrechen(object source, AbgebrochenEventArgs e);
        public event SucheAbbrechen SucheAbgebrochen; // Event schickt client an Hörer


        public class AbgebrochenEventArgs : EventArgs
        {
            public bool Abbruch { get; set; }
            public AbgebrochenEventArgs(bool abbruch)
            {
                Abbruch = abbruch;
            }
        }


        public DoubleBufferedPanel()
        {
            InitializeComponent();

            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            TimerF = new Timer();
            TimerF.Interval = 25;
            TimerF.Tick += TimerF_Tick;

            this.Visible = false;
        }


        public void Working(bool Start = false)
        {
            if (!Start)
            {
                TimerF.Stop();
                this.Visible = false;
            }
            else
            {
                TimerF.Start();
                this.Visible = true;
            }

        }

        private void DrawProgress(Graphics g, Rectangle rect, Single percentage, int angle)
        {
            float processAngle = 360 / 100 * percentage;
            float remainderAngle = 360 - processAngle;

            Pen progressPen = new Pen(Color.Green, 2);
            Pen remainderPen = new Pen(Color.Transparent, 2);

            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.DrawArc(progressPen, rect, counter, angle);
            g.DrawArc(remainderPen, rect, angle - counter, remainderAngle);

        }

        private void TimerF_Tick(object sender, EventArgs e)
        {
            this.Refresh();
            counter += 10;
            if (counter >= 360)
                counter = counter - 360;

            this.Visible = true;
            Application.DoEvents();
        }

        private void DoubleBufferedPanel_Paint_1(object sender, PaintEventArgs e)
        {
            DrawProgress(e.Graphics, new Rectangle(4, 4, this.Width - 15, this.Width - 15), 80, counter);
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            SucheAbgebrochen?.Invoke(this, new AbgebrochenEventArgs(true));
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            pictureBox1.BackgroundImage = Properties.Resources.abbrechen;
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            pictureBox1.BackgroundImage = null;
        }
    }
}
