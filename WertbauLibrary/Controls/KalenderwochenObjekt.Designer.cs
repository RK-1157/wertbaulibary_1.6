﻿namespace WertbauLibrary.Controls
{
    partial class KalenderwochenObjekt
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button8 = new System.Windows.Forms.Button();
            this.Button7 = new System.Windows.Forms.Button();
            this.Button6 = new System.Windows.Forms.Button();
            this.Button5 = new System.Windows.Forms.Button();
            this.Button4 = new System.Windows.Forms.Button();
            this.Button3 = new System.Windows.Forms.Button();
            this.Button2 = new System.Windows.Forms.Button();
            this.Button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Button8
            // 
            this.Button8.Location = new System.Drawing.Point(836, 3);
            this.Button8.Name = "Button8";
            this.Button8.Size = new System.Drawing.Size(113, 29);
            this.Button8.TabIndex = 16;
            this.Button8.Text = "Button8";
            this.Button8.UseVisualStyleBackColor = true;
            this.Button8.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Button7
            // 
            this.Button7.Location = new System.Drawing.Point(717, 3);
            this.Button7.Name = "Button7";
            this.Button7.Size = new System.Drawing.Size(113, 29);
            this.Button7.TabIndex = 15;
            this.Button7.Text = "Button7";
            this.Button7.UseVisualStyleBackColor = true;
            this.Button7.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Button6
            // 
            this.Button6.Location = new System.Drawing.Point(598, 3);
            this.Button6.Name = "Button6";
            this.Button6.Size = new System.Drawing.Size(113, 29);
            this.Button6.TabIndex = 14;
            this.Button6.Text = "Button6";
            this.Button6.UseVisualStyleBackColor = true;
            this.Button6.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Button5
            // 
            this.Button5.Location = new System.Drawing.Point(479, 3);
            this.Button5.Name = "Button5";
            this.Button5.Size = new System.Drawing.Size(113, 29);
            this.Button5.TabIndex = 13;
            this.Button5.Text = "Button5";
            this.Button5.UseVisualStyleBackColor = true;
            this.Button5.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Button4
            // 
            this.Button4.Location = new System.Drawing.Point(360, 3);
            this.Button4.Name = "Button4";
            this.Button4.Size = new System.Drawing.Size(113, 29);
            this.Button4.TabIndex = 12;
            this.Button4.Text = "Button4";
            this.Button4.UseVisualStyleBackColor = true;
            this.Button4.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Button3
            // 
            this.Button3.Location = new System.Drawing.Point(241, 3);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(113, 29);
            this.Button3.TabIndex = 11;
            this.Button3.Text = "Button3";
            this.Button3.UseVisualStyleBackColor = true;
            this.Button3.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Button2
            // 
            this.Button2.Location = new System.Drawing.Point(122, 3);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(113, 29);
            this.Button2.TabIndex = 10;
            this.Button2.Text = "Button2";
            this.Button2.UseVisualStyleBackColor = true;
            this.Button2.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Button1
            // 
            this.Button1.Location = new System.Drawing.Point(3, 3);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(113, 29);
            this.Button1.TabIndex = 9;
            this.Button1.Text = "Button1";
            this.Button1.UseVisualStyleBackColor = true;
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // KW_Objekt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Button8);
            this.Controls.Add(this.Button7);
            this.Controls.Add(this.Button6);
            this.Controls.Add(this.Button5);
            this.Controls.Add(this.Button4);
            this.Controls.Add(this.Button3);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.Button1);
            this.Name = "KW_Objekt";
            this.Size = new System.Drawing.Size(954, 36);
            this.Load += new System.EventHandler(this.KW_Objekt_Load);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button Button8;
        internal System.Windows.Forms.Button Button7;
        internal System.Windows.Forms.Button Button6;
        internal System.Windows.Forms.Button Button5;
        internal System.Windows.Forms.Button Button4;
        internal System.Windows.Forms.Button Button3;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Button Button1;
    }
}
