﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WertbauLibrary.Controls
{
    public class UC_LoadPanel:Panel
    {
        protected Timer t_Timer = new Timer();
        protected int counter = 0;

        protected readonly Pen progressPen = new Pen(Color.Green, 2f);
        protected readonly Pen remainderPen = new Pen(Color.Transparent, 2f);

        protected float _winkel = 80f;

        public UC_LoadPanel():base()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);

            Interval = 20;

            t_Timer.Tick += T_Timer_Tick;
            this.Visible = false;
            this.Paint += LoadPanel_Paint;
            this.Size = new Size(54, 54);
        }

       

        [DefaultValue(20)]
        public int Interval
        {
            get { return t_Timer.Interval; }
            set { t_Timer.Interval = value; }
        }

        [DefaultValue(typeof(Color), "Green")]
        public Color KreisFarbe
        {
            get { return progressPen.Color; }
            set { progressPen.Color = value; }
        }

        [DefaultValue(2f)]
        public float KreisDicke
        {
            get { return progressPen.Width; }
            set
            {
                progressPen.Width = value;
                remainderPen.Width = value;
            }
        }

        [DefaultValue(80f)]
        public float Winkel
        {
            get { return _winkel; }
            set { _winkel = value; }
        }

        public void Working(bool start = false)
        {
            if (!start)
            {
                t_Timer.Stop();
                this.Visible = false;
            }
            else
            {
                t_Timer.Start();
                this.Visible = true;
           }
        }

        private void T_Timer_Tick(object sender, EventArgs e)
        {
            this.counter = (this.counter + 10) % 360;
            this.Invalidate();
        }

        private void LoadPanel_Paint(object sender, PaintEventArgs e)
        {
            Rectangle rectangle = this.DisplayRectangle;
            int Dicke = (int)Math.Floor(KreisDicke);
            rectangle.X += Dicke / 2;
            rectangle.Y += Dicke / 2;
            rectangle.Height -= Dicke + 1;
            rectangle.Width -= Dicke + 1;
            DrawProgress(e.Graphics, rectangle, Winkel, counter);
        }

        private void DrawProgress(Graphics g , Rectangle rect , float percentage, int angle)
        {
            float progressAngle = 360f / 100f * percentage;
            float remainderAngle = 360f - progressAngle;


            //set the smoothing to high quality for better output
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            //draw the blue and white arcs
            g.DrawArc(progressPen, rect, counter, progressAngle);
            g.DrawArc(remainderPen, rect, progressAngle - counter, remainderAngle);
        }
    }
}
