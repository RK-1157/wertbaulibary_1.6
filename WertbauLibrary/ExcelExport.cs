﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using WertbauLibrary.Classes;

namespace WertbauLibrary
{
    public static class ExcelExport
    {
        /// <summary>
        /// Exportiert eine DataGridView in ein Excel-Worksheet
        /// </summary>
        /// <param name="dg">Die zu exportierende DataGridView</param>
        /// <param name="ausgewertet">Zeitpunkt (null = nicht verwenden)</param>
        /// <param name="title">Titel (null = nicht verwenden)</param>
        /// <param name="bereich">Bereich (null = nicht verwenden)</param>
        /// <param name="exportColIndexList">Die zu exportierenden Spaltenindexe (0-basiert!) (null = alle Spalten)</param>
        /// <param name="statusColIndexList">Die Spaltenindexe sind Status-, also Farbspalten (null = nicht verwenden)</param>
        /// <param name="customStatusColorsList">Eigene Liste definierter Statusfarben (Key=>Status, Farbe) (null = nicht verwenden)</param>
        /// <param name="additionalColumns">Eine Liste zusätzlicher (leerer) Spalten (null = nicht verwenden)</param>
        /// <param name="umranden">Excel-Zellen werden umrandet</param>
        /// <param name="freezeHeader">Der Kopf, incl. Spaltenkopf sind starr und verschwinden durch Scrollen nicht</param>
        /// <param name="blatt">Das für den Export zu verwendende Excel-Worksheet</param>
        /// <returns></returns>
        public static void ÜbertrageDGZuExcel(DataGridView dg, DateTime? ausgewertet, string title, string bereich,
            int[] exportColIndexList, int[] statusColIndexList, SortedList<int, Color> customStatusColorsList, string[] additionalColumns, 
            bool umranden, bool freezeHeader, Worksheet blatt)
        {
            int zeileTitle = 1;
            int spalte = 1;
            int zeile = 4;

            char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

            if (ausgewertet != null)
            {
                blatt.Cells[zeileTitle, 1].Value = "Ausgewertet am " + ausgewertet.Value.ToShortDateString() + " " + ausgewertet.Value.ToShortTimeString();
                blatt.Cells[zeileTitle, 1].Font.Bold = true;

                zeileTitle++;
            }

            if (!string.IsNullOrEmpty(title))
            {
                blatt.Cells[zeileTitle, 1].Value = title;
                blatt.Cells[zeileTitle, 1].Font.Bold = true;
            }

            if (!string.IsNullOrEmpty(bereich))
            {
                blatt.Cells[zeile, spalte].Value = bereich;
                blatt.Cells[zeile, spalte].Font.Bold = true;
            }

            // Wenn es mehr als 26 Spalten sind, dann weiter mit AA, AB, AC usw. - es werden doch nicht etwa mehr als 52 Spalten exportiert?!
            var columnChar = (exportColIndexList == null ? dg.ColumnCount : exportColIndexList.Length) +
                (additionalColumns == null ? 0 : additionalColumns.Length) - 1;
            var rangeEnd = columnChar < chars.Length ? chars[columnChar].ToString() : "A" + chars[columnChar - chars.Length];
            string ranged = "A6:" + rangeEnd;

            zeile += 2;

            var colIndex = 0;
            foreach (DataGridViewColumn col in dg.Columns)
            {
                if (exportColIndexList != null)
                    if (!exportColIndexList.Contains(colIndex))
                    {
                        colIndex += 1;
                        continue;
                    }

                blatt.Cells[zeile, spalte].Value = col.HeaderText;
                blatt.Cells[zeile, spalte].Font.Bold = true;
                blatt.Cells[zeile, spalte].Interior.Color = Color.LightGray;

                foreach (DataGridViewRow row in dg.Rows)
                {
                    if (row.Cells[col.Index].Value != null)
                    {
                        if (statusColIndexList.Contains(colIndex))
                        {
                            // Status-Spalte => farbig
                            if (row.Cells[col.Index].Value.GetType() == typeof(int))
                            {
                                int status = (int)row.Cells[col.Index].Value;

                                Color statusFarbe = Farben.StatusFarbe((Farben.Status)status);

                                if (customStatusColorsList != null)
                                {
                                    if (customStatusColorsList.ContainsKey(status))
                                        statusFarbe = customStatusColorsList[status];
                                    else
                                        statusFarbe = Color.Transparent;
                                }

                                blatt.Cells[zeile + row.Index + 1, spalte].Interior.Color = statusFarbe;
                            }
                        }
                        else
                        {
                            if (row.Cells[col.Index].Value.GetType() == typeof(string))
                                blatt.Cells[zeile + row.Index + 1, spalte].NumberFormat = "@";
                            else if (row.Cells[col.Index].Value.GetType() == typeof(DateTime))
                            {
                                if (((DateTime)row.Cells[col.Index].Value).Hour + ((DateTime)row.Cells[col.Index].Value).Minute +
                                    ((DateTime)row.Cells[col.Index].Value).Second > 0)
                                {
                                    // DateTime
                                    blatt.Cells[zeile + row.Index + 1, spalte].NumberFormat = "TT.MM.JJJJ hh:mm:ss";
                                }
                                else
                                {
                                    // Date
                                    blatt.Cells[zeile + row.Index + 1, spalte].NumberFormat = "TT.MM.JJJJ";
                                }
                            }
                        }
                    }

                    blatt.Cells[zeile + row.Index + 1, spalte].Value = statusColIndexList.Contains(colIndex) ? "" : row.Cells[col.Index].Value;
                }

                colIndex += 1;
                spalte += 1;
            }

            if (additionalColumns != null)
            {
                var additionalColumnsSpalte = (exportColIndexList == null ? dg.ColumnCount : exportColIndexList.Length) + 1;

                foreach (var additionalColumn in additionalColumns)
                {
                    blatt.Cells[6, additionalColumnsSpalte].Value = additionalColumn;
                    blatt.Cells[6, additionalColumnsSpalte].Font.Bold = true;
                    blatt.Cells[6, additionalColumnsSpalte].Interior.Color = Color.LightGray;

                    additionalColumnsSpalte++;
                }
            }

            ranged += "" + (dg.Rows.Count + 6);

            if (umranden)
                BorderAround(blatt.Range[ranged]);

            if (freezeHeader)
            {
                var splitRow = blatt.Rows[7];
                splitRow.Activate();
                splitRow.Select();
                splitRow.Application.ActiveWindow.FreezePanes = true;
            }

            blatt.Cells[3, 1].Select();
        }

        private static void BorderAround(Range range)
        {
            Borders borders = range.Borders;
            borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;

            borders[XlBordersIndex.xlInsideVertical].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlInsideHorizontal].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlDiagonalUp].LineStyle = XlLineStyle.xlLineStyleNone;
            borders[XlBordersIndex.xlDiagonalDown].LineStyle = XlLineStyle.xlLineStyleNone;
            borders = null;
        }

        private static void ReleaseComObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            }
            catch
            {
            }
        }
    }
}
