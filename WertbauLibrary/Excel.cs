﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WertbauLibrary
{
    /*
     Export nach Excel

        DatagridView
        umranden: bool
        Spaltenliste: int[]
        Spalte "Status": int[] => Zelle->Farbe
        Zusätzliche Spalten: string[] (Spaltennamen)

         */

    public static class Excel
    {
        private/*public*/ static void ÜbertrageDGZuExcel(DataGridView dg, DateTime? ausgewertet, string title, string bereich, 
            int[] exportColIndexList, int[] statusColIndexList, string[] additionalColumns, bool umranden, Worksheet blatt)
        {
            int zeileTitle = 1;
            int spalte = 1;
            int zeile = 4;

            char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

            if(ausgewertet != null)
            {
                blatt.Cells[zeileTitle, 1].Value = "Ausgewertet am " + ausgewertet.Value.ToShortDateString() + " " + ausgewertet.Value.ToShortTimeString();
                blatt.Cells[zeileTitle, 1].Font.Bold = true;

                zeileTitle++;
            }
            
            if(!string.IsNullOrEmpty(title))
            {
                blatt.Cells[zeileTitle, 1].Value = title;
                blatt.Cells[zeileTitle, 1].Font.Bold = true;
            }

            if (!string.IsNullOrEmpty(bereich))
            {
                blatt.Cells[zeile, spalte].Value = bereich;
                blatt.Cells[zeile, spalte].Font.Bold = true;
            }

            string ranged = "A6:" + (chars[dg.ColumnCount - 1]);

            zeile += 2;

            foreach (DataGridViewColumn col in dg.Columns)
            {
                blatt.Cells[zeile, spalte].Value = col.HeaderText;
                blatt.Cells[zeile, spalte].Font.Bold = true;
                blatt.Cells[zeile, spalte].Interior.Color = System.Drawing.Color.LightGray;

                foreach (DataGridViewRow row in dg.Rows)
                {
                    if (row.Cells[col.Index].Value.GetType() == typeof(string))
                        blatt.Cells[zeile + row.Index + 1, spalte].NumberFormat = "@";
                    else if (row.Cells[col.Index].Value.GetType() == typeof(DateTime))
                    {
                        if (((DateTime)row.Cells[col.Index].Value).Hour + ((DateTime)row.Cells[col.Index].Value).Minute +
                            ((DateTime)row.Cells[col.Index].Value).Second > 0)
                        {
                            // DateTime
                            blatt.Cells[zeile + row.Index + 1, spalte].NumberFormat = "TT.MM.JJJJ hh:mm:ss";
                        }
                        else
                        {
                            // Date
                            blatt.Cells[zeile + row.Index + 1, spalte].NumberFormat = "TT.MM.JJJJ";
                        }
                    }

                    blatt.Cells[zeile + row.Index + 1, spalte].Value = row.Cells[col.Index].Value;
                }

                spalte += 1;
            }

            ranged += "" + (dg.Rows.Count + 6);

            if(umranden)
                BorderAround(blatt.Range[ranged]);

            var splitRow = blatt.Rows[7];
            splitRow.Activate();
            splitRow.Select();
            splitRow.Application.ActiveWindow.FreezePanes = true;
            blatt.Cells[3, 1].Select();
        }

        private static void BorderAround(Range range)
        {
            Borders borders = range.Borders;
            borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;

            borders[XlBordersIndex.xlInsideVertical].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlInsideHorizontal].LineStyle = XlLineStyle.xlContinuous;
            borders[XlBordersIndex.xlDiagonalUp].LineStyle = XlLineStyle.xlLineStyleNone;
            borders[XlBordersIndex.xlDiagonalDown].LineStyle = XlLineStyle.xlLineStyleNone;
            borders = null;
        }

        private static void ReleaseComObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            }
            catch
            {
            }
        }
    }
}
