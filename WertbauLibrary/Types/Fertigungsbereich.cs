﻿namespace WertbauLibrary.Types
{
    public enum  Fertigungsbereich
    {
        None = 0,

        Profilierung,
        Eckverbindung,
        Oberflaeche,
        Endmontage_NGWA,
        Endmontage_Holz,
        HST,
        Sonderbau_Geb6,
        AluschalenFertigung,
        Einzelstab,
        Profillierung_Geb6,
        Eckverbindung_HO,
        Eckverbindung_NGWA,
        /// <summary>
        /// Wird für forschleife verwendet und stellt keinen Bereich dar
        /// </summary>
        Last_Line

            // Ab 100 ist es  MAWI
    }
}
