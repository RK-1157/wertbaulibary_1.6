﻿namespace WertbauLibrary.Types
{
    public enum Festverglasung
    {
        Rahmen = 0,
        Innen = 1,
        Außen = 2,
    }
}
