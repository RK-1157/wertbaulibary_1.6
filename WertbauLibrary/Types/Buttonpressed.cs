﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WertbauLibrary.Types
{
    public enum ButtonPressed
    {
        None = 0,
        Top,
        Up,
        Down,
        Bottom
    }
}
