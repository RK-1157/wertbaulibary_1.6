﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Threading;

namespace WertbauLibrary
{
    public static class Konstanten
    {
        public const string MySqlDatenConnectionstring = "server=192.168.116.26;uid=hokorfid;pwd=geheim2!;database=feprod;";
        public const string MySqlDatenConnectionstring_test = "server=192.168.116.26;uid=hokorfid;pwd=geheim2!;database=test_feprod;";
        //public const string PpsDatenConnectionstring = "Server=DB2007;Database=PPSDaten;user id=hokorfid;pwd=geheim2!;";
        //public const string PufferConnectionstring = "Server=DB2008;Database=WBT_MaschineData;user id=hokorfid; pwd=geheim2!;";

        //public const string V200ConnectionString = "Server=DB2008;Database=WERTBAUv200;user id=hokorfid; pwd=geheim2!;";
        //public const string EKAWertbauConnectionString = "Server=DB2008;Database=EKWERTBAU;user id=hokorfid; pwd=geheim2!;";

        private static string _PpsDatenConnectionstring = null;
        public static string PpsDatenConnectionstring
        {
            get
            {
                if (_PpsDatenConnectionstring == null)
                    _PpsDatenConnectionstring = ConnectionStringFromMysql("PPSDATEN");
                return _PpsDatenConnectionstring;
            }
        }

        private static string _PufferConnectionstring = null;
        public static string PufferConnectionstring
        {
            get
            {
                if (_PufferConnectionstring == null)
                    _PufferConnectionstring = ConnectionStringFromMysql("WBT_MASCHINEDATA");
                return _PufferConnectionstring;
            }
        }

        private static string _V200ConnectionString = null;
        public static string V200ConnectionString
        {
            get
            {
                if (_V200ConnectionString == null)
                    _V200ConnectionString = ConnectionStringFromMysql("WertbauV200");
                return _V200ConnectionString;
            }
        }

        private static string _EKAWertbauConnectionString = null;
        public static string EKAWertbauConnectionString
        {
            get
            {
                if (_EKAWertbauConnectionString == null)
                    _EKAWertbauConnectionString = ConnectionStringFromMysql("EKWERTBAU");
                return _EKAWertbauConnectionString;
            }
        }

        private static string ConnectionStringFromMysql(string feld)
        {
            string ret = "";
            using (var conn = new MySqlConnection(MySqlDatenConnectionstring))
            {
                conn.Open();
                using (var cmd = new MySqlCommand("SELECT * from db_connection WHERE id = 2", conn))
                {
                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret = reader[feld].ToString();
                        }
                }
            }

            return ret;
        }

        public static readonly int[] ProfilierStationenArray = { 1, 4 };
        public static readonly string ProfilierStationen = string.Join(",", ProfilierStationenArray);

        public static readonly int[] EinzelstabStationenArray = { 9056 };
        public static readonly string EinzelstabStationen = string.Join(",", EinzelstabStationenArray);

        public static readonly int[] EndStationenNGWAArray = { 150,152 }; // ACHTUNG!!!!!!!!!!!!!!!!!!!!!!! Neue Station id 152 hinzugekommen, die ist eine eigenentwicklung!!!
        public static readonly string EndStationenNGWA = string.Join(",", EndStationenNGWAArray);

        public static readonly int[] EndStationenHolzArray = { 69, 148 };
        public static readonly string EndStationenHolz = string.Join(",", EndStationenHolzArray);

        public static readonly int[] Gebaeude6Array = { 2, 5 };
        public static readonly string Gebaeude6Stationen = string.Join(",", Gebaeude6Array);

        public static int MaxTH { get; set; }
        public static int CountTH { get; set; }
        public static List<Thread> _ListTH = new List<Thread>();
        public static List<Thread> ListTH { get => _ListTH; }

        public static readonly TimeSpan RueckstandZeitspanne = new TimeSpan(14, 0, 0, 0);
        public static readonly TimeSpan SchichtstartZeitspanne = new TimeSpan(3, 00, 0);
        public static readonly string UhrzeitSchichtstart = SchichtstartZeitspanne.ToString();
        public static readonly string UhrzeitSchichtende = SchichtstartZeitspanne.Subtract(new TimeSpan(0, 0, 1)).ToString();

        public static readonly DateTime NADat = new DateTime(1901, 1, 1);
        public static readonly DateTime ignoreNADat = new DateTime(1902, 2, 2);

        public static DateTime DatumSchicht(this DateTime dateTime)
        {
            return (dateTime - SchichtstartZeitspanne).Date;
        }
    }
}
