﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WertbauLibrary.Types;
using TageszaehlerLibary.Classes;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System.Threading;
using WertbauLibrary;
using System.IO;
using System.Data;

namespace TageszaehlerLibary
{
    public static class Tageszaehler
    {

        //Entfernen nach nächsten WertbaulibaryUpdate
        #region "HoleAufträge"
        public static IList<string> HoleAuftraegeSollProfilierung(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Profilierung_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND `Kennzeichen` IN ('NGWA','HO','HA') AND Gebaeude6 = 0 ";
            if (FilterFertig)
                sql += " AND `Profilierung_ist` IS NULL";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        public static IList<string> HoleAuftraegeSollEinzelstab(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Einzelstab_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND `Kennzeichen` IN ('NGWA','HO','HA') AND Gebaeude6 = 0 ";
            if (FilterFertig)
                sql += " AND `Einzelstab_ist` IS NULL";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        public static IList<string> HoleAuftraegeSollEckverbindung(DateTime von, DateTime bis, Fertigungsbereich bereich, bool FilterFertig = false)
        {
            string sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Eckverbindung_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE  AND Gebaeude6 = 0 ";

            switch(bereich)
            {
                case Fertigungsbereich.Eckverbindung:
                    sql += "  AND `Kennzeichen` IN ('NGWA','HO','HA')";
                    break;
                case Fertigungsbereich.Eckverbindung_HO:
                    sql += "  AND `Kennzeichen` IN ('HO')";
                    break;
                case Fertigungsbereich.Eckverbindung_NGWA:
                    sql += "  AND `Kennzeichen` IN ('NGWA','HA')";
                    break;
            }

            if (FilterFertig)
                sql += " AND `Eckverbindung_ist` IS NULL";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        public static IList<string> HoleAuftraegeSollOberflaeche(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Oberflaeche_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND `Kennzeichen` IN ('NGWA','HO','HA')";
            if (FilterFertig)
                sql += " AND `Oberflaeche_ist` IS NULL";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        public static IList<string> HoleAuftraegeSollAluschale(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql;
            if (FilterFertig)
                sql = @"SELECT `Auftragsnummer`
                    FROM `auftragsstatus` AS AuS 
                    JOIN `auftrag` AS Au
                      ON au.`Id` = AuS.`auftrag_Id` 
                    JOIN `position` AS pos  
                      ON pos.`OrderNr` = au.`AuftragsNr` 	
                    WHERE DATE(`Oberflaeche_soll`) BETWEEN @von AND @bis 
	                    AND `Loeschkennzeichen` = FALSE 
                        AND `Kennzeichen` IN ('NGWA','HA') 
 	                    AND (pos.`AluFluegelFertig` < 2 OR pos.`AluRahmenFertig`< 2)
                    GROUP BY au.`Id`";
            else
                sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Oberflaeche_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND `Kennzeichen` IN ('NGWA','HA')";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        public static IList<string> HoleAuftraegeSollEndNGWA(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Endmontage_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND `Kennzeichen` IN ('NGWA')";
            if (FilterFertig)
                sql += " AND `Endmontage_ist` IS NULL";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        public static IList<string> HoleAuftraegeSollEndHolz(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE
            DATE(`Endmontage_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND (`Kennzeichen` IN ('HO') OR (`Kennzeichen` IN ('HA') AND WS01 = true )) AND Gebaeude6 = 0 ";

            if (FilterFertig)
                sql += " AND `Endmontage_ist` IS NULL";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }

        public static IList<string> HoleAuftraegeSollHST(DateTime von, DateTime bis, bool FilterFertig = false)
        {
            string sql = @"SELECT `Auftragsnummer` FROM `auftragsstatus` AS AuS WHERE DATE(`Endmontage_soll`) BETWEEN @von AND @bis AND `Loeschkennzeichen` = FALSE AND `HST` = TRUE";
            if (FilterFertig)
                sql += " AND `Endmontage_ist` IS NULL";
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();

                List<string> ret = new List<string>();
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@von", MySqlDbType.Date).Value = von;
                    cmd.Parameters.Add("@bis", MySqlDbType.Date).Value = bis;

                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            ret.Add(reader.GetString(0));
                        }
                }
                return ret;
            }
        }
        #endregion

        #region "Bereich"
        #region "Allgemein"       
        #region "Update"
        public static void UpdateTageszaehlerAll(DateTime date)
        {

            System.Threading.Tasks.Parallel.For((int)Fertigungsbereich.None + 1, (int)Fertigungsbereich.Last_Line, (bereich) =>
            //for (Fertigungsbereich bereich = Fertigungsbereich.None + 1; bereich < Fertigungsbereich.Last_Line; bereich++)
            {
                using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
                {
                    conn.Open();
                    UpdateTageszaehler(date, (Fertigungsbereich)bereich, conn);
                }
            });
        }
        public static void UpdateTageszaehlerAll(DateTime date, MySqlConnection conn)
        {
            for (Fertigungsbereich bereich = Fertigungsbereich.None + 1; bereich < Fertigungsbereich.Last_Line; bereich++)
            {
                UpdateTageszaehler(date, bereich, conn);
            }
        }

        public static TageszaehlerBereich UpdateTageszaehler(DateTime date, Fertigungsbereich fertigungsbereich)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();
                return UpdateTageszaehler(date, fertigungsbereich, conn);
            }
        }
        public static TageszaehlerBereich UpdateTageszaehler(DateTime date, Fertigungsbereich fertigungsbereich, MySqlConnection conn)
        {
            var tageszaehler = new TageszaehlerBereich
            {
                Datum = date.Date,
                Fertigungsbereich = fertigungsbereich
            };

            bool ok = false;
            switch (fertigungsbereich)
            {
                case Fertigungsbereich.Profilierung:
                    ok = UpdateTageszaehlerProfilierung(tageszaehler, conn);
                    break;
                case Fertigungsbereich.Eckverbindung:
                    ok = UpdateTageszaehlerEckverbindung(tageszaehler, conn);
                    break;
                case Fertigungsbereich.Eckverbindung_HO:
                    ok = UpdateTageszaehlerEckverbindung(tageszaehler, conn);
                    break;
                case Fertigungsbereich.Eckverbindung_NGWA:
                    ok = UpdateTageszaehlerEckverbindung(tageszaehler, conn);
                    break;
                case Fertigungsbereich.Oberflaeche:
                    ok = UpdateTageszaehlerOberflaeche(tageszaehler, conn);
                    break;
                case Fertigungsbereich.AluschalenFertigung:
                    ok = UpdateTageszaehlerAluschale(tageszaehler, conn);
                    break;
                case Fertigungsbereich.Endmontage_NGWA:
                    ok = UpdateTageszaehlerEndNGWA(tageszaehler, conn);
                    break;
                case Fertigungsbereich.Endmontage_Holz:
                    ok = UpdateTageszaehlerEndHolz(tageszaehler, conn);
                    break;
                case Fertigungsbereich.HST:
                    ok = UpdateTageszaehlerHST(tageszaehler, conn);
                    break;
                case Fertigungsbereich.Sonderbau_Geb6:
                    ok = UpdateTageszaehlerGeb6Sonder(tageszaehler, conn);
                    break;
                case Fertigungsbereich.Einzelstab:
                    ok = UpdateTageszaehlerEinzelstab(tageszaehler, conn);
                    break;
                case Fertigungsbereich.Profillierung_Geb6:
                    ok = UpdateTageszaehlerProfillierungGeb6(tageszaehler, conn);
                    break;
            }
            if (ok)
            {
                tageszaehler.Finished = (DateTime.Now.DatumSchicht() > tageszaehler.Datum);

                UpdateTageszaehler_InMySql(tageszaehler, conn);

            }
            return tageszaehler;
        }

        public static void UpdateTageszaehler_InMySql(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            if (tageszaehler.Soll != 0 || tageszaehler.Fertig != 0 || tageszaehler.RueckstandBisher != 0 || tageszaehler.VorlaufBisher != 0)
            {
                const string cmd_text =
                  @"INSERT INTO `tageszaehler_bereiche`    (`Datum`,`Bereich`,`Bereich_Bez`,`Soll`,`Soll_m2`,`Fertig`,`Fertig_m2`,`Fertig_NF`,`Fertig_Soll`,`Fertig_Soll_m2`,`Vorlauf`,`Vorlauf_m2`,`Vorlauf_Soll`,`Vorlauf_Soll_m2`,`Rueckstand`,`Rueckstand_m2`,`Finished`,`Rueckstand_bisher`,`Rueckstand_bisher_m2`,`Rueckstand_bisher_mitSoll`,`Rueckstand_bisher_mitSoll_m2`,`Vorlauf_bisher`,`Vorlauf_bisher_m2`)
                VALUES								       (@Datum, @Bereich, @Bereich_Bez, @Soll, @Soll_m2, @Fertig, @Fertig_m2, @Fertig_NF, @Fertig_Soll, @Fertig_Soll_m2, @Vorlauf, @Vorlauf_m2, @Vorlauf_Soll, @Vorlauf_Soll_m2, @Rueckstand, @Rueckstand_m2, @Finished, @Rueckstand_bisher, @Rueckstand_bisher_m2, @Rueckstand_bisher_mitSoll, @Rueckstand_bisher_mitSoll_m2, @Vorlauf_bisher, @Vorlauf_bisher_m2)
                ON DUPLICATE KEY UPDATE `Soll` 			                = @Soll,
                                        `Soll_m2` 		                = @Soll_m2,
                                        `Fertig` 		                = @Fertig,
                                        `Fertig_m2` 	                = @Fertig_m2,
                                        `Fertig_NF`		                = @Fertig_NF,
                                        `Fertig_Soll`                   = @Fertig_Soll,
                                        `Fertig_Soll_m2`                = @Fertig_Soll_m2,
                                        `Vorlauf` 		                = @Vorlauf,
                                        `Vorlauf_m2`	                = @Vorlauf_m2,
                                        `Vorlauf_Soll`                  = @Vorlauf_Soll,
                                        `Vorlauf_Soll_m2`               = @Vorlauf_Soll_m2,
                                        `Rueckstand` 	                = @Rueckstand,
                                        `Rueckstand_m2`                 = @Rueckstand_m2,
                                        `Finished`                      = @Finished,
                                        `Rueckstand_bisher`             = @Rueckstand_bisher,
                                        `Rueckstand_bisher_m2`          = @Rueckstand_bisher_m2,
                                        `Rueckstand_bisher_mitSoll`     = @Rueckstand_bisher_mitSoll,
                                        `Rueckstand_bisher_mitSoll_m2`  = @Rueckstand_bisher_mitSoll_m2,
                                        `Vorlauf_bisher`                = @Vorlauf_bisher,
                                        `Vorlauf_bisher_m2`             = @Vorlauf_bisher_m2";

                using (var cmd = new MySqlCommand(cmd_text, conn))
                {
                    MySqlParameter para;
                    cmd.Parameters.Add("@Datum", MySqlDbType.Date).Value = tageszaehler.Datum.Date;
                    cmd.Parameters.Add("@Bereich", MySqlDbType.Int32).Value = tageszaehler.Fertigungsbereich;
                    cmd.Parameters.Add("@Bereich_Bez", MySqlDbType.VarString).Value = tageszaehler.Fertigungsbereich.ToString();
                    cmd.Parameters.Add("@Soll", MySqlDbType.Int32).Value = tageszaehler.Soll;
                    para = cmd.Parameters.Add("@Soll_m2", MySqlDbType.Decimal);
                    para.Precision = 15;
                    para.Scale = 2;
                    para.Value = tageszaehler.SollM2;
                    cmd.Parameters.Add("@Fertig", MySqlDbType.Int32).Value = tageszaehler.Fertig;
                    para = cmd.Parameters.Add("@Fertig_m2", MySqlDbType.Decimal);
                    para.Precision = 15;
                    para.Scale = 2;
                    para.Value = tageszaehler.FertigM2;
                    cmd.Parameters.Add("@Fertig_NF", MySqlDbType.Int32).Value = tageszaehler.FertigNf;
                    cmd.Parameters.Add("@Fertig_Soll", MySqlDbType.Int32).Value = tageszaehler.FertigSoll;
                    para = cmd.Parameters.Add("@Fertig_Soll_m2", MySqlDbType.Decimal);
                    para.Precision = 15;
                    para.Scale = 2;
                    para.Value = tageszaehler.FertigSollM2;
                    cmd.Parameters.Add("@Vorlauf", MySqlDbType.Int32).Value = tageszaehler.Vorlauf;
                    para = cmd.Parameters.Add("@Vorlauf_m2", MySqlDbType.Decimal);
                    para.Precision = 15;
                    para.Scale = 2;
                    para.Value = tageszaehler.VorlaufM2;
                    cmd.Parameters.Add("@Vorlauf_Soll", MySqlDbType.Int32).Value = tageszaehler.VorlaufSoll;
                    para = cmd.Parameters.Add("@Vorlauf_Soll_m2", MySqlDbType.Decimal);
                    para.Precision = 15;
                    para.Scale = 2;
                    para.Value = tageszaehler.VorlaufSollM2;
                    cmd.Parameters.Add("@Rueckstand", MySqlDbType.Int32).Value = tageszaehler.Rueckstand;
                    para = cmd.Parameters.Add("@Rueckstand_m2", MySqlDbType.Decimal);
                    para.Precision = 15;
                    para.Scale = 2;
                    para.Value = tageszaehler.RueckstandM2;
                    cmd.Parameters.Add("@Finished", MySqlDbType.Bit).Value = tageszaehler.Finished;
                    cmd.Parameters.Add("@Rueckstand_bisher", MySqlDbType.Int32).Value = tageszaehler.RueckstandBisher;
                    para = cmd.Parameters.Add("@Rueckstand_bisher_m2", MySqlDbType.Decimal);
                    para.Precision = 15;
                    para.Scale = 2;
                    para.Value = tageszaehler.RueckstandBisherM2;
                    cmd.Parameters.Add("@Rueckstand_bisher_mitSoll", MySqlDbType.Int32).Value = tageszaehler.RueckstandBisherMitSoll;
                    para = cmd.Parameters.Add("@Rueckstand_bisher_mitSoll_m2", MySqlDbType.Decimal);
                    para.Precision = 15;
                    para.Scale = 2;
                    para.Value = tageszaehler.RueckstandBisherMitSollM2;
                    cmd.Parameters.Add("@Vorlauf_bisher", MySqlDbType.Int32).Value = tageszaehler.VorlaufBisher;
                    para = cmd.Parameters.Add("@Vorlauf_bisher_m2", MySqlDbType.Decimal);
                    para.Precision = 15;
                    para.Scale = 2;
                    para.Value = tageszaehler.VorlaufBisherM2;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion

        #region "Get"
        public static TageszaehlerBereich GetTageszaehler(DateTime date, Fertigungsbereich fertigungsbereich)
        {
            return GetTageszaehler(date, fertigungsbereich, false);
        }
        public static TageszaehlerBereich GetTageszaehler(DateTime date, Fertigungsbereich fertigungsbereich, bool force_Update)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();
                return GetTageszaehler(date, fertigungsbereich, force_Update, conn);
            }
        }
        public static TageszaehlerBereich GetTageszaehler(DateTime date, Fertigungsbereich fertigungsbereich, MySqlConnection conn)
        {
            return GetTageszaehler(date, fertigungsbereich, false, conn);
        }
        public static TageszaehlerBereich GetTageszaehler(DateTime date, Fertigungsbereich fertigungsbereich, bool force_Update, MySqlConnection conn)
        {
            bool finished = false;
            if (!force_Update)
            {
                using (var cmd = new MySqlCommand(@"Select IFNULL(MAX(`Finished`),0) AS Finished FROM `tageszaehler_bereiche` WHERE `Datum` = @Datum AND `Bereich` = @Bereich", conn))
                {
                    cmd.Parameters.Add("@Datum", MySqlDbType.Date).Value = date;
                    cmd.Parameters.Add("@Bereich", MySqlDbType.Int32).Value = fertigungsbereich;
                    using (var reader = cmd.ExecuteReader())
                        if (reader.Read())
                            finished = reader.GetBoolean(0);
                }
            }
            if (!finished)
                try
                {
                    UpdateTageszaehler(date, fertigungsbereich, conn);
                }
                catch { }

            TageszaehlerBereich ret = new TageszaehlerBereich()
            {
                Datum = date,
                Fertigungsbereich = fertigungsbereich
            };
            using (var cmd = new MySqlCommand("SELECT `Soll`,`Soll_m2`,`Fertig`,`Fertig_m2`,`Fertig_NF`,`Fertig_Soll`,`Fertig_Soll_m2`,`Vorlauf`,`Vorlauf_m2`,`Vorlauf_Soll`,`Vorlauf_Soll_m2`,`Rueckstand`,`Rueckstand_m2`,`Rueckstand_bisher`,`Rueckstand_bisher_m2`,`Rueckstand_bisher_mitSoll`,`Rueckstand_bisher_mitSoll_m2`,`Vorlauf_bisher`,`Vorlauf_bisher_m2`  FROM `tageszaehler_bereiche` WHERE `Datum` = @Datum AND `Bereich` = @Bereich", conn))
            {
                cmd.Parameters.Add("@Datum", MySqlDbType.Date).Value = date;
                cmd.Parameters.Add("@Bereich", MySqlDbType.Int32).Value = fertigungsbereich;
                using (var reader = cmd.ExecuteReader())
                    if (reader.Read())
                    {
                        ret.Soll = reader.GetInt32("Soll");
                        ret.SollM2 = reader.GetDecimal("Soll_m2");
                        ret.Fertig = reader.GetInt32("Fertig");
                        ret.FertigM2 = reader.GetDecimal("Fertig_m2");
                        ret.FertigNf = reader.GetInt32("Fertig_NF");
                        ret.FertigSoll = reader.GetInt32("Fertig_Soll");
                        ret.FertigSollM2 = reader.GetDecimal("Fertig_Soll_m2");
                        ret.Vorlauf = reader.GetInt32("Vorlauf");
                        ret.VorlaufM2 = reader.GetDecimal("Vorlauf_m2");
                        ret.Rueckstand = reader.GetInt32("Rueckstand");
                        ret.RueckstandM2 = reader.GetDecimal("Rueckstand_m2");
                        ret.RueckstandBisher = reader.GetInt32("Rueckstand_bisher");
                        ret.RueckstandBisherM2 = reader.GetDecimal("Rueckstand_bisher_m2");
                        ret.RueckstandBisherMitSoll = reader.GetInt32("Rueckstand_bisher_mitSoll");
                        ret.RueckstandBisherMitSollM2 = reader.GetDecimal("Rueckstand_bisher_mitSoll_m2");
                        ret.VorlaufBisher = reader.GetInt32("Vorlauf_bisher");
                        ret.VorlaufBisherM2 = reader.GetDecimal("Vorlauf_bisher_m2");
                        ret.VorlaufSoll = reader.GetInt32("Vorlauf_Soll");
                        ret.VorlaufSollM2 = reader.GetDecimal("Vorlauf_Soll_m2");
                    }
            }
            return ret;
        }
        #endregion

        #region "Filter"
        public static IEnumerable<string> FilterAuftragListStatus100(IEnumerable<string> Basis, DateTime date, MySqlConnection conn)
        {
            return FilterAuftragListStatus100(Basis, AuNr => AuNr, date, conn);
        }
        public static IEnumerable<T> FilterAuftragListStatus100<T>(IEnumerable<T> Basis, Func<T, string> GetAuNr, DateTime date, MySqlConnection conn)
        {
            List<T> ret = new List<T>();
            using (var cmd = new MySqlCommand(
                @"SELECT DISTINCT 1
                    FROM Auftrag a
                    WHERE  @AuNr IN (a.SapNr, a.auftragsNr)
                      AND (   a.Status >= 100)
                      AND IFNULL(a.`ChangeDat`,a.`AngelDat`) < CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')", conn))
            {
                var para_AuNr = cmd.Parameters.Add("@AuNr", MySqlDbType.VarString);
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = date;

                foreach (var itm in Basis)
                {
                    para_AuNr.Value = GetAuNr(itm);
                    using (var reader = cmd.ExecuteReader())
                        if (!reader.Read())
                            ret.Add(itm);
                }
            }

            Basis = ret;
            ret = new List<T>();
            using (var cmd = new MySqlCommand(
                @"SELECT DISTINCT 1 FROM `auftragsstatus` WHERE `Auftragsnummer` = @AuNr  AND `Fertigmeldung_pps` < CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"');", conn))
            {
                var para_AuNr = cmd.Parameters.Add("@AuNr", MySqlDbType.VarString);
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = date;

                foreach (var itm in Basis)
                {
                    para_AuNr.Value = GetAuNr(itm);
                    using (var reader = cmd.ExecuteReader())
                        if (!reader.Read())
                            ret.Add(itm);
                }
            }
            return ret;
        }

        public static IEnumerable<string> FilterAuftragListHalle6(IEnumerable<string> Basis)
        {
            return FilterAuftragListHalle6(Basis, AuNr => AuNr);
        }
        public static IEnumerable<T> FilterAuftragListHalle6<T>(IEnumerable<T> Basis, Func<T, string> GetAuNr)
        {
            var ret = new List<T>();
            foreach (var itm in Basis)
            {
                var AuNr = GetAuNr(itm);
                if (!Wertbau.IstAuftragHalle6MySQL(AuNr))
                    ret.Add(itm);
            }
            return ret;
        }


        public static IEnumerable<string> FilterAuftragListHalle6Marked(IEnumerable<string> Basis, MySqlConnection conn)
        {
            return FilterAuftragListHalle6Marked(Basis, AuNr => AuNr, conn);
        }
        public static IEnumerable<T> FilterAuftragListHalle6Marked<T>(IEnumerable<T> Basis, Func<T, string> GetAuNr, MySqlConnection conn)
        {
            List<T> ret = new List<T>();
            using (var cmd = new MySqlCommand(
                @"SELECT DISTINCT 1
                    FROM framewood AS fw
		            JOIN position AS pos
			            ON fw.`Position_Id` = pos.`Id`
		            JOIN Auftrag AS AU
			            ON pos.`OrderNr` = AU.`AuftragsNr`
                    WHERE  @AuNr IN (AU.SapNr, AU.auftragsNr)
                      AND ( fw.gebaude6 <> 0 AND fw.produce = 1)", conn))
            {
                var para_AuNr = cmd.Parameters.Add("@AuNr", MySqlDbType.VarString);
                foreach (var itm in Basis)
                {
                    para_AuNr.Value = GetAuNr(itm);
                    using (var reader = cmd.ExecuteReader())
                        if (!reader.Read())
                            ret.Add(itm);
                }
            }
            return ret;
        }

        public static IEnumerable<string> FilterAuftragListWS1(IEnumerable<string> Basis)
        {
            return FilterAuftragListWS1(Basis, AuNr => AuNr);
        }
        public static IEnumerable<T> FilterAuftragListWS1<T>(IEnumerable<T> Basis, Func<T, string> GetAuNr)
        {
            var ret = new List<T>();
            foreach (var itm in Basis)
            {
                var AuNr = GetAuNr(itm);
                if (!Wertbau.IstAuftragWS1(AuNr))
                    ret.Add(itm);
            }
            return ret;
        }

        public static IEnumerable<string> FilterAuftragListAlumat(IEnumerable<string> Basis, MySqlConnection conn)
        {
            return FilterAuftragListAlumat(Basis, AuNr => AuNr, conn);
        }
        public static IEnumerable<T> FilterAuftragListAlumat<T>(IEnumerable<T> Basis, Func<T, string> GetAuNr, MySqlConnection conn)
        {
            List<T> ret = new List<T>();
            using (var cmd = new MySqlCommand(
                @"SELECT DISTINCT 1
                    FROM partwood AS pw
		            JOIN framewood AS fw
			            ON pw.`FrameWood_FrameID` = fw.`FrameID` 
                        AND fw.produce = 1 
		            JOIN position AS pos
			            ON fw.`Position_Id` = pos.`Id`
		            JOIN Auftrag AS AU
			            ON pos.`OrderNr` = AU.`AuftragsNr`
                    WHERE @AuNr IN(a.AuftragsNr, a.SAPNr)
                        AND partnamelong LIKE  '%alumat%';", conn))
            {
                var para_AuNr = cmd.Parameters.Add("@AuNr", MySqlDbType.VarString);
                foreach (var itm in Basis)
                {
                    para_AuNr.Value = GetAuNr(itm);
                    using (var reader = cmd.ExecuteReader())
                        if (!reader.Read())
                            ret.Add(itm);

                }
            }
            return ret;
        }


        #endregion


        #endregion

        #region "Profillierung"
        #region "HoleFertig"
        public static FertigRet HoleFertigProfillierungFromAuNr(string AuNr)
        {
            return HoleFertigProfillierungFromAuNr(AuNr, DateTime.Now);
        }
        public static FertigRet HoleFertigProfillierungFromAuNr(string AuNr, DateTime MaxDT)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();
                return HoleFertigProfillierungFromAuNr(AuNr, MaxDT, conn);
            }
        }
        public static FertigRet HoleFertigProfillierungFromAuNr(string AuNr, MySqlConnection conn)
        {
            return HoleFertigProfillierungFromAuNr(AuNr, DateTime.Now, conn);
        }
        public static FertigRet HoleFertigProfillierungFromAuNr(string AuNr, DateTime MaxDT, MySqlConnection conn)
        {
            var ret = new FertigRet()
            {
                Menge = 0,
                M2 = 0m
            };
            using (var cmd = new MySqlCommand(
                @"SELECT IFNULL(SUM(Anzahl),0) AS Anzahl, IFNULL(SUM(CASE WHEN tbl.FrameTyp =1 THEN ((pos.`TotalHeight` * pos.`TotalWidth`) / 1000000) ELSE 0 END),0) AS QM
                FROM(	SELECT pos.`Id`, fw.`FrameTyp`, COUNT(*) AS Anzahl
		                FROM partwood AS pw
		                 JOIN framewood AS fw
			                ON pw.`FrameWood_FrameID` = fw.`FrameID` 
                            AND fw.produce = 1 
		                 JOIN position AS pos
			                ON fw.`Position_Id` = pos.`Id`
		                 JOIN auftrag AS au
			                ON au.`AuftragsNr` = pos.`OrderNr`
		                WHERE @AuNR IN (au.SAPNr, au.AuftragsNr)
                            AND pos.`ProfFertigDatum` < DATE(DATE_ADD(@Date, INTERVAL 1 DAY)) AND pos.`ProfFertigDatum` is NOT NULL                          
		                GROUP BY pos.`Id`, fw.`FrameTyp`) AS tbl
                LEFT JOIN position AS pos
	                ON tbl.`Id` = pos.`Id`", conn))
            {
                cmd.Parameters.Add("@AuNr", MySqlDbType.VarString).Value = AuNr;
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = MaxDT;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        ret.Menge += reader.GetInt32("Anzahl");
                        ret.M2 += reader.GetDecimal("QM");
                    }
            }
            return ret;
        }
        #endregion
        #region "Filter"
        public static IEnumerable<string> FilterAuftragListForProfillierung(IEnumerable<string> Basis)
        {
            return FilterAuftragListForProfillierung(Basis, AuNR => AuNR);
        }
        public static IEnumerable<T> FilterAuftragListForProfillierung<T>(IEnumerable<T> Basis, Func<T, string> GetAuNr)
        {
            Basis = FilterAuftragListHalle6(Basis, GetAuNr);

            //Basis = FilterAuftragListAlumat(Basis, GetAuNr, conn);

            return Basis;
        }
        #endregion
        #region "GetSoll"
        public static AuftragSoll GetAuftragSollProfillierung(string AuNr, MySqlConnection conn)
        {
            AuftragSoll ret = new AuftragSoll() { AuNr = AuNr };

            using (var cmd = new MySqlCommand(
                @"SELECT `Anzahl_kanteln` AS Anzahl,`Quadratmeter` AS QM FROM `auftragsstatus` WHERE `Auftragsnummer` = @AuNR ", conn))
            {
                var para_AuNr = cmd.Parameters.Add("@AuNR", MySqlDbType.VarString);

                para_AuNr.Value = AuNr;
                using (var reader = cmd.ExecuteReader())
                    if (reader.Read())
                    {
                        ret.SollKanteln = reader.GetInt32("Anzahl");
                        ret.Sollm2 = reader.GetDecimal("QM");
                    }


            }
            return ret;
        }
        public static ICollection<PositionSoll> GetAuftragSollProfillierungPosition(string AuNr, MySqlConnection conn, params int[] Stationids)
        {
            return GetAuftragSollProfillierungPosition(AuNr, DateTime.Now, conn, Stationids);
        }
        public static ICollection<PositionSoll> GetAuftragSollProfillierungPosition(string AuNr, DateTime MaxDT, MySqlConnection conn, params int[] Stationids)
        {
            var solls = new List<PositionSoll>();

            Wertbau.HoleAuftragrueckstellung(AuNr, conn, out bool Rck, out string RckGrund);

            using (var cmd = new MySqlCommand(
                @"	SELECT pos.`Id`, pos.`PositionsId`, pos.`OrderPosNr`, pos.ProfFertigDatum, pos.`OrderPosNr`, IFNULL(SUM(tbl.Anzahl),0) AS Anzahl, 
                    IFNULL(SUM(CASE WHEN tbl.`FrameTyp` = 1 THEN (pos.`TotalHeight` * pos.`TotalWidth`)/1000000 ELSE NULL END),
                    (pos.`TotalHeight` * pos.`TotalWidth`)/1000000) AS QM,
	                    IFNULL(AU.Status,0) AS Status,IFNULL(AU.`ChangeDat`,AU.`AngelDat`) AS StatusDat, IFNULL(SUM(tbl.Ist),0) AS Ist, IFNULL(SUM(tbl.Schlecht),0) AS Schlecht, IFNULL(SUM(tbl.Rep),0) AS Rep
                    FROM ( 	SELECT COUNT(DISTINCT pw.ID) AS Anzahl, pos.`Id`, fw.`FrameTyp`, COUNT(CASE WHEN pws.`Gutteil` =0 THEN 1 ELSE NULL END) AS Schlecht, 
		                    COUNT(CASE WHEN pws.`Gutteil` =1 THEN 1 ELSE NULL END) AS Ist,COUNT(CASE WHEN pws.`Gutteil` >1 THEN 1 ELSE NULL END) AS Rep
		                    FROM partwood AS pw
		                    LEFT JOIN partwoodstatus as pws
			                    ON pws.`Partwood_Id` = pw.`Id`
			                    AND pws.`Station_Id` IN(" + string.Join(",", Stationids) + @")
                                AND pws.`Gutteil` >= 1
                                AND IFNULL(pws.`ChangeDat`,pws.`AngelDat`) < CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
		                    JOIN framewood AS fw
			                    ON pw.`FrameWood_FrameID` = fw.`FrameID` 
                                AND fw.produce = 1 
		                    JOIN position AS pos
			                    ON fw.`Position_Id` = pos.`Id`
		                    JOIN Auftrag AS AU
			                    ON pos.`OrderNr` = AU.`AuftragsNr`
		                    WHERE @AuNr IN (AU.`AuftragsNr`, AU.`SAPNr`)                            
			                    AND (pw.partmaterial NOT LIKE '%Dummy%' AND pw.partmaterial NOT LIKE '%Rohholz%' 
			                    AND pw.vFamilia_id != 38 
			                    AND NOT pw.vclase_id IN (42,38,1,4) AND NOT pw.vrole_id IN (1,18))
		                    GROUP BY pos.`Id`, fw.`FrameTyp`) AS tbl
                    JOIN position AS pos
	                    ON tbl.`Id` = pos.`Id`
                    JOIN Auftrag AS AU
	                    ON pos.`OrderNr` = AU.`AuftragsNr`
                    GROUP BY pos.`Id`", conn))
            {
                var para_AuNr = cmd.Parameters.Add("@AuNR", MySqlDbType.VarString);
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = MaxDT;
                cmd.CommandTimeout = 360;
                para_AuNr.Value = AuNr;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        PositionSoll pos = new PositionSoll
                        {
                            AuNr = AuNr,
                            SollKanteln = reader.GetInt32("Anzahl"),
                            Sollm2 = reader.GetDecimal("QM"),
                            PosId = reader.GetInt32("Id"),
                            PosPositionsId = reader.GetString("PositionsId"),
                            PosNr = reader.GetString("OrderPosNr"),
                            Rckstellen = Rck,
                            RckstellenGrund = RckGrund
                        };

                        var status = reader.GetInt32("Status");
                        DateTime? profFertigDatum = reader.IsDBNull(reader.GetOrdinal("ProfFertigDatum")) ?
                            null : (DateTime?)reader.GetDateTime("ProfFertigDatum");
                        pos.FertigDatum = profFertigDatum;
                        DateTime StatusDatumZeit = reader.GetDateTime("StatusDat");
                        if ((status >= 100
                             && (StatusDatumZeit - Konstanten.SchichtstartZeitspanne).Date <= MaxDT.Date)
                        || (profFertigDatum.HasValue
                             && profFertigDatum.Value.Date <= MaxDT.Date && profFertigDatum.Value.Date.Year > Konstanten.ignoreNADat.Year))
                        {
                            pos.KantelnRep = 0;
                            pos.KantelnSchlecht = 0;
                            pos.KantelnIst = pos.SollKanteln;
                        }
                        else
                        {
                            pos.KantelnIst = reader.GetInt32("Ist");
                            pos.KantelnRep = reader.GetInt32("Rep");
                            pos.KantelnSchlecht = reader.GetInt32("Schlecht");
                        }
                        if ((pos.Fertig && profFertigDatum == null) || (profFertigDatum == null && pos.SollKanteln <= 0))
                        {
                            pos.SetFertig = true;
                        }

                        solls.Add(pos);
                    }
            }

            foreach (var pos in solls)
                if (pos.SetFertig)
                    using (var cmd = new MySqlCommand("UPDATE `position` SET `ProfFertigDatum` = DATE(NOW()) WHERE `Id` = @ID;", conn))
                    {
                        cmd.Parameters.Add("@ID", MySqlDbType.Int32).Value = pos.PosId;

                        cmd.ExecuteNonQuery();
                    }
            return solls.ToArray();
        }
        #endregion
        #region "Update
        private static bool UpdateTageszaehlerProfilierung(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            int counter = 0;
            bool ret = true;
            bool retry;
            bool Soll = true;
            bool Fertig = true;
            bool Rueck = true;
            bool Vor = true;
            do
            {
                retry = false;
                try
                {
                    if (Soll)
                    {
                        UpdateTageszaehlerProfilierungSoll(tageszaehler, conn);
                        Soll = false;
                    }
                    if (Fertig)
                    {
                        UpdateTageszaehlerProfilierungFertig(tageszaehler, conn);
                        Fertig = false;
                    }
                    if (Rueck)
                    {
                        UpdateTageszaehlerProfilierungRueck(tageszaehler, conn);
                        Rueck = false;
                    }
                    if (Vor)
                    {
                        UpdateTageszaehlerProfilierungVor(tageszaehler, conn);
                        Vor = false;
                    }

                    if (tageszaehler.Datum == DateTime.Now.DatumSchicht())
                        UpdateProfilierungSetIstAfterEck(conn);
                }
                catch (MySqlException MysqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, MysqlEx.Message + Environment.NewLine + MysqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
                catch (SqlException SqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, SqlEx.Message + Environment.NewLine + SqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
            } while (retry);

            return ret;
        }

        private class UpdateTageszaehlerProfilierungFertigHelper
        {
            public int ID { get; set; }
            public int Anzahl { get; set; }
            public decimal QM { get; set; }
            public bool QM_Fertig { get; set; }
            public DateTime? AuFertigDat { get; set; }
            public string Auftrag { get; set; }
        }
        private static void UpdateTageszaehlerProfilierungSoll(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            IEnumerable<string> SollAuftraege = HoleAuftraegeSollProfilierung(tageszaehler.Datum, tageszaehler.Datum);

            SollAuftraege = FilterAuftragListForProfillierung(SollAuftraege);

            #region "GetSoll"
            tageszaehler.Soll = 0;
            tageszaehler.SollM2 = 0m;
            tageszaehler.VorlaufSoll = 0;
            tageszaehler.VorlaufSollM2 = 0m;

            foreach (var AuNr in SollAuftraege)
            {
                var Auf = GetAuftragSollProfillierung(AuNr, conn);
                tageszaehler.Soll += Auf.SollKanteln;
                tageszaehler.SollM2 += Auf.Sollm2;
                var Ist = HoleFertigProfillierungFromAuNr(AuNr, tageszaehler.Datum.AddDays(-1), conn);
                tageszaehler.VorlaufSoll += Ist.Menge;
                tageszaehler.VorlaufSollM2 += Ist.M2;
            }

            #endregion
        }
        private static void UpdateTageszaehlerProfilierungFertig(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.Fertig = 0;
            tageszaehler.FertigM2 = 0m;
            tageszaehler.Vorlauf = 0;
            tageszaehler.VorlaufM2 = 0m;
            tageszaehler.Rueckstand = 0;
            tageszaehler.RueckstandM2 = 0m;
            tageszaehler.FertigSoll = 0;
            tageszaehler.FertigSollM2 = 0m;

            #region "GetPartIDs"
            var positionen = new List<UpdateTageszaehlerProfilierungFertigHelper>();

            #region SQL OLD
            /*
            var sqlOld = @"SELECT IFNULL(pos.`Id`,-1) AS `Id`, IFNULL(COUNT(DISTINCT pw.`Id`),0) AS Anzahl, IFNULL(MAX((pos.`TotalHeight` * pos.`TotalWidth`) / 1000000),0) AS QM, MAX(pos.`ProfFertigDatum`) AS ProfFertigDatum, MAX(IFNULL(au.`cSAPNr`,IFNULL(au.`AuftragsNr`,''))) AS AuftragsNr, MAX(IFNULL( pws.`AngelDat`,pws.`ChangeDat`)) AS FertigDat, MAX(aus.`Profilierung_ist`) AS `Profilierung_ist`
		                FROM partwoodstatus AS pws
		                LEFT JOIN partwood AS pw
			                ON pws.`Partwood_Id` = pw.`Id`
		                LEFT JOIN framewood AS fw
			                ON pw.`FrameWood_FrameID` = fw.`FrameID` 
                            AND fw.produce = 1 
		                LEFT JOIN position AS pos
			                ON fw.`Position_Id` = pos.`Id`
		                LEFT JOIN partwoodstatusupdate AS pwsu 
			                on pws.partwood_partid = pwsu.partwood_partid
                        LEFT JOIN vw_auftrag AS au
	                        ON au.`AuftragsNr` = pos.`OrderNr`   
                        LEFT JOIN `auftragsstatus` as aus
	                        ON aus.`auftrag_Id` = au.`Id`
		                WHERE pws.station_id IN (" + Konstanten.ProfilierStationen + @") 	
                            AND pws.`Gutteil` >= 1
			                AND IFNULL(pws.`ChangeDat`, pws.`AngelDat`) BETWEEN CONCAT(@Date, ' " + Konstanten.UhrzeitSchichtstart + @"') AND CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
			                AND pwsu.station_id IS NULL
		                GROUP BY pos.`Id`
                        ORDER BY AuftragsNr"; 
            */
            #endregion

            var sql = @"SELECT IFNULL(a.`cSAPNr`, a.`AuftragsNr`) AS AuftragsNr, pos.id as Id, IFNULL(MAX((pos.`TotalHeight` * pos.`TotalWidth`) / 1000000),0) AS QM,
                            COUNT(pwsu.gutteil) Anzahl, MAX(pos.ProfFertigDatum) AS ProfFertigDatum, MAX(IFNULL(pwsu.changedat,pwsu.angeldat)) as FertigDat,
                            MAX(aus.`Profilierung_ist`) AS `Profilierung_ist`
                            FROM (		
                            SELECT partwood_PartID, gutteil, station_id, angeldat, changedat 
			                            FROM partwoodstatus
                                        WHERE AngelDat BETWEEN CONCAT(@Date, ' " + Konstanten.UhrzeitSchichtstart + @"') 
                                            AND CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
				                            AND (Gutteil >=1 )
                                            AND Station_ID IN (" + Konstanten.ProfilierStationen + @")
                                            ) as pwsu
                            JOIN partwood as pw
	                            ON pwsu.partwood_PartID =  pw.PartID
                            JOIN framewood fw
                                ON fw.frameid = pw.framewood_frameid
                                AND fw.produce = 1
                            JOIN position as pos
	                            on pw.`Position_PositionsId` = pos.PositionsId
                            JOIN vw_auftrag as a
	                            ON a.AuftragsNr = pos.OrderNr
							JOIN auftragsstatus as aus
	                            ON (IFNULL(a.`cSAPNr`, a.`AuftragsNr`)) = aus.auftragsnummer
                            GROUP BY pos.ID
                            ORDER BY AuftragsNr";

            using (var cmd = new MySqlCommand(
                sql, conn))
            {
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = tageszaehler.Datum.Date;
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var pos = new UpdateTageszaehlerProfilierungFertigHelper
                        {
                            ID = reader.GetInt32("Id"),
                            Anzahl = reader.GetInt32("Anzahl"),
                            QM = reader.GetDecimal("QM"),
                            QM_Fertig = false,
                            Auftrag = reader.GetString("AuftragsNr"),
                            AuFertigDat = reader.IsDBNull(reader.GetOrdinal("FertigDat")) ? null : (DateTime?)reader.GetDateTime("FertigDat")
                        };

                        if (pos.AuFertigDat.ToString().Contains("00:00:00"))
                            Console.WriteLine("STOP");

                        if (!reader.IsDBNull(reader.GetOrdinal("ProfFertigDatum")))
                        {
                            pos.QM_Fertig = true;

                            var PFD = reader.GetDateTime("ProfFertigDatum");
                            if (PFD.Date != tageszaehler.Datum.Date)
                                pos.QM = 0;
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("Profilierung_ist")))
                            pos.AuFertigDat = null;

                        positionen.Add(pos);
                    }
            }
            #endregion

            #region "CheckFertig"

            using (var cmd = new MySqlCommand(
                @"SELECT DISTINCT 1
                FROM position AS pos
                JOIN  framewood AS fw
	                ON fw.`Position_Id` = pos.`Id` 
                    AND fw.produce = 1 
                JOIN partwood AS pw
	                ON pw.`FrameWood_FrameID` = fw.`FrameID`
                LEFT JOIN partwoodstatus AS pws
	                ON pws.`Partwood_Id` = pw.`Id`
                    AND IFNULL(pws.`ChangeDat`,pws.`AngelDat`) < CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
                    AND pws.station_id IN (" + Konstanten.ProfilierStationen + @") 
                WHERE  pos.ID = @Pos_ID
                    AND pws.ID IS NULL
                    AND fw.frametyp IN (1,2) 
                    AND pw.partmaterial NOT LIKE '%Dummy%' AND pw.partmaterial NOT LIKE '%Rohholz%' 
		            AND  pw.vFamilia_id != 38 
		            AND NOT pw.vclase_id IN (42,38,1,4) AND NOT pw.vrole_id IN (1,18)", conn))
            {
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = tageszaehler.Datum.Date;
                var para_ID = cmd.Parameters.Add("@Pos_ID", MySqlDbType.Int32);
                foreach (var pos in positionen)
                    if (!pos.QM_Fertig)
                    {
                        para_ID.Value = pos.ID;
                        using (var reader = cmd.ExecuteReader())
                            if (reader.Read())
                            {
                                pos.QM = 0;
                                pos.QM_Fertig = true;
                                pos.AuFertigDat = null;
                            }
                    }

            }
            #endregion

            #region "SetFertig"
            using (var cmd = new MySqlCommand(
                @"UPDATE position
                SET `ProfFertigDatum` = @Date
                WHERE `Id` = @Pos_ID", conn))
            {
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = tageszaehler.Datum.Date;
                var para_ID = cmd.Parameters.Add("@Pos_ID", MySqlDbType.Int32);
                foreach (var pos in positionen)
                    if (!pos.QM_Fertig)
                    {
                        para_ID.Value = pos.ID;
                        cmd.ExecuteNonQuery();

                        pos.QM_Fertig = true;
                    }
            }
            #endregion

            #region "CheckFertigAuftrag"
            var auLst = positionen.Where(pos => pos.AuFertigDat.HasValue).GroupBy(pos => pos.Auftrag).Select(pos => new Tuple<string, DateTime>(pos.Key, pos.Max(ipos => ipos.AuFertigDat.Value)));
            List<Tuple<string, DateTime>> auLst_Update = new List<Tuple<string, DateTime>>(auLst.Count());

            using (var cmd = new MySqlCommand(
                @"SELECT DISTINCT 1
                    FROM `auftrag` as au
                    JOIN `position` as pos
	                    ON au.`AuftragsNr` = pos.`OrderNr`
                    LEFT OUTER JOIN framewood on pos.positionsid = framewood.positionsid	
                    WHERE @AuNr IN (`AuftragsNr`,`SAPNr`)  and framewood.frameid is not null AND framewood.produce = 1
                     AND pos.`ProfFertigDatum` IS NULL", conn))
            {
                var para = cmd.Parameters.Add("@AuNr", MySqlDbType.VarString);
                foreach (var au in auLst)
                {
                    para.Value = au.Item1;
                    using (var reader = cmd.ExecuteReader())
                        if (!reader.Read())
                            auLst_Update.Add(au);
                }

            }
            #endregion

            #region "SetFertigAuftrag"
            using (var cmd = new MySqlCommand(
                @" UPDATE `auftragsstatus`
                 SET `Profilierung_ist` =@Date
                 WHERE `Auftragsnummer` =@AuNr", conn))
            {
                var para_auNr = cmd.Parameters.Add("@AuNr", MySqlDbType.VarString);
                var para_Date = cmd.Parameters.Add("@Date", MySqlDbType.DateTime);

                foreach (var au in auLst_Update)
                {
                    para_auNr.Value = au.Item1;
                    para_Date.Value = au.Item2;

                    cmd.ExecuteNonQuery();
                }

            }
            #endregion

            #region "Rechnen"
            DateTime? Soll = null;
            string Auftrag = "";
            foreach (var pos in positionen)
            {
                tageszaehler.Fertig += pos.Anzahl;
                tageszaehler.FertigM2 += pos.QM;

                if (Auftrag != pos.Auftrag)
                    Soll = Wertbau.HoleProfilierDatumDTMySql(pos.Auftrag, true);

                if (Soll.HasValue)
                {
                    DateTime solldat = Soll.Value.Date;
                    DateTime tagdat = tageszaehler.Datum.Date;
                    if (solldat == tagdat)
                    {
                        tageszaehler.FertigSoll += pos.Anzahl;
                        tageszaehler.FertigSollM2 += pos.QM;
                    }
                    else if (solldat < tagdat && solldat.Year > Konstanten.ignoreNADat.Year)
                    {
                        tageszaehler.Rueckstand += pos.Anzahl;
                        tageszaehler.RueckstandM2 += pos.QM;
                    }
                    else if (solldat > tagdat)
                    {
                        tageszaehler.Vorlauf += pos.Anzahl;
                        tageszaehler.VorlaufM2 += pos.QM;
                    }
                }
            }
            #endregion

            #region "NF"



            tageszaehler.FertigNf = 0;

            using (var cmd = new MySqlCommand(
                @"SELECT COUNT(DISTINCT partwood_partid) AS Anzahl
                    FROM partwoodstatusupdate
                    WHERE station_id IN (" + Konstanten.ProfilierStationen + @") 
                      AND gutteil = 0  
                      AND `AngelDat` BETWEEN CONCAT(@Date, ' " + Konstanten.UhrzeitSchichtstart + @"') AND CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')", conn))
            {
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = tageszaehler.Datum.Date;
                var val = cmd.ExecuteScalar();
                if (val is long)
                    tageszaehler.FertigNf = (int)(long)val;
            }

            //tageszaehler.Fertig += tageszaehler.FertigNf;
            #endregion
        }
        /// <summary>
        /// Funktion schreibt aktuelle Exceptions wie zbsp. das mehrmalige Versuchen vom schreiben in die DB in eine Textdatei. 
        /// Dies natürlich dann mit Datum sowie Uhrzeit und Bereich um es später nachzuvollziehen!
        /// </summary>
        /// <param name="tageszaehler"></param>
        /// <param name="ex"></param>
        static void SchreibeException(TageszaehlerBereich tageszaehler, string ex)
        {
            StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + DateTime.Now.ToString("yyyy-MM-dd ") + "Exceptions.txt", true);
            sw.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " || B: " + tageszaehler.Fertigungsbereich.ToString() + " -> " + ex);
            sw.Close();
        }

        private static void UpdateTageszaehlerProfilierungRueck(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.RueckstandBisher = 0;
            tageszaehler.RueckstandBisherM2 = 0m;

            IEnumerable<string> SollAuftraege = HoleAuftraegeSollProfilierung(Konstanten.ignoreNADat, tageszaehler.Datum.AddDays(-1), true);

            SollAuftraege = FilterAuftragListForProfillierung(SollAuftraege);

            SollAuftraege = FilterAuftragListStatus100(SollAuftraege, tageszaehler.Datum, conn);

            foreach (var AuNr in SollAuftraege)
            {
                var PosColl = GetAuftragSollProfillierungPosition(AuNr, tageszaehler.Datum, conn, Konstanten.ProfilierStationenArray);
                foreach (var Pos in PosColl)
                {
                    if (Pos.SollKanteln > (Pos.KantelnIst + Pos.KantelnRep))
                    {
                        tageszaehler.RueckstandBisher += Pos.SollKanteln - Pos.KantelnIst - Pos.KantelnRep;
                        tageszaehler.RueckstandBisherM2 += Pos.Sollm2;
                    }
                }
            }

            tageszaehler.RueckstandBisherMitSoll = tageszaehler.RueckstandBisher + tageszaehler.Soll - tageszaehler.FertigSoll - tageszaehler.VorlaufSoll;
            tageszaehler.RueckstandBisherMitSollM2 = tageszaehler.RueckstandBisherM2 + tageszaehler.SollM2 - tageszaehler.FertigSollM2 - tageszaehler.VorlaufSollM2;
        }
        private static void UpdateTageszaehlerProfilierungVor(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.VorlaufBisher = 0;
            tageszaehler.VorlaufBisherM2 = 0m;

            IEnumerable<string> SollAuftraege = HoleAuftraegeSollProfilierung(tageszaehler.Datum.AddDays(1), tageszaehler.Datum.AddDays(7));

            SollAuftraege = FilterAuftragListForProfillierung(SollAuftraege);

            foreach (var Au in SollAuftraege)
            {
                var Ist = HoleFertigProfillierungFromAuNr(Au, tageszaehler.Datum.AddDays(-1), conn);
                tageszaehler.VorlaufBisher += Ist.Menge;
                tageszaehler.VorlaufBisherM2 += Ist.M2;
            }
            tageszaehler.VorlaufBisher += tageszaehler.VorlaufSoll;
            tageszaehler.VorlaufBisherM2 += tageszaehler.VorlaufSollM2;
        }
        private static void UpdateProfilierungSetIstAfterEck(MySqlConnection conn)
        {
            using (var cmd = new MySqlCommand("UPDATE `auftragsstatus` SET `Profilierung_ist` = '1901-01-01' WHERE `Profilierung_ist` IS NULL AND `Eckverbindung_ist` IS NOT NULL;", conn))
            {
                cmd.ExecuteNonQuery();
            }
        }
        #endregion
        #endregion

        #region "ProfillierungGeb6"
        #region "HoleFertig"
        public static FertigRet HoleFertigProfillierungGeb6FromAuNr(string AuNr)
        {
            return HoleFertigProfillierungGeb6FromAuNr(AuNr, DateTime.Now);
        }
        public static FertigRet HoleFertigProfillierungGeb6FromAuNr(string AuNr, DateTime MaxDT)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();
                return HoleFertigProfillierungGeb6FromAuNr(AuNr, MaxDT, conn);
            }
        }
        public static FertigRet HoleFertigProfillierungGeb6FromAuNr(string AuNr, MySqlConnection conn)
        {
            return HoleFertigProfillierungGeb6FromAuNr(AuNr, DateTime.Now, conn);
        }
        public static FertigRet HoleFertigProfillierungGeb6FromAuNr(string AuNr, DateTime MaxDT, MySqlConnection conn)
        {
            var ret = new FertigRet()
            {
                Menge = 0,
                M2 = 0m
            };
            using (var cmd = new MySqlCommand(
                @"SELECT IFNULL(SUM(Anzahl),0) AS Anzahl, IFNULL(SUM(CASE WHEN tbl.FrameTyp =1 THEN ((pos.`TotalHeight` * pos.`TotalWidth`) / 1000000) ELSE 0 END),0) AS QM
                FROM(	SELECT pos.`Id`, fw.`FrameTyp`, COUNT(*) AS Anzahl
		                FROM partwood AS pw
		                 JOIN framewood AS fw
			                ON pw.`FrameWood_FrameID` = fw.`FrameID` 
                            AND fw.produce = 1 
		                 JOIN position AS pos
			                ON fw.`Position_Id` = pos.`Id`
		                 JOIN auftrag AS au
			                ON au.`AuftragsNr` = pos.`OrderNr`
		                WHERE @AuNR IN (au.SAPNr, au.AuftragsNr)
                            AND pos.`ProfFertigDatum` < DATE(DATE_ADD(@Date, INTERVAL 1 DAY))	
                            AND YEAR(pos.`ProfFertigDatum`) > 2000 
		                GROUP BY pos.`Id`, fw.`FrameTyp`) AS tbl
                LEFT JOIN position AS pos
	                ON tbl.`Id` = pos.`Id`", conn))
            {
                cmd.Parameters.Add("@AuNr", MySqlDbType.VarString).Value = AuNr;
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = MaxDT;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        ret.Menge += reader.GetInt32("Anzahl");
                        ret.M2 += reader.GetDecimal("QM");
                    }
            }
            return ret;
        }
        #endregion
        #region "Filter"
        public static IEnumerable<string> FilterAuftragListForProfillierungGeb6(IEnumerable<string> Basis)
        {
            return FilterAuftragListForProfillierungGeb6(Basis, AuNR => AuNR);
        }
        public static IEnumerable<T> FilterAuftragListForProfillierungGeb6<T>(IEnumerable<T> Basis, Func<T, string> GetAuNr)
        {
            Basis = FilterAuftragListHalle6(Basis, GetAuNr);

            //Basis = FilterAuftragListAlumat(Basis, GetAuNr, conn);

            return Basis;
        }
        #endregion



        // 11.01.2019 Änderungen vom Vorlauf

        /*
          
            Vorlauf bisher soll vorlauf für aktuellen Tag beinhalten
            Außerdem muss vom Rückstand der Vorlauf abgezogen werden der für diesen Tag bestand!
            Dieser wurde nicht berücksichtigt


           tageszaehler.VorlaufBisher += tageszaehler.VorlaufSoll;
            tageszaehler.VorlaufBisherM2 += tageszaehler.VorlaufSollM2;
          
           tageszaehler.RueckstandBisherMitSoll = tageszaehler.RueckstandBisher + tageszaehler.Soll - tageszaehler.FertigSoll - tageszaehler.VorlaufSoll;
            tageszaehler.RueckstandBisherMitSollM2 = tageszaehler.RueckstandBisherM2 + tageszaehler.SollM2 - tageszaehler.FertigSollM2 - tageszaehler.VorlaufSollM2;
         */




        #region "GetSoll"
        public static AuftragSoll GetAuftragSollProfillierungGeb6(string AuNr, MySqlConnection conn)
        {
            AuftragSoll ret = new AuftragSoll() { AuNr = AuNr };

            using (var cmd = new MySqlCommand(
                @"SELECT `Anzahl_kanteln` AS Anzahl,`Quadratmeter` AS QM FROM `auftragsstatus` WHERE `Auftragsnummer` = @AuNR", conn))
            {
                var para_AuNr = cmd.Parameters.Add("@AuNR", MySqlDbType.VarString);

                para_AuNr.Value = AuNr;
                using (var reader = cmd.ExecuteReader())
                    if (reader.Read())
                    {
                        ret.SollKanteln = reader.GetInt32("Anzahl");
                        ret.Sollm2 = reader.GetDecimal("QM");
                    }


            }
            return ret;
        }
        public static ICollection<PositionSoll> GetAuftragSollProfillierungGeb6Position(string AuNr, MySqlConnection conn, params int[] Stationids)
        {
            return GetAuftragSollProfillierungGeb6Position(AuNr, DateTime.Now, conn, Stationids);
        }
        public static ICollection<PositionSoll> GetAuftragSollProfillierungGeb6Position(string AuNr, DateTime MaxDT, MySqlConnection conn, params int[] Stationids)
        {
            var solls = new List<PositionSoll>();

            Wertbau.HoleAuftragrueckstellung(AuNr, conn, out bool Rck, out string RckGrund);

            using (var cmd = new MySqlCommand(
                @"	SELECT pos.`Id`, pos.`PositionsId`, pos.`OrderPosNr`, pos.ProfFertigDatum, IFNULL(SUM(tbl.Anzahl),0) AS Anzahl, IFNULL(SUM(CASE WHEN tbl.FrameTyp =1 THEN ((pos.`TotalHeight` * pos.`TotalWidth`) / 1000000) ELSE 0 END),0) AS QM,
	                    IFNULL(AU.Status,0) AS Status,IFNULL(AU.`ChangeDat`,AU.`AngelDat`) AS StatusDat, IFNULL(SUM(tbl.Ist),0) AS Ist, IFNULL(SUM(tbl.Schlecht),0) AS Schlecht, IFNULL(SUM(tbl.Rep),0) AS Rep
                    FROM ( 	SELECT COUNT(DISTINCT pw.ID) AS Anzahl, pos.`Id`, fw.`FrameTyp`, COUNT(CASE WHEN pws.`Gutteil` =0 THEN 1 ELSE NULL END) AS Schlecht, 
		                    COUNT(CASE WHEN pws.`Gutteil` =1 THEN 1 ELSE NULL END) AS Ist,COUNT(CASE WHEN pws.`Gutteil` >1 THEN 1 ELSE NULL END) AS Rep
		                    FROM partwood AS pw
		                    LEFT JOIN partwoodstatus as pws
			                    ON pws.`Partwood_Id` = pw.`Id`
			                    AND pws.`Station_Id` IN(" + string.Join(",", Stationids) + @")
                                AND IFNULL(pws.`ChangeDat`,pws.`AngelDat`) < CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
		                    JOIN framewood AS fw
			                    ON pw.`FrameWood_FrameID` = fw.`FrameID` 
                                AND fw.produce = 1 
		                    JOIN position AS pos
			                    ON fw.`Position_Id` = pos.`Id`
		                    JOIN Auftrag AS AU
			                    ON pos.`OrderNr` = AU.`AuftragsNr`
		                    WHERE @AuNr IN (AU.`AuftragsNr`, AU.`SAPNr`)                            
			                    AND (pw.partmaterial NOT LIKE '%Dummy%' AND pw.partmaterial NOT LIKE '%Rohholz%' 
			                    AND pw.vFamilia_id != 38 
			                    AND NOT pw.vclase_id IN (42,38,1,4) AND NOT pw.vrole_id IN (1,18))
		                    GROUP BY pos.`Id`, fw.`FrameTyp`) AS tbl
                    JOIN position AS pos
	                    ON tbl.`Id` = pos.`Id`
                    JOIN Auftrag AS AU
	                    ON pos.`OrderNr` = AU.`AuftragsNr`
                    GROUP BY pos.`Id`", conn))
            {
                var para_AuNr = cmd.Parameters.Add("@AuNR", MySqlDbType.VarString);
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = MaxDT;
                cmd.CommandTimeout = 360;
                para_AuNr.Value = AuNr;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        PositionSoll pos = new PositionSoll
                        {
                            AuNr = AuNr,
                            SollKanteln = reader.GetInt32("Anzahl"),
                            Sollm2 = reader.GetDecimal("QM"),
                            PosId = reader.GetInt32("Id"),
                            PosPositionsId = reader.GetString("PositionsId"),
                            PosNr = reader.GetString("OrderPosNr"),
                            Rckstellen = Rck,
                            RckstellenGrund = RckGrund
                        };

                        var status = reader.GetInt32("Status");
                        DateTime? profFertigDatum = reader.IsDBNull(reader.GetOrdinal("ProfFertigDatum")) ?
                            null : (DateTime?)reader.GetDateTime(reader.GetOrdinal("ProfFertigDatum"));
                        pos.FertigDatum = profFertigDatum;
                        DateTime StatusDatumZeit = reader.GetDateTime("StatusDat");
                        if ((status >= 100
                             && (StatusDatumZeit - Konstanten.SchichtstartZeitspanne).Date <= MaxDT.Date)
                        || (profFertigDatum.HasValue
                             && profFertigDatum.Value.Date <= MaxDT.Date && profFertigDatum.Value.Date.Year > Konstanten.ignoreNADat.Year))
                        {
                            pos.KantelnRep = 0;
                            pos.KantelnSchlecht = 0;
                            pos.KantelnIst = pos.SollKanteln;
                        }
                        else
                        {
                            pos.KantelnIst = reader.GetInt32("Ist");
                            pos.KantelnRep = reader.GetInt32("Rep");
                            pos.KantelnSchlecht = reader.GetInt32("Schlecht");
                        }

                        solls.Add(pos);
                    }
            }

            return solls.ToArray();
        }
        #endregion
        #region "Update
        private static bool UpdateTageszaehlerProfillierungGeb6(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            int counter = 0;
            bool ret = true;
            bool retry;
            bool Soll = true;
            bool Fertig = true;
            bool Rueck = true;
            bool Vor = true;
            do
            {
                retry = false;
                try
                {
                    if (Soll)
                    {
                        UpdateTageszaehlerProfillierungGeb6Soll(tageszaehler, conn);
                        Soll = false;
                    }
                    if (Fertig)
                    {
                        UpdateTageszaehlerProfillierungGeb6Fertig(tageszaehler, conn);
                        Fertig = false;
                    }
                    if (Rueck)
                    {
                        UpdateTageszaehlerProfillierungGeb6Rueck(tageszaehler, conn);
                        Rueck = false;
                    }
                    if (Vor)
                    {
                        UpdateTageszaehlerProfillierungGeb6Vor(tageszaehler, conn);
                        Vor = false;
                    }
                }
                catch (MySqlException MysqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, MysqlEx.Message + Environment.NewLine + MysqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
                catch (SqlException SqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, SqlEx.Message + Environment.NewLine + SqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
            } while (retry);

            return ret;
        }

        private static void UpdateTageszaehlerProfillierungGeb6Soll(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {

            tageszaehler.Soll = 0;
            tageszaehler.SollM2 = 0m;
            tageszaehler.VorlaufSoll = 0;
            tageszaehler.VorlaufSollM2 = 0m;


        }

        private static void UpdateTageszaehlerProfillierungGeb6Fertig(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.Fertig = 0;
            tageszaehler.FertigM2 = 0m;
            tageszaehler.Vorlauf = 0;
            tageszaehler.VorlaufM2 = 0m;
            tageszaehler.Rueckstand = 0;
            tageszaehler.RueckstandM2 = 0m;
            tageszaehler.FertigSoll = 0;
            tageszaehler.FertigSollM2 = 0m;
        }

        private static void UpdateTageszaehlerProfillierungGeb6Rueck(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.RueckstandBisher = 0;
            tageszaehler.RueckstandBisherM2 = 0m;


        }
        private static void UpdateTageszaehlerProfillierungGeb6Vor(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.VorlaufBisher = 0;
            tageszaehler.VorlaufBisherM2 = 0m;


            tageszaehler.VorlaufBisher += tageszaehler.VorlaufSoll;
            tageszaehler.VorlaufBisherM2 += tageszaehler.VorlaufSollM2;
        }
        #endregion
        #endregion

        #region "Einzelstab"
        #region "HoleFertig"
        public static FertigRet HoleFertigEinzelstabFromAuNr(string AuNr)
        {
            return HoleFertigEinzelstabFromAuNr(AuNr, DateTime.Now);
        }
        public static FertigRet HoleFertigEinzelstabFromAuNr(string AuNr, DateTime MaxDT)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();
                return HoleFertigEinzelstabFromAuNr(AuNr, MaxDT, conn);
            }
        }
        public static FertigRet HoleFertigEinzelstabFromAuNr(string AuNr, MySqlConnection conn)
        {
            return HoleFertigEinzelstabFromAuNr(AuNr, DateTime.Now, conn);
        }
        public static FertigRet HoleFertigEinzelstabFromAuNr(string AuNr, DateTime MaxDT, MySqlConnection conn)
        {
            var ret = new FertigRet()
            {
                Menge = 0,
                M2 = 0m
            };
            using (var cmd = new MySqlCommand(
                @"SELECT IFNULL(SUM(Anzahl),0) AS Anzahl, IFNULL(SUM(CASE WHEN tbl.FrameTyp =1 THEN ((pos.`TotalHeight` * pos.`TotalWidth`) / 1000000) ELSE 0 END),0) AS QM
                FROM(	SELECT pos.`Id`, fw.`FrameTyp`, COUNT(*) AS Anzahl
		                FROM partwood AS pw
		                 JOIN framewood AS fw
			                ON pw.`FrameWood_FrameID` = fw.`FrameID` 
                            AND fw.produce = 1 
		                 JOIN position AS pos
			                ON fw.`Position_Id` = pos.`Id`
		                 JOIN auftrag AS au
			                ON au.`AuftragsNr` = pos.`OrderNr`
		                WHERE @AuNR IN (au.SAPNr, au.AuftragsNr)
                            AND DATE(pos.`EinzelstabFertigDatum`) < DATE(DATE_ADD(@Date, INTERVAL 1 DAY))	
                            AND YEAR(pos.`EinzelstabFertigDatum`) > 2000 
		                GROUP BY pos.`Id`, fw.`FrameTyp`) AS tbl
                LEFT JOIN position AS pos
	                ON tbl.`Id` = pos.`Id`", conn))
            {
                cmd.Parameters.Add("@AuNr", MySqlDbType.VarString).Value = AuNr;
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = MaxDT;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        ret.Menge += reader.GetInt32("Anzahl");
                        ret.M2 += reader.GetDecimal("QM");
                    }
            }
            return ret;
        }

        public static FertigRet HoleFertigEinzelstabFromDT(DateTime date, MySqlConnection conn)
        {
            var tageszaehler = new TageszaehlerBereich
            {
                Datum = date
            };
            UpdateTageszaehlerEinzelstabFertig(tageszaehler, conn);
            return new FertigRet() { Menge = tageszaehler.Fertig, M2 = tageszaehler.FertigM2 };
        }
        #endregion
        #region "Filter"
        public static IEnumerable<string> FilterAuftragListForEinzelstab(IEnumerable<string> Basis)
        {
            return FilterAuftragListForEinzelstab(Basis, AuNR => AuNR);
        }
        public static IEnumerable<T> FilterAuftragListForEinzelstab<T>(IEnumerable<T> Basis, Func<T, string> GetAuNr)
        {
            Basis = FilterAuftragListHalle6(Basis, GetAuNr);

            //Basis = FilterAuftragListAlumat(Basis, GetAuNr, conn);

            return Basis;
        }
        #endregion
        #region "GetSoll"
        public static AuftragSoll GetAuftragSollEinzelstab(string AuNr, MySqlConnection conn)
        {
            return GetAuftragSollProfillierung(AuNr, conn);
        }
        public static ICollection<PositionSoll> GetAuftragSollEinzelstabPosition(string AuNr, MySqlConnection conn, params int[] Stationids)
        {
            return GetAuftragSollEinzelstabPosition(AuNr, DateTime.Now, conn, Stationids);
        }
        public static ICollection<PositionSoll> GetAuftragSollEinzelstabPosition(string AuNr, DateTime MaxDT, MySqlConnection conn, params int[] Stationids)
        {
            var solls = new List<PositionSoll>();

            Wertbau.HoleAuftragrueckstellung(AuNr, conn, out bool Rck, out string RckGrund);

            using (var cmd = new MySqlCommand(
                @"	SELECT pos.`Id`, pos.`PositionsId`, pos.`OrderPosNr`, pos.`EinzelstabFertigDatum`, IFNULL(SUM(tbl.Anzahl),0) AS Anzahl, 
                 IFNULL(SUM(CASE WHEN tbl.`FrameTyp` = 1 THEN (pos.`TotalHeight` * pos.`TotalWidth`)/1000000 ELSE NULL END),
                    (pos.`TotalHeight` * pos.`TotalWidth`)/1000000) AS QM,
	                    IFNULL(AU.Status,0) AS Status,IFNULL(AU.`ChangeDat`,AU.`AngelDat`) AS StatusDat, IFNULL(SUM(tbl.Ist),0) AS Ist, IFNULL(SUM(tbl.Schlecht),0) AS Schlecht, IFNULL(SUM(tbl.Rep),0) AS Rep
                    FROM ( 	SELECT COUNT(DISTINCT pw.ID) AS Anzahl, pos.`Id`, fw.`FrameTyp`, COUNT(CASE WHEN pws.`Gutteil` =0 THEN 1 ELSE NULL END) AS Schlecht, 
		                    COUNT(CASE WHEN pws.`Gutteil` =1 THEN 1 ELSE NULL END) AS Ist,COUNT(CASE WHEN pws.`Gutteil` >1 THEN 1 ELSE NULL END) AS Rep
		                    FROM partwood AS pw
		                    LEFT JOIN partwoodstatus as pws
			                    ON pws.`Partwood_Id` = pw.`Id`
                                AND pws.`Gutteil` >= 1
			                    AND pws.`Station_Id` IN(" + string.Join(",", Stationids) + @")
                                AND IFNULL(pws.`ChangeDat`,pws.`AngelDat`) < CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
		                    JOIN framewood AS fw
			                    ON pw.`FrameWood_FrameID` = fw.`FrameID` 
                                AND fw.produce = 1 
		                    JOIN position AS pos
			                    ON fw.`Position_Id` = pos.`Id`
		                    JOIN Auftrag AS AU
			                    ON pos.`OrderNr` = AU.`AuftragsNr`
		                    WHERE @AuNr IN (AU.`AuftragsNr`, AU.`SAPNr`)                            
			                    AND pw.partmaterial NOT LIKE '%Dummy%' AND pw.partmaterial NOT LIKE '%Rohholz%' 
			                    AND pw.vFamilia_id != 38 
			                    AND NOT pw.vclase_id IN (42,38,1,4) AND NOT pw.vrole_id IN (1,18) 
		                    GROUP BY pos.`Id`, fw.`FrameTyp`) AS tbl
                    JOIN position AS pos
	                    ON tbl.`Id` = pos.`Id`
                    JOIN Auftrag AS AU
	                    ON pos.`OrderNr` = AU.`AuftragsNr`
                    GROUP BY pos.`Id`", conn))
            {
                var para_AuNr = cmd.Parameters.Add("@AuNR", MySqlDbType.VarString);
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = MaxDT;
                cmd.CommandTimeout = 360;
                para_AuNr.Value = AuNr;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        PositionSoll pos = new PositionSoll
                        {
                            AuNr = AuNr,
                            SollKanteln = reader.GetInt32("Anzahl"),
                            Sollm2 = reader.GetDecimal("QM"),
                            PosId = reader.GetInt32("Id"),
                            PosPositionsId = reader.GetString("PositionsId"),
                            PosNr = reader.GetString("OrderPosNr"),
                            Rckstellen = Rck,
                            RckstellenGrund = RckGrund
                        };

                        var status = reader.GetInt32("Status");
                        DateTime? einzelFertigDatum = reader.IsDBNull(reader.GetOrdinal("EinzelstabFertigDatum")) ?
                            null : (DateTime?)reader.GetDateTime(reader.GetOrdinal("EinzelstabFertigDatum"));
                        pos.FertigDatum = einzelFertigDatum;
                        DateTime StatusDatumZeit = reader.GetDateTime("StatusDat");
                        if ((status >= 100
                             && (StatusDatumZeit - Konstanten.SchichtstartZeitspanne).Date <= MaxDT.Date)
                        || (einzelFertigDatum.HasValue
                             && einzelFertigDatum.Value.Date <= MaxDT.Date))
                        {
                            pos.KantelnRep = 0;
                            pos.KantelnSchlecht = 0;
                            pos.KantelnIst = pos.SollKanteln;
                        }
                        else
                        {
                            pos.KantelnIst = reader.GetInt32("Ist");
                            pos.KantelnRep = reader.GetInt32("Rep");
                            pos.KantelnSchlecht = reader.GetInt32("Schlecht");
                        }
                        if (pos.Fertig && einzelFertigDatum == null)
                        {
                            pos.SetFertig = true;
                        }
                        solls.Add(pos);
                    }
            }
            foreach (var pos in solls)
                if (pos.SetFertig)
                    using (var cmd = new MySqlCommand("UPDATE `position` SET `EinzelstabFertigDatum` = DATE(NOW()) WHERE `Id` = @ID;", conn))
                    {
                        cmd.Parameters.Add("@ID", MySqlDbType.Int32).Value = pos.PosId;

                        cmd.ExecuteNonQuery();
                    }
            return solls.ToArray();
        }
        #endregion
        #region "Update
        private static bool UpdateTageszaehlerEinzelstab(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            int counter = 0;
            bool ret = true;
            bool retry;
            bool Soll = true;
            bool Fertig = true;
            bool Rueck = true;
            bool Vor = true;
            do
            {
                retry = false;
                try
                {
                    if (Soll)
                    {
                        UpdateTageszaehlerEinzelstabSoll(tageszaehler, conn);
                        Soll = false;
                    }
                    if (Fertig)
                    {
                        UpdateTageszaehlerEinzelstabFertig(tageszaehler, conn);
                        Fertig = false;
                    }
                    if (Rueck)
                    {
                        UpdateTageszaehlerEinzelstabRueck(tageszaehler, conn);
                        Rueck = false;
                    }
                    if (Vor)
                    {
                        UpdateTageszaehlerEinzelstabVor(tageszaehler, conn);
                        Vor = false;
                    }
                    if (tageszaehler.Datum == DateTime.Now.DatumSchicht())
                        UpdateEinzelstabSetIstAfterEck(conn);
                }
                catch (MySqlException MysqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, MysqlEx.Message + Environment.NewLine + MysqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
                catch (SqlException SqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, SqlEx.Message + Environment.NewLine + SqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
            } while (retry);

            return ret;
        }

        private static void UpdateTageszaehlerEinzelstabSoll(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            IEnumerable<string> SollAuftraege = HoleAuftraegeSollEinzelstab(tageszaehler.Datum, tageszaehler.Datum);

            SollAuftraege = FilterAuftragListForEinzelstab(SollAuftraege);

            #region "GetSoll"
            tageszaehler.Soll = 0;
            tageszaehler.SollM2 = 0m;
            tageszaehler.VorlaufSoll = 0;
            tageszaehler.VorlaufSollM2 = 0m;

            foreach (var AuNr in SollAuftraege)
            {
                var Auf = GetAuftragSollEinzelstab(AuNr, conn);
                tageszaehler.Soll += Auf.SollKanteln;
                tageszaehler.SollM2 += Auf.Sollm2;
                var Ist = HoleFertigEinzelstabFromAuNr(AuNr, tageszaehler.Datum.AddDays(-1), conn);
                tageszaehler.VorlaufSoll += Ist.Menge;
                tageszaehler.VorlaufSollM2 += Ist.M2;
            }

            #endregion
        }
        private static void UpdateTageszaehlerEinzelstabFertig(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.Fertig = 0;
            tageszaehler.FertigM2 = 0m;
            tageszaehler.Vorlauf = 0;
            tageszaehler.VorlaufM2 = 0m;
            tageszaehler.Rueckstand = 0;
            tageszaehler.RueckstandM2 = 0m;
            tageszaehler.FertigSoll = 0;
            tageszaehler.FertigSollM2 = 0m;

            #region "GetPartIDs"
            var positionen = new List<UpdateTageszaehlerProfilierungFertigHelper>();

            using (var cmd = new MySqlCommand(
                @"SELECT IFNULL(pos.`Id`,-1) AS `Id`, IFNULL(COUNT(DISTINCT pw.`Id`),0) AS Anzahl, IFNULL(MAX((pos.`TotalHeight` * pos.`TotalWidth`) / 1000000),0) AS QM, MAX(pos.`EinzelstabFertigDatum`) AS EinzelstabFertigDatum, MAX(IFNULL(au.`cSAPNr`,IFNULL(au.`AuftragsNr`,''))) AS AuftragsNr, MAX(IFNULL( pws.`AngelDat`,pws.`ChangeDat`)) AS FertigDat, MAX(aus.`Einzelstab_ist`) AS `Einzelstab_ist`
		                FROM partwoodstatus AS pws
		                LEFT JOIN partwood AS pw
			                ON pws.`Partwood_Id` = pw.`Id`
		                LEFT JOIN framewood AS fw
			                ON pw.`FrameWood_FrameID` = fw.`FrameID` 
                            AND fw.produce = 1 
		                LEFT JOIN position AS pos
			                ON fw.`Position_Id` = pos.`Id`
                        LEFT JOIN vw_auftrag AS au
	                        ON au.`AuftragsNr` = pos.`OrderNr`   
                        LEFT JOIN `auftragsstatus` as aus
	                        ON aus.`auftrag_Id` = au.`Id`
		                WHERE pws.station_id IN (" + Konstanten.EinzelstabStationen + @") 	
                            AND pws.`Gutteil` >= 1
			                AND IFNULL(pws.`ChangeDat`, pws.`AngelDat`) BETWEEN CONCAT(@Date, ' " + Konstanten.UhrzeitSchichtstart + @"') AND CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
		                GROUP BY pos.`Id`
                        ORDER BY AuftragsNr", conn))
            {
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = tageszaehler.Datum.Date;
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var pos = new UpdateTageszaehlerProfilierungFertigHelper
                        {
                            ID = reader.GetInt32("Id"),
                            Anzahl = reader.GetInt32("Anzahl"),
                            QM = reader.GetDecimal("QM"),
                            QM_Fertig = false,
                            Auftrag = reader.GetString("AuftragsNr"),
                            AuFertigDat = reader.IsDBNull(reader.GetOrdinal("FertigDat")) ? null : (DateTime?)reader.GetDateTime("FertigDat")
                        };

                        if (!reader.IsDBNull(reader.GetOrdinal("EinzelstabFertigDatum")))
                        {
                            pos.QM_Fertig = true;

                            var PFD = reader.GetDateTime("EinzelstabFertigDatum");
                            if (PFD.Date != tageszaehler.Datum.Date)
                                pos.QM = 0;
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("Einzelstab_ist")))
                            pos.AuFertigDat = null;

                        positionen.Add(pos);
                    }
            }
            #endregion

            #region "CheckFertig"
            using (var cmd = new MySqlCommand(
                @"SELECT DISTINCT 1
                FROM position AS pos
                JOIN  framewood AS fw
	                ON fw.`Position_Id` = pos.`Id` 
                    AND fw.produce = 1 
                JOIN partwood AS pw
	                ON pw.`FrameWood_FrameID` = fw.`FrameID`
                LEFT JOIN partwoodstatus AS pws
	                ON pws.`Partwood_Id` = pw.`Id`
                    AND IFNULL(pws.`ChangeDat`,pws.`AngelDat`) < CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
                    AND pws.station_id IN (" + Konstanten.EinzelstabStationen + @") 
                WHERE  pos.ID = @Pos_ID
                    AND pws.ID IS NULL
                    AND fw.frametyp IN (1,2) 
                    AND pw.partmaterial NOT LIKE '%Dummy%' AND pw.partmaterial NOT LIKE '%Rohholz%' 
		            AND  pw.vFamilia_id != 38 
		            AND NOT pw.vclase_id IN (42,38,1,4) AND NOT pw.vrole_id IN (1,18)", conn))
            {
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = tageszaehler.Datum.Date;
                var para_ID = cmd.Parameters.Add("@Pos_ID", MySqlDbType.Int32);
                foreach (var pos in positionen)
                    if (!pos.QM_Fertig)
                    {
                        para_ID.Value = pos.ID;
                        using (var reader = cmd.ExecuteReader())
                            if (reader.Read())
                            {
                                pos.QM = 0;
                                pos.QM_Fertig = true;
                                pos.AuFertigDat = null;
                            }
                    }

            }
            #endregion

            #region "SetFertig"
            using (var cmd = new MySqlCommand(
                @"UPDATE position
                SET `EinzelstabFertigDatum` = @Date
                WHERE `Id` = @Pos_ID", conn))
            {
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = tageszaehler.Datum.Date;
                var para_ID = cmd.Parameters.Add("@Pos_ID", MySqlDbType.Int32);
                foreach (var pos in positionen)
                    if (!pos.QM_Fertig)
                    {
                        para_ID.Value = pos.ID;
                        cmd.ExecuteNonQuery();

                        pos.QM_Fertig = true;
                    }
            }
            #endregion

            #region "CheckFertigAuftrag"
            var auLst = positionen.Where(pos => pos.AuFertigDat.HasValue).GroupBy(pos => pos.Auftrag).Select(pos => new Tuple<string, DateTime>(pos.Key, pos.Max(ipos => ipos.AuFertigDat.Value)));
            List<Tuple<string, DateTime>> auLst_Update = new List<Tuple<string, DateTime>>(auLst.Count());

            using (var cmd = new MySqlCommand(
                @"SELECT DISTINCT 1
                    FROM `auftrag` as au
                    JOIN `position` as pos
	                    ON au.`AuftragsNr` = pos.`OrderNr`
                    LEFT OUTER JOIN framewood on pos.positionsid = framewood.positionsid	
                    WHERE @AuNr IN (`AuftragsNr`,`SAPNr`)  and framewood.frameid is not null AND framewood.produce = 1
                     AND pos.`EinzelstabFertigDatum` IS NULL", conn))
            {
                var para = cmd.Parameters.Add("@AuNr", MySqlDbType.VarString);
                foreach (var au in auLst)
                {
                    para.Value = au.Item1;
                    using (var reader = cmd.ExecuteReader())
                        if (!reader.Read())
                            auLst_Update.Add(au);
                }

            }
            #endregion

            #region "SetFertigAuftrag"
            using (var cmd = new MySqlCommand(
                @" UPDATE `auftragsstatus`
                 SET `Einzelstab_ist` =@Date
                 WHERE `Auftragsnummer` =@AuNr", conn))
            {
                var para_auNr = cmd.Parameters.Add("@AuNr", MySqlDbType.VarString);
                var para_Date = cmd.Parameters.Add("@Date", MySqlDbType.DateTime);

                foreach (var au in auLst_Update)
                {
                    para_auNr.Value = au.Item1;
                    para_Date.Value = au.Item2;

                    cmd.ExecuteNonQuery();
                }

            }
            #endregion

            #region "Rechnen"
            DateTime? Soll = null;
            string Auftrag = "";
            foreach (var pos in positionen)
            {
                tageszaehler.Fertig += pos.Anzahl;
                tageszaehler.FertigM2 += pos.QM;

                if (Auftrag != pos.Auftrag)
                    Soll = Wertbau.HoleEinzelstabDatumDTMySql(pos.Auftrag, true);

                if (Soll.HasValue)
                {
                    DateTime solldat = Soll.Value.Date;
                    DateTime tagdat = tageszaehler.Datum.Date;
                    if (solldat == tagdat)
                    {
                        tageszaehler.FertigSoll += pos.Anzahl;
                        tageszaehler.FertigSollM2 += pos.QM;
                    }
                    else if (solldat < tagdat && solldat.Year > Konstanten.ignoreNADat.Year)
                    {
                        tageszaehler.Rueckstand += pos.Anzahl;
                        tageszaehler.RueckstandM2 += pos.QM;
                    }
                    else if (solldat > tagdat)
                    {
                        tageszaehler.Vorlauf += pos.Anzahl;
                        tageszaehler.VorlaufM2 += pos.QM;
                    }
                }
            }
            #endregion

            #region "NF"

            tageszaehler.FertigNf = 0;

            using (var cmd = new MySqlCommand(
                @"SELECT COUNT(DISTINCT partwood_partid) AS Anzahl
                    FROM partwoodstatusupdate
                    WHERE station_id IN (" + Konstanten.EinzelstabStationen + @") 
                      AND gutteil = 0  
                      AND `AngelDat` BETWEEN CONCAT(@Date, ' " + Konstanten.UhrzeitSchichtstart + @"') AND CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')", conn))
            {
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = tageszaehler.Datum.Date;
                var val = cmd.ExecuteScalar();
                if (val is long)
                    tageszaehler.FertigNf = (int)(long)val;
            }

            tageszaehler.Fertig += tageszaehler.FertigNf;
            #endregion
        }
        private static void UpdateTageszaehlerEinzelstabRueck(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.RueckstandBisher = 0;
            tageszaehler.RueckstandBisherM2 = 0m;

            IEnumerable<string> SollAuftraege = HoleAuftraegeSollEinzelstab(Konstanten.ignoreNADat, tageszaehler.Datum.AddDays(-1), true);

            SollAuftraege = FilterAuftragListForEinzelstab(SollAuftraege);

            SollAuftraege = FilterAuftragListStatus100(SollAuftraege, tageszaehler.Datum, conn);

            foreach (var AuNr in SollAuftraege)
            {
                var PosColl = GetAuftragSollEinzelstabPosition(AuNr, tageszaehler.Datum, conn, Konstanten.EinzelstabStationenArray);
                foreach (var Pos in PosColl)
                {
                    if (Pos.SollKanteln > (Pos.KantelnIst + Pos.KantelnRep))
                    {
                        tageszaehler.RueckstandBisher += Pos.SollKanteln - Pos.KantelnIst - Pos.KantelnRep;
                        tageszaehler.RueckstandBisherM2 += Pos.Sollm2;
                    }
                }
            }

            tageszaehler.RueckstandBisherMitSoll = tageszaehler.RueckstandBisher + tageszaehler.Soll - tageszaehler.FertigSoll - tageszaehler.VorlaufSoll;
            tageszaehler.RueckstandBisherMitSollM2 = tageszaehler.RueckstandBisherM2 + tageszaehler.SollM2 - tageszaehler.FertigSollM2 - tageszaehler.VorlaufSollM2;
        }
        private static void UpdateTageszaehlerEinzelstabVor(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.VorlaufBisher = 0;
            tageszaehler.VorlaufBisherM2 = 0m;

            IEnumerable<string> SollAuftraege = HoleAuftraegeSollEinzelstab(tageszaehler.Datum.AddDays(1), tageszaehler.Datum.AddDays(7));

            SollAuftraege = FilterAuftragListForEinzelstab(SollAuftraege);

            foreach (var Au in SollAuftraege)
            {
                var Ist = HoleFertigEinzelstabFromAuNr(Au, tageszaehler.Datum.AddDays(-1), conn);
                tageszaehler.VorlaufBisher += Ist.Menge;
                tageszaehler.VorlaufBisherM2 += Ist.M2;
            }
            tageszaehler.VorlaufBisher += tageszaehler.VorlaufSoll;
            tageszaehler.VorlaufBisherM2 += tageszaehler.VorlaufSollM2;
        }
        private static void UpdateEinzelstabSetIstAfterEck(MySqlConnection conn)
        {
            using (var cmd = new MySqlCommand("UPDATE `auftragsstatus` SET `Einzelstab_ist` = '1901-01-01' WHERE `Einzelstab_ist` IS NULL AND `Eckverbindung_ist` IS NOT NULL;", conn))
            {
                cmd.ExecuteNonQuery();
            }
        }
        #endregion
        #endregion

        #region "Eckverbindung"
        #region "HoleFertig"
        public static FertigRet HoleFertigEckverbindungFromAuNr(string AuNr)
        {
            return HoleFertigEckverbindungFromAuNr(AuNr, DateTime.Now);
        }
        public static FertigRet HoleFertigEckverbindungFromAuNr(string AuNr, DateTime MaxDT)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();
                return HoleFertigEckverbindungFromAuNr(AuNr, MaxDT, conn);
            }
        }
        public static FertigRet HoleFertigEckverbindungFromAuNr(string AuNr, MySqlConnection conn)
        {
            return HoleFertigEckverbindungFromAuNr(AuNr, DateTime.Now, conn);
        }
        public static FertigRet HoleFertigEckverbindungFromAuNr(string AuNr, DateTime MaxDT, MySqlConnection conn)
        {
            var ret = new FertigRet()
            {
                Menge = 0,
                M2 = 0m
            };
            using (var cmd = new MySqlCommand(
                @"SELECT IFNULL(SUM(Anzahl),0) AS Anzahl, IFNULL(SUM(QM),0) AS QM
                FROM ( SELECT pos.`Id`,COUNT(*) AS Anzahl, 
		                MAX(CASE WHEN NOT EXISTS(SELECT * 
								                FROM framewood AS fwi 
								                WHERE fwi.`Position_Id` = pos.`Id` 
                                                  AND fwi.produce = 1 
                                                  AND (		`Bereich_Eckverbindung` IS NULL 
										                OR `Bereich_Eckverbindung` > CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"'))) THEN (pos.`TotalHeight` * pos.`TotalWidth`) / 1000000 ELSE 0 END ) AS QM
		                FROM Position AS pos
		                JOIN framewood AS fw 
			                ON fw.`Position_Id` = pos.`Id` 
                            AND fw.produce = 1 
                        JOIN auftrag AS au
			                ON au.`AuftragsNr` = pos.`OrderNr`
		                WHERE fw.`Bereich_Eckverbindung` < CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
		                  AND fw.`FrameTyp` IN (1,2)
                          AND @AuNR IN (au.SAPNr, au.AuftragsNr)
		                GROUP BY pos.`Id`) as tbl
                JOIN Position AS pos
	                ON tbl.ID = pos.id;", conn))
            {
                cmd.Parameters.Add("@AuNr", MySqlDbType.VarString).Value = AuNr;
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = MaxDT;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        ret.Menge += reader.GetInt32("Anzahl");
                        ret.M2 += reader.GetDecimal("QM");
                    }
            }
            return ret;
        }
        #endregion
        #region "Filter"
        public static IEnumerable<string> FilterAuftragListForEckverbindung(IEnumerable<string> Basis)
        {
            return FilterAuftragListForEckverbindung(Basis, AuNR => AuNR);
        }
        public static IEnumerable<T> FilterAuftragListForEckverbindung<T>(IEnumerable<T> Basis, Func<T, string> GetAuNr)
        {
            Basis = FilterAuftragListHalle6(Basis, GetAuNr);

            //Basis = FilterAuftragListHalle6Marked(Basis, GetAuNr, conn);

            return Basis;
        }
        #endregion
        #region "GetSoll"
        public static AuftragSoll GetAuftragSollEckverbindung(string AuNr, MySqlConnection conn)
        {
            AuftragSoll ret = new AuftragSoll() { AuNr = AuNr };

            using (var cmd = new MySqlCommand(
                @"SELECT `Quadratmeter` AS QM,`Anzahl_vierecke` AS Anzahl FROM `auftragsstatus` WHERE `Auftragsnummer` LIKE CONCAT(@AuNr,'%')", conn))
            {
                var para_AuNr = cmd.Parameters.Add("@AuNR", MySqlDbType.VarString);

                para_AuNr.Value = AuNr;
                using (var reader = cmd.ExecuteReader())
                    if (reader.Read())
                    {
                        ret.SollKanteln = reader.GetInt32("Anzahl");
                        ret.Sollm2 = reader.GetDecimal("QM");
                    }
            }

            return ret;
        }

        public static ICollection<PositionSollViereck> GetAuftragSollEckverbindungPosition(string AuNr, MySqlConnection conn)
        {
            return GetAuftragSollEckverbindungPosition(AuNr, DateTime.Now, true, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollEckverbindungPosition(string AuNr, bool Ende, MySqlConnection conn)
        {
            return GetAuftragSollEckverbindungPosition(AuNr, DateTime.Now, Ende, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollEckverbindungPosition(string AuNr, DateTime MaxDT, MySqlConnection conn)
        {
            return GetAuftragSollEckverbindungPosition(AuNr, MaxDT, true, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollEckverbindungPosition(string AuNr, DateTime MaxDT, bool Ende, MySqlConnection conn)
        {
            var ret = new List<PositionSollViereck>();

            Wertbau.HoleAuftragrueckstellung(AuNr, conn, out bool Rck, out string RckGrund);

            var Bereich = Ende ? "`Bereich_Eckverbindung`" : "`Bereich_Eckverbindung_Start`";

            if (AuNr == "186264.16")
                Console.WriteLine("");

            using (var cmd = new MySqlCommand(
                @"SELECT pos.`Id`, pos.`PositionsId`, pos.`ProfileSystem`,pos.`OrderPosNr`,pos.`Instanz`,pos.`Description`,a.`Prod_Jahr`,a.`Prod_KW`
                    ,MAX(fw.`timberkind`) AS timberkind,MAX(fw.`CoatingInside`) AS CoatingInside,MAX(fw.`CoatingOutside`) AS CoatingOutside
		            ,IFNULL(SUM(CASE WHEN fw.`FrameTyp` = 1 THEN (pos.`TotalHeight` * pos.`TotalWidth`)/1000000 ELSE NULL END),
                    (pos.`TotalHeight` * pos.`TotalWidth`)/1000000) AS m2Soll
		            ,COUNT(CASE WHEN fw.`FrameTyp` = 1 THEN 1 ELSE NULL END) AS RahmenSoll
                    ,COUNT(CASE WHEN fw_s.`FrameTyp` = 1 THEN 1 ELSE NULL END) AS RahmenIst
                    ,COUNT(CASE WHEN fw.`FrameTyp` = 2 THEN 1 ELSE NULL END) AS FluegelSoll
                    ,COUNT(CASE WHEN fw_s.`FrameTyp` = 2 THEN 1 ELSE NULL END) AS FluegelIst
                    ,MAX(fw." + (Bereich) + @") AS FertigDat
            FROM Auftrag AS a
            JOIN Position AS pos
	            ON pos.`OrderNr` = a.`AuftragsNr`
            JOIN framewood AS fw 
	            ON fw.`Position_Id` = pos.`Id` 
                AND fw.produce = 1 
            LEFT JOIN framewood AS fw_s
	            ON fw.`Id` = fw_s.`Id` 
                AND fw_s." + (Bereich) + @" < CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
            WHERE @AuNr IN (a.`AuftragsNr`, a.`SAPNr`)
              AND fw.`FrameTyp` IN (1,2)
            GROUP BY pos.`Id`;", conn))
            {
                cmd.Parameters.Add("@AuNR", MySqlDbType.VarString).Value = AuNr;
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = MaxDT;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var Pos = new PositionSollViereck()
                        {
                            AuNr = AuNr,

                            PosId = reader.GetInt32("Id"),
                            PosPositionsId = reader.GetString("PositionsId"),
                            PosNr = reader.GetString("OrderPosNr"),
                            InstanzNr = reader.GetString("Instanz"),

                            Beschreibung = reader.GetString("Description"),
                            System = reader.GetString("ProfileSystem"),
                            HolzArt = reader.GetString("timberkind"),
                            FarbeAussen = reader.GetString("CoatingOutside"),
                            FarbeInnen = reader.GetString("CoatingInside"),
                            Sollm2 = reader.GetDecimal("m2Soll"),
                            SollRahmen = reader.GetInt32("RahmenSoll"),
                            IstRahmen = reader.GetInt32("RahmenIst"),
                            SollFluegel = reader.GetInt32("FluegelSoll"),
                            IstFluegel = reader.GetInt32("FluegelIst"),
                            ProdKw = reader.GetInt32("Prod_KW"),
                            ProdJahr = reader.GetInt32("Prod_Jahr"),
                            Rckstellen = Rck,
                            RckstellenGrund = RckGrund
                        };

                        if (Pos.Fertig)
                        {
                            Pos.Istm2 = Pos.Sollm2;
                            Pos.FertigDat = reader.GetDateTime("FertigDat");

                        }
                        else
                            Pos.FertigDat = null;

                        ret.Add(Pos);
                    }

            }

            return ret;
        }
        #endregion
        #region "Update"
        private static bool UpdateTageszaehlerEckverbindung(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            int counter = 0;
            bool ret = true;
            bool retry;
            bool Soll = true;
            bool Fertig = true;
            bool Rueck = true;
            bool Vor = true;
            do
            {
                retry = false;
                try
                {
                    if (Soll)
                    {
                        UpdateTageszaehlerEckverbindungSoll(tageszaehler, conn);
                        Soll = false;
                    }
                    if (Fertig)
                    {
                        UpdateTageszaehlerEckverbindungFertig(tageszaehler, conn);
                        Fertig = false;
                    }
                    if (Rueck)
                    {
                        UpdateTageszaehlerEckverbindungRueck(tageszaehler, conn);
                        Rueck = false;
                    }
                    if (Vor)
                    {
                        UpdateTageszaehlerEckverbindungVor(tageszaehler, conn);
                        Vor = false;
                    }
                }
                catch (MySqlException MysqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, MysqlEx.Message + Environment.NewLine + MysqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
                catch (SqlException SqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, SqlEx.Message + Environment.NewLine + SqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
            } while (retry);

            return ret;
        }

        private static void UpdateTageszaehlerEckverbindungSoll(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            IEnumerable<string> SollAuftraege = HoleAuftraegeSollEckverbindung(tageszaehler.Datum, tageszaehler.Datum, tageszaehler.Fertigungsbereich);

            SollAuftraege = FilterAuftragListForEckverbindung(SollAuftraege);

            #region "GetSoll"
            tageszaehler.Soll = 0;
            tageszaehler.SollM2 = 0m;
            tageszaehler.VorlaufSoll = 0;
            tageszaehler.VorlaufSollM2 = 0m;

            foreach (var AuNr in SollAuftraege)
            {
                var Auf = GetAuftragSollEckverbindung(AuNr, conn);
                tageszaehler.Soll += Auf.SollKanteln;
                tageszaehler.SollM2 += Auf.Sollm2;
                var Ist = HoleFertigEckverbindungFromAuNr(AuNr, tageszaehler.Datum.AddDays(-1), conn);
                tageszaehler.VorlaufSoll += Ist.Menge;
                tageszaehler.VorlaufSollM2 += Ist.M2;
            }
            #endregion
        }
        private static void UpdateTageszaehlerEckverbindungFertig(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.Fertig = 0;
            tageszaehler.FertigM2 = 0m;
            tageszaehler.Vorlauf = 0;
            tageszaehler.VorlaufM2 = 0m;
            tageszaehler.Rueckstand = 0;
            tageszaehler.RueckstandM2 = 0m;
            tageszaehler.FertigSoll = 0;
            tageszaehler.FertigSollM2 = 0m;
            
            #region "Anzahl/m²"
            using (var cmd = new MySqlCommand(
                @"SELECT IFNULL(au.`cSAPNr`,au.`AuftragsNr`) AS `AuNr`,SUM(Anzahl) AS Anzahl, SUM(QM) AS QM
                FROM ( SELECT pos.`Id`,COUNT(*) AS Anzahl, 
		                MAX(CASE WHEN NOT EXISTS(SELECT * 
								                FROM framewood AS fwi 
								                WHERE fwi.`Position_Id` = pos.`Id` 
                                                  AND fwi.produce = 1 
                                                  AND (		`Bereich_Eckverbindung` IS NULL 
										                OR `Bereich_Eckverbindung` > CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"'))) THEN (pos.`TotalHeight` * pos.`TotalWidth`) / 1000000 ELSE 0 END ) AS QM
		                FROM Position AS pos
		                JOIN framewood AS fw 
			                ON fw.`Position_Id` = pos.`Id` 
                            AND fw.produce = 1 
		                WHERE fw.`Bereich_Eckverbindung` BETWEEN CONCAT(@Date, ' " + Konstanten.UhrzeitSchichtstart + @"') AND CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
		                  AND fw.`FrameTyp` IN (1,2)
		                GROUP BY pos.`Id`) as tbl
                JOIN Position AS pos
	                ON tbl.ID = pos.id
                JOIN vw_auftrag AS au
	                ON au.`AuftragsNr` = pos.`OrderNr`               
                GROUP BY AuNr;", conn))
            {
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = tageszaehler.Datum.Date;
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        int Anz = reader.GetInt32("Anzahl");
                        decimal QM = reader.GetDecimal("QM");
                       
                        if (!reader.IsDBNull(reader.GetOrdinal("AuNr")))
                        {
                            var AuNr = reader.GetString("AuNr");
                            string KZ = Wertbau.HoleKennzeichenMySQL(AuNr);

                            if (KZ.Length == 0)
                                continue;

                            if (KZ == "HO")
                            {
                                if (tageszaehler.Fertigungsbereich != Fertigungsbereich.Eckverbindung && tageszaehler.Fertigungsbereich != Fertigungsbereich.Eckverbindung_HO)
                                    continue;
                            } 

                            if (KZ == "NGWA" || KZ == "HA")
                            {
                                if (tageszaehler.Fertigungsbereich != Fertigungsbereich.Eckverbindung && tageszaehler.Fertigungsbereich != Fertigungsbereich.Eckverbindung_NGWA)
                                    continue;
                            }

                            DateTime? Soll = Wertbau.HoleEckverbundDatumDTMySql(AuNr);
                            if (Soll.HasValue)
                            {
                                DateTime solldat = Soll.Value.Date;
                                DateTime tagdat = tageszaehler.Datum.Date;
                                if (solldat == tagdat)
                                {
                                    tageszaehler.FertigSoll += Anz;
                                    tageszaehler.FertigSollM2 += QM;
                                }
                                else if (solldat < tagdat && solldat.Year > Konstanten.ignoreNADat.Year)
                                {
                                    tageszaehler.Rueckstand += Anz;
                                    tageszaehler.RueckstandM2 += QM;
                                }
                                else if (solldat > tagdat)
                                {
                                    tageszaehler.Vorlauf += Anz;
                                    tageszaehler.VorlaufM2 += QM;
                                }
                            }
                        }

                        tageszaehler.Fertig += Anz;
                        tageszaehler.FertigM2 += QM;
                    }
            }
            #endregion

            #region "NF"
            // tageszaehler.Fertig_NF = -1;
            #endregion
        }
        private static void UpdateTageszaehlerEckverbindungRueck(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.RueckstandBisher = 0;
            tageszaehler.RueckstandBisherM2 = 0m;
            IEnumerable<string> SollAuftraege = HoleAuftraegeSollEckverbindung(Konstanten.ignoreNADat, tageszaehler.Datum.AddDays(-1),tageszaehler.Fertigungsbereich, true);

            SollAuftraege = FilterAuftragListStatus100(SollAuftraege, tageszaehler.Datum, conn);

            SollAuftraege = FilterAuftragListForEckverbindung(SollAuftraege);

            foreach (var AuNr in SollAuftraege)
            {
                var pos_coll = GetAuftragSollEckverbindungPosition(AuNr, tageszaehler.Datum, conn);
                foreach (var pos in pos_coll)
                {
                    string KZ = Wertbau.HoleKennzeichenMySQL(pos.AuNr);

                    if (KZ.Length == 0)
                        continue;

                    if (KZ == "HO")
                    {
                        if (tageszaehler.Fertigungsbereich != Fertigungsbereich.Eckverbindung && tageszaehler.Fertigungsbereich != Fertigungsbereich.Eckverbindung_HO)
                            continue;
                    }

                    if (KZ == "NGWA" || KZ == "HA")
                    {
                        if (tageszaehler.Fertigungsbereich != Fertigungsbereich.Eckverbindung && tageszaehler.Fertigungsbereich != Fertigungsbereich.Eckverbindung_NGWA)
                            continue;
                    }

                    if (!pos.Fertig)
                    {
                        tageszaehler.RueckstandBisher += pos.SollGesamt - pos.IstGesamt;
                        tageszaehler.RueckstandBisherM2 += pos.Sollm2;
                    }
                }
            }

            tageszaehler.RueckstandBisherMitSoll = tageszaehler.RueckstandBisher + tageszaehler.Soll - tageszaehler.FertigSoll - tageszaehler.VorlaufSoll;
            tageszaehler.RueckstandBisherMitSollM2 = tageszaehler.RueckstandBisherM2 + tageszaehler.SollM2 - tageszaehler.FertigSollM2 - tageszaehler.VorlaufSollM2;
        }
        private static void UpdateTageszaehlerEckverbindungVor(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.VorlaufBisher = 0;
            tageszaehler.VorlaufBisherM2 = 0m;

            IEnumerable<string> SollAuftraege = HoleAuftraegeSollEckverbindung(tageszaehler.Datum.AddDays(1), tageszaehler.Datum.AddDays(7),tageszaehler.Fertigungsbereich );

            SollAuftraege = FilterAuftragListForEckverbindung(SollAuftraege);

            foreach (var Au in SollAuftraege)
            {               
                var Ist = HoleFertigEckverbindungFromAuNr(Au, tageszaehler.Datum.AddDays(-1), conn);
                tageszaehler.VorlaufBisher += Ist.Menge;
                tageszaehler.VorlaufBisherM2 += Ist.M2;
            }
            tageszaehler.VorlaufBisher += tageszaehler.VorlaufSoll;
            tageszaehler.VorlaufBisherM2 += tageszaehler.VorlaufSollM2;
        }
        #endregion
        #endregion

        #region "Oberfläche"
        #region "HoleFertig"
        public static FertigRet HoleFertigOberflaecheFromAuNr(string AuNr)
        {
            return HoleFertigOberflaecheFromAuNr(AuNr, DateTime.Now);
        }
        public static FertigRet HoleFertigOberflaecheFromAuNr(string AuNr, DateTime MaxDT)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();
                return HoleFertigOberflaecheFromAuNr(AuNr, MaxDT, conn);
            }
        }
        public static FertigRet HoleFertigOberflaecheFromAuNr(string AuNr, MySqlConnection conn)
        {
            return HoleFertigOberflaecheFromAuNr(AuNr, DateTime.Now, conn);
        }
        public static FertigRet HoleFertigOberflaecheFromAuNr(string AuNr, DateTime MaxDT, MySqlConnection conn)
        {
            var ret = new FertigRet()
            {
                Menge = 0,
                M2 = 0m
            };
            using (var cmd = new MySqlCommand(
                @"SELECT IFNULL(SUM(Anzahl),0) AS Anzahl, IFNULL(SUM(QM),0) AS QM
                FROM ( SELECT pos.`Id`,COUNT(*) AS Anzahl, 
		                MAX(CASE WHEN NOT EXISTS(SELECT * 
								                FROM framewood AS fwi 
								                WHERE fwi.`Position_Id` = pos.`Id` 
                                                  AND fwi.produce = 1 
                                                  AND (		`Bereich_Oberflaeche` IS NULL 
										                OR `Bereich_Oberflaeche` > CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"'))) THEN (pos.`TotalHeight` * pos.`TotalWidth`) / 1000000 ELSE 0 END ) AS QM
		                FROM Position AS pos
		                JOIN framewood AS fw 
			                ON fw.`Position_Id` = pos.`Id` 
                            AND fw.produce = 1 
                        JOIN auftrag AS au
			                ON au.`AuftragsNr` = pos.`OrderNr`
		                WHERE fw.`Bereich_Oberflaeche` < CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
		                  AND fw.`FrameTyp` IN (1,2)
                          AND @AuNR IN (au.SAPNr, au.AuftragsNr)
		                GROUP BY pos.`Id`) as tbl
                JOIN Position AS pos
	                ON tbl.ID = pos.id;", conn))
            {
                cmd.Parameters.Add("@AuNr", MySqlDbType.VarString).Value = AuNr;
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = MaxDT;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        ret.Menge += reader.GetInt32("Anzahl");
                        ret.M2 += reader.GetDecimal("QM");
                    }
            }
            return ret;
        }
        #endregion
        #region "Filter
        public static IEnumerable<string> FilterAuftragListForOberflaeche(IEnumerable<string> Basis)
        {
            return FilterAuftragListForOberflaeche(Basis, AuNR => AuNR);
        }
        public static IEnumerable<T> FilterAuftragListForOberflaeche<T>(IEnumerable<T> Basis, Func<T, string> GetAuNr)
        {
            return Basis;
        }
        #endregion
        #region "Getsoll"
        public static AuftragSoll GetAuftragSollOberflaeche(string AuNr, MySqlConnection conn)
        {
            return GetAuftragSollEckverbindung(AuNr, conn);
        }

        public static ICollection<PositionSollViereck> GetAuftragSollOberflaechePosition(string AuNr, MySqlConnection conn)
        {
            return GetAuftragSollOberflaechePosition(AuNr, DateTime.Now, true, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollOberflaechePosition(string AuNr, bool Ende, MySqlConnection conn)
        {
            return GetAuftragSollOberflaechePosition(AuNr, DateTime.Now, Ende, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollOberflaechePosition(string AuNr, DateTime MaxDT, MySqlConnection conn)
        {
            return GetAuftragSollOberflaechePosition(AuNr, MaxDT, true, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollOberflaechePosition(string AuNr, DateTime MaxDT, bool Ende, MySqlConnection conn)
        {
            var ret = new List<PositionSollViereck>();

            Wertbau.HoleAuftragrueckstellung(AuNr, conn, out bool Rck, out string RckGrund);

            var Bereich = Ende ? "`Bereich_Oberflaeche`" : "`Bereich_Oberflaeche_Start`";
            using (var cmd = new MySqlCommand(
                @"SELECT pos.`Id`, pos.`PositionsId`, pos.`ProfileSystem`,pos.`OrderPosNr`,pos.`Instanz`,pos.`Description`,a.`Prod_Jahr`,a.`Prod_KW`
                    ,MAX(fw.`timberkind`) AS timberkind,MAX(fw.`CoatingInside`) AS CoatingInside,MAX(fw.`CoatingOutside`) AS CoatingOutside
		            ,IFNULL(SUM(CASE WHEN fw.`FrameTyp` = 1 THEN (pos.`TotalHeight` * pos.`TotalWidth`)/1000000 ELSE NULL END),
                    (pos.`TotalHeight` * pos.`TotalWidth`)/1000000) AS m2Soll
		            ,COUNT(CASE WHEN fw.`FrameTyp` = 1 THEN 1 ELSE NULL END) AS RahmenSoll
                    ,COUNT(CASE WHEN fw_s.`FrameTyp` = 1 THEN 1 ELSE NULL END) AS RahmenIst
                    ,COUNT(CASE WHEN fw.`FrameTyp` = 2 THEN 1 ELSE NULL END) AS FluegelSoll
                    ,COUNT(CASE WHEN fw_s.`FrameTyp` = 2 THEN 1 ELSE NULL END) AS FluegelIst
                    ,MAX(fw." + (Bereich) + @") AS FertigDat
            FROM Auftrag AS a
            JOIN Position AS pos
	            ON pos.`OrderNr` = a.`AuftragsNr`
            JOIN framewood AS fw 
	            ON fw.`Position_Id` = pos.`Id` 
                AND fw.produce = 1 
            LEFT JOIN framewood AS fw_s
	            ON fw.`Id` = fw_s.`Id` 
                AND fw_s." + (Bereich) + @" < CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
            WHERE @AuNr IN (a.`AuftragsNr`, a.`SAPNr`)
              AND fw.`FrameTyp` IN (1,2)
            GROUP BY pos.`Id`;", conn))
            {
                cmd.Parameters.Add("@AuNR", MySqlDbType.VarString).Value = AuNr;
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = MaxDT;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var Pos = new PositionSollViereck()
                        {
                            AuNr = AuNr,
                            PosId = reader.GetInt32("Id"),
                            PosPositionsId = reader.GetString("PositionsId"),
                            PosNr = reader.GetString("OrderPosNr"),
                            InstanzNr = reader.GetString("Instanz"),

                            Beschreibung = reader.GetString("Description"),
                            System = reader.GetString("ProfileSystem"),
                            HolzArt = reader.GetString("timberkind"),
                            FarbeAussen = reader.GetString("CoatingOutside"),
                            FarbeInnen = reader.GetString("CoatingInside"),
                            Sollm2 = reader.GetDecimal("m2Soll"),
                            SollRahmen = reader.GetInt32("RahmenSoll"),
                            IstRahmen = reader.GetInt32("RahmenIst"),
                            SollFluegel = reader.GetInt32("FluegelSoll"),
                            IstFluegel = reader.GetInt32("FluegelIst"),
                            ProdKw = reader.GetInt32("Prod_KW"),
                            ProdJahr = reader.GetInt32("Prod_Jahr"),
                            Rckstellen = Rck,
                            RckstellenGrund = RckGrund
                        };
                        if (Pos.Fertig)
                        {
                            Pos.Istm2 = Pos.Sollm2;
                            Pos.FertigDat = reader.GetDateTime("FertigDat");
                        }
                        else
                            Pos.FertigDat = null;

                        ret.Add(Pos);
                    }

            }

            return ret;
        }
        #endregion
        #region "Update"
        private static bool UpdateTageszaehlerOberflaeche(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            int counter = 0;
            bool ret = true;
            bool retry;
            bool Soll = true;
            bool Fertig = true;
            bool Rueck = true;
            bool Vor = true;
            do
            {
                retry = false;
                try
                {
                    if (Soll)
                    {
                        UpdateTageszaehlerOberflaecheSoll(tageszaehler, conn);
                        Soll = false;
                    }
                    if (Fertig)
                    {
                        UpdateTageszaehlerOberflaecheFertig(tageszaehler, conn);
                        Fertig = false;
                    }
                    if (Rueck)
                    {
                        UpdateTageszaehlerOberflaecheRueck(tageszaehler, conn);
                        Rueck = false;
                    }
                    if (Vor)
                    {
                        UpdateTageszaehlerOberflaecheVor(tageszaehler, conn);
                        Vor = false;
                    }
                }
                catch (MySqlException MysqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, MysqlEx.Message + Environment.NewLine + MysqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
                catch (SqlException SqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, SqlEx.Message + Environment.NewLine + SqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
            } while (retry);

            return ret;
        }

        private static void UpdateTageszaehlerOberflaecheSoll(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            IEnumerable<string> SollAuftraege = HoleAuftraegeSollOberflaeche(tageszaehler.Datum, tageszaehler.Datum);

            SollAuftraege = FilterAuftragListForOberflaeche(SollAuftraege);

            #region "GetSoll"
            tageszaehler.Soll = 0;
            tageszaehler.SollM2 = 0m;
            tageszaehler.VorlaufSoll = 0;
            tageszaehler.VorlaufSollM2 = 0m;
            foreach (var AuNr in SollAuftraege)
            {
                var Auf = GetAuftragSollOberflaeche(AuNr, conn);
                tageszaehler.Soll += Auf.SollKanteln;
                tageszaehler.SollM2 += Auf.Sollm2;
                var Ist = HoleFertigOberflaecheFromAuNr(AuNr, tageszaehler.Datum.AddDays(-1), conn);
                tageszaehler.VorlaufSoll += Ist.Menge;
                tageszaehler.VorlaufSollM2 += Ist.M2;
            }
            #endregion
        }
        private static void UpdateTageszaehlerOberflaecheFertig(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.Fertig = 0;
            tageszaehler.FertigM2 = 0m;
            tageszaehler.Vorlauf = 0;
            tageszaehler.VorlaufM2 = 0m;
            tageszaehler.Rueckstand = 0;
            tageszaehler.RueckstandM2 = 0m;
            tageszaehler.FertigSoll = 0;
            tageszaehler.FertigSollM2 = 0m;

            #region "Anzahl/m²"
            using (var cmd = new MySqlCommand(
                @"SELECT IFNULL(au.`cSAPNr`,au.`AuftragsNr`) AS `AuNr`,SUM(Anzahl) AS Anzahl, SUM(QM) AS QM
                FROM ( SELECT pos.`Id`,COUNT(*) AS Anzahl, 
		                MAX(CASE WHEN NOT EXISTS(SELECT * 
								                FROM framewood AS fwi 
								                WHERE fwi.`Position_Id` = pos.`Id` 
                                                  AND fwi.produce = 1 
                                                  AND (		`Bereich_Oberflaeche` IS NULL 
										                OR `Bereich_Oberflaeche` > CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"'))) THEN (pos.`TotalHeight` * pos.`TotalWidth`) / 1000000 ELSE 0 END ) AS QM
		                FROM Position AS pos
		                JOIN framewood AS fw 
			                ON fw.`Position_Id` = pos.`Id` 
                            AND fw.produce = 1 
		                WHERE fw.`Bereich_Oberflaeche` BETWEEN CONCAT(@Date, ' " + Konstanten.UhrzeitSchichtstart + @"') AND CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
		                  AND fw.`FrameTyp` IN (1,2)
		                GROUP BY pos.`Id`) as tbl
                JOIN Position AS pos
	                ON tbl.ID = pos.id
                JOIN vw_auftrag AS au
	                ON au.`AuftragsNr` = pos.`OrderNr`                
                GROUP BY AuNr;", conn))
            {
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = tageszaehler.Datum.Date;
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        int Anz = reader.GetInt32("Anzahl");
                        decimal QM = reader.GetDecimal("QM");
                        tageszaehler.Fertig += Anz;
                        tageszaehler.FertigM2 += QM;
                        if (!reader.IsDBNull(reader.GetOrdinal("AuNr")))
                        {
                            var AuNr = reader.GetString("AuNr");
                            DateTime? Soll = Wertbau.HoleOberflaecheDatumDTMySql(AuNr);
                            if (Soll.HasValue)
                            {
                                DateTime solldat = Soll.Value.Date;
                                DateTime tagdat = tageszaehler.Datum.Date;
                                if (solldat == tagdat)
                                {
                                    tageszaehler.FertigSoll += Anz;
                                    tageszaehler.FertigSollM2 += QM;
                                }
                                else if (solldat < tagdat && solldat.Year > Konstanten.ignoreNADat.Year)
                                {
                                    tageszaehler.Rueckstand += Anz;
                                    tageszaehler.RueckstandM2 += QM;
                                }
                                else if (solldat > tagdat)
                                {
                                    tageszaehler.Vorlauf += Anz;
                                    tageszaehler.VorlaufM2 += QM;
                                }
                            }
                        }
                    }
            }
            #endregion

            #region "NF"
            // tageszaehler.Fertig_NF = -1;
            #endregion
        }
        private static void UpdateTageszaehlerOberflaecheRueck(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.RueckstandBisher = 0;
            tageszaehler.RueckstandBisherM2 = 0m;
            IEnumerable<string> SollAuftraege = HoleAuftraegeSollOberflaeche(Konstanten.ignoreNADat, tageszaehler.Datum.AddDays(-1), true);

            SollAuftraege = FilterAuftragListStatus100(SollAuftraege, tageszaehler.Datum, conn);

            SollAuftraege = FilterAuftragListForOberflaeche(SollAuftraege);

            foreach (var AuNr in SollAuftraege)
            {
                var pos_coll = GetAuftragSollOberflaechePosition(AuNr, tageszaehler.Datum, conn);
                foreach (var pos in pos_coll)
                {
                    if (!pos.Fertig)
                    {
                        tageszaehler.RueckstandBisher += pos.SollGesamt - pos.IstGesamt;
                        tageszaehler.RueckstandBisherM2 += pos.Sollm2;
                    }
                }
            }

            tageszaehler.RueckstandBisherMitSoll = tageszaehler.RueckstandBisher + tageszaehler.Soll - tageszaehler.FertigSoll - tageszaehler.VorlaufSoll;
            tageszaehler.RueckstandBisherMitSollM2 = tageszaehler.RueckstandBisherM2 + tageszaehler.SollM2 - tageszaehler.FertigSollM2 - tageszaehler.VorlaufSollM2;
        }
        private static void UpdateTageszaehlerOberflaecheVor(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.VorlaufBisher = 0;
            tageszaehler.VorlaufBisherM2 = 0m;

            IEnumerable<string> SollAuftraege = HoleAuftraegeSollOberflaeche(tageszaehler.Datum.AddDays(1), tageszaehler.Datum.AddDays(7));

            SollAuftraege = FilterAuftragListForOberflaeche(SollAuftraege);

            foreach (var Au in SollAuftraege)
            {
                var Ist = HoleFertigOberflaecheFromAuNr(Au, tageszaehler.Datum.AddDays(-1), conn);
                tageszaehler.VorlaufBisher += Ist.Menge;
                tageszaehler.VorlaufBisherM2 += Ist.M2;
            }
            tageszaehler.VorlaufBisher += tageszaehler.VorlaufSoll;
            tageszaehler.VorlaufBisherM2 += tageszaehler.VorlaufSollM2;
        }
        #endregion
        #endregion

        #region "AluSchale"
        #region "HoleFertig"
        public static FertigRet HoleFertigAluschaleFromAuNr(string AuNr)
        {
            return HoleFertigAluschaleFromAuNr(AuNr, DateTime.Now);
        }
        public static FertigRet HoleFertigAluschaleFromAuNr(string AuNr, DateTime MaxDT)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            using (var Pconn = new SqlConnection(Konstanten.PufferConnectionstring))
            {
                conn.Open();
                Pconn.Open();
                return HoleFertigAluschaleFromAuNr(AuNr, MaxDT, conn, Pconn);
            }
        }
        public static FertigRet HoleFertigAluschaleFromAuNr(string AuNr, MySqlConnection conn, SqlConnection Pconn)
        {
            return HoleFertigAluschaleFromAuNr(AuNr, DateTime.Now, conn, Pconn);
        }
        public static FertigRet HoleFertigAluschaleFromAuNr(string AuNr, DateTime MaxDT, MySqlConnection conn, SqlConnection Pconn)
        {
            var ret = new FertigRet()
            {
                Menge = 0,
                M2 = 0m
            };

            var pLst = new List<Tuple<string, DateTime?, DateTime?>>();
            using (var cmd = new SqlCommand(@"SELECT [Pos],[App45_RahmenDatum],[App45_FlügelDatum] FROM [HO].[AluschalenFertigung] WHERE [Auftrag] = @AuNr", Pconn))
            {
                cmd.Parameters.Add("@AuNr", SqlDbType.NVarChar, 50).Value = AuNr;
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        string p;
                        DateTime? Rahmendat = null;
                        DateTime? Fluegeldat = null;
                        p = reader.GetInt32(reader.GetOrdinal("Pos")).ToString();

                        var ordi = reader.GetOrdinal("App45_RahmenDatum");
                        if (!reader.IsDBNull(ordi))
                        {
                            Rahmendat = reader.GetDateTime(ordi);
                            if (Rahmendat.Value.Date > MaxDT.Date)
                                Rahmendat = null;
                        }

                        ordi = reader.GetOrdinal("App45_FlügelDatum");
                        if (!reader.IsDBNull(ordi))
                        {
                            Fluegeldat = reader.GetDateTime(ordi);
                            if (Fluegeldat.Value.Date > MaxDT.Date)
                                Fluegeldat = null;
                        }

                        if (Fluegeldat.HasValue || Rahmendat.HasValue)
                            pLst.Add(new Tuple<string, DateTime?, DateTime?>(p, Rahmendat, Fluegeldat));
                    }
            }

            if (pLst.Any())
            {
                using (var cmd = new MySqlCommand(
                    @"SELECT IFNULL(SUM(CASE WHEN fw.`FrameTyp` = 1 THEN (pos.`TotalHeight` * pos.`TotalWidth`)/1000000 ELSE NULL END),0) AS m2Soll
		                ,COUNT(DISTINCT IF( fw.`FrameTyp` =1,fw.`FrameId`, NULL)) as RahmenSoll
		                ,COUNT(DISTINCT IF( fw.`FrameTyp` =2,fw.`FrameId`, NULL)) as FluegelSoll
                FROM `auftragsstatus` AS AuS
                JOIN `auftrag` AS Au
	                ON Au.`Id` = AuS.`auftrag_Id`
                JOIN `position` AS pos 
	                ON pos.`OrderNr` = Au.`AuftragsNr`
                JOIN `framewood` AS fw 
	                ON pos.`Id` = fw.`Position_Id`
                WHERE Aus.`Auftragsnummer` LIKE CONCAT(@AuNr,'%')                    
	                AND fw.`Produce` = 1
                    AND fw.`FrameTyp` IN (1, 2)
                    AND pos.`OrderPosNr` = @OrderPosNr
                    AND EXISTS(SELECT * FROM `aluschalen` AS Alu WHERE Alu.`Framewood_Frameid` = fw.`FrameId`)", conn))
                {
                    cmd.Parameters.Add("@AuNr", MySqlDbType.VarString).Value = AuNr;
                    var para_OrderNr = cmd.Parameters.Add("@OrderPosNr", MySqlDbType.VarString);

                    foreach (var p in pLst)
                    {
                        para_OrderNr.Value = p.Item1;

                        using (var reader = cmd.ExecuteReader())
                            while (reader.Read())
                            {
                                if ((p.Item2.HasValue || reader.GetInt32("RahmenSoll") < 1) && (p.Item3.HasValue || reader.GetInt32("FluegelSoll") < 1))
                                {
                                    ret.Menge += reader.GetInt32("RahmenSoll");
                                    ret.Menge += reader.GetInt32("FluegelSoll");
                                    ret.M2 += reader.GetDecimal("m2Soll");
                                }
                                else if (p.Item2.HasValue || reader.GetInt32("RahmenSoll") < 1)
                                {
                                    ret.Menge += reader.GetInt32("RahmenSoll");
                                }
                                else if ((p.Item3.HasValue || reader.GetInt32("FluegelSoll") < 1))
                                {
                                    ret.Menge += reader.GetInt32("FluegelSoll");
                                }
                            }
                    }
                }
            }
            return ret;
        }
        #endregion
        #region "Filter
        public static IEnumerable<string> FilterAuftragListForAluschale(IEnumerable<string> Basis)
        {
            return FilterAuftragListForAluschale(Basis, AuNR => AuNR);
        }
        public static IEnumerable<T> FilterAuftragListForAluschale<T>(IEnumerable<T> Basis, Func<T, string> GetAuNr)
        {
            return Basis;
        }
        #endregion
        #region "Getsoll"
        public static AuftragSoll GetAuftragSollAluschale(string AuNr, MySqlConnection conn)
        {
            AuftragSoll ret = new AuftragSoll() { AuNr = AuNr };

            using (var cmd = new MySqlCommand(
                @"  SELECT  COUNT(DISTINCT fw.`FrameId`) as Anzahl, IFNULL(MAX(AuS.`Quadratmeter`),0) AS QM
                    FROM `auftragsstatus` AS AuS
                    JOIN `auftrag` AS Au
	                    ON Au.`Id` = AuS.`auftrag_Id`
                    JOIN `position` AS pos 
	                    ON pos.`OrderNr` = Au.`AuftragsNr`
                    JOIN `framewood` AS fw 
	                    ON pos.`Id` = fw.`Position_Id`
                    WHERE Aus.`Auftragsnummer` LIKE CONCAT(@AuNr,'%')
                        AND fw.`FrameTyp` IN (1, 2)
                        AND fw.`Produce` = 1
                        AND EXISTS(SELECT * FROM `aluschalen` AS Alu WHERE Alu.`Framewood_Frameid` = fw.`FrameId`)", conn))
            {
                var para_AuNr = cmd.Parameters.Add("@AuNR", MySqlDbType.VarString);

                para_AuNr.Value = AuNr;
                using (var reader = cmd.ExecuteReader())
                    if (reader.Read())
                    {
                        ret.SollKanteln = reader.GetInt32("Anzahl");
                        ret.Sollm2 = reader.GetDecimal("QM");
                    }
            }

            return ret;
        }

        public static ICollection<PositionSollViereck> GetAuftragSollAluschalePosition(string AuNr, MySqlConnection conn, SqlConnection Pconn)
        {
            return GetAuftragSollAluschalePosition(AuNr, DateTime.Now, true, conn, Pconn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollAluschalePosition(string AuNr, bool Ende, MySqlConnection conn, SqlConnection Pconn)
        {
            return GetAuftragSollAluschalePosition(AuNr, DateTime.Now, Ende, conn, Pconn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollAluschalePosition(string AuNr, DateTime MaxDT, MySqlConnection conn, SqlConnection Pconn)
        {
            return GetAuftragSollAluschalePosition(AuNr, MaxDT, true, conn, Pconn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollAluschalePosition(string AuNr, DateTime MaxDT, bool Ende, MySqlConnection conn, SqlConnection Pconn)
        {
            var ret = new List<PositionSollViereck>();
            var fertigePos = new List<Tuple<string, DateTime?, DateTime?>>();
            using (var cmd = new SqlCommand(@"SELECT [Pos],[App45_RahmenDatum],[App45_FlügelDatum] FROM [HO].[AluschalenFertigung] WHERE [Auftrag] = @AuNr", Pconn))
            {
                cmd.Parameters.Add("@AuNr", SqlDbType.NVarChar, 50).Value = AuNr;
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var p = reader.GetInt32(reader.GetOrdinal("Pos")).ToString();
                        DateTime? Rahmendat = null;
                        DateTime? Fluegeldat = null;

                        var ordi = reader.GetOrdinal("App45_RahmenDatum");
                        if (!reader.IsDBNull(ordi))
                        {
                            Rahmendat = reader.GetDateTime(ordi);
                            if (Rahmendat.Value.Date > MaxDT.Date)
                                Rahmendat = null;
                        }

                        ordi = reader.GetOrdinal("App45_FlügelDatum");
                        if (!reader.IsDBNull(ordi))
                        {
                            Fluegeldat = reader.GetDateTime(ordi);
                            if (Fluegeldat.Value.Date > MaxDT.Date)
                                Fluegeldat = null;
                        }

                        fertigePos.Add(new Tuple<string, DateTime?, DateTime?>(p, Rahmendat, Fluegeldat));
                    }
            }

            Wertbau.HoleAuftragrueckstellung(AuNr, conn, out bool Rck, out string RckGrund);

            var Bereich = Ende ? "`Bereich_Oberflaeche`" : "`Bereich_Oberflaeche_Start`";
            using (var cmd = new MySqlCommand(
                @"SELECT  pos.`Id`, pos.`PositionsId`, pos.`ProfileSystem`,pos.`OrderPosNr`,pos.`Instanz`,pos.`Description`,Au.`Prod_Jahr`,Au.`Prod_KW`
		                ,MAX(fw.`timberkind`) AS timberkind,MAX(fw.`CoatingInside`) AS CoatingInside,MAX(fw.`CoatingOutside`) AS CoatingOutside
		                ,IFNULL(SUM(CASE WHEN fw.`FrameTyp` = 1 THEN (pos.`TotalHeight` * pos.`TotalWidth`)/1000000 ELSE NULL END),
                        (pos.`TotalHeight` * pos.`TotalWidth`)/1000000) AS m2Soll
		                ,COUNT(DISTINCT IF( fw.`FrameTyp` = 1,fw.`FrameId`, NULL)) as RahmenSoll
		                ,COUNT(DISTINCT IF( fw.`FrameTyp` = 2,fw.`FrameId`, NULL)) as FluegelSoll
                FROM `auftragsstatus` AS AuS
                JOIN `auftrag` AS Au
	                ON Au.`Id` = AuS.`auftrag_Id`
                JOIN `position` AS pos 
	                ON pos.`OrderNr` = Au.`AuftragsNr`
                JOIN `framewood` AS fw 
	                ON pos.`Id` = fw.`Position_Id`
                WHERE Aus.`Auftragsnummer` LIKE CONCAT(@AuNr,'%')
	                AND fw.`Produce` = 1
                    AND fw.`FrameTyp` IN (1, 2)
                    AND EXISTS(SELECT * FROM `aluschalen` AS Alu WHERE Alu.`Framewood_Frameid` = fw.`FrameId`)
                GROUP BY pos.`Id`
                ORDER BY pos.`OrderPosNr`;", conn))
            {
                cmd.Parameters.Add("@AuNR", MySqlDbType.VarString).Value = AuNr;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var Pos = new PositionSollViereck()
                        {
                            AuNr = AuNr,
                            PosId = reader.GetInt32("Id"),
                            PosPositionsId = reader.GetString("PositionsId"),
                            PosNr = reader.GetString("OrderPosNr").Trim(),
                            InstanzNr = reader.GetString("Instanz"),

                            Beschreibung = reader.GetString("Description"),
                            System = reader.GetString("ProfileSystem"),
                            HolzArt = reader.GetString("timberkind"),
                            FarbeAussen = reader.GetString("CoatingOutside"),
                            FarbeInnen = reader.GetString("CoatingInside"),
                            Sollm2 = reader.GetDecimal("m2Soll"),
                            SollRahmen = reader.GetInt32("RahmenSoll"),
                            //IstRahmen = reader.GetInt32("RahmenIst"),
                            SollFluegel = reader.GetInt32("FluegelSoll"),
                            //IstFluegel = reader.GetInt32("FluegelIst"),
                            ProdKw = reader.GetInt32("Prod_KW"),
                            ProdJahr = reader.GetInt32("Prod_Jahr"),
                            Rckstellen = Rck,
                            RckstellenGrund = RckGrund
                        };
                        var fp = fertigePos.FirstOrDefault(tp => tp.Item1 == Pos.PosNr);
                        if (fp != null)
                        {
                            if ((fp.Item2.HasValue || reader.GetInt32("RahmenSoll") < 1) && (fp.Item3.HasValue || reader.GetInt32("FluegelSoll") < 1))
                            {
                                Pos.IstRahmen = Pos.SollRahmen;
                                Pos.IstFluegel = Pos.SollFluegel;
                                Pos.Istm2 = Pos.Sollm2;
                                if (fp.Item2.HasValue)
                                    if (fp.Item3.HasValue)
                                        Pos.FertigDat = (fp.Item2.Value > fp.Item3.Value) ? fp.Item2 : fp.Item3;
                                    else
                                        Pos.FertigDat = fp.Item2.Value;
                                else if (fp.Item3.HasValue)
                                    Pos.FertigDat = fp.Item3.Value;
                            }
                            else
                            {
                                Pos.FertigDat = null;
                                if (fp.Item2.HasValue || reader.GetInt32("RahmenSoll") < 1)
                                    Pos.IstRahmen = Pos.SollRahmen;
                                else if (fp.Item3.HasValue || reader.GetInt32("FluegelSoll") < 1)
                                    Pos.IstFluegel = Pos.SollFluegel;
                            }
                        }


                        ret.Add(Pos);
                    }

            }

            return ret;
        }
        #endregion
        #region "Update"
        private static bool UpdateTageszaehlerAluschale(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            int counter = 0;
            bool ret = true;
            bool retry;
            bool Soll = true;
            bool Fertig = true;
            bool Rueck = true;
            bool Vor = true;
            do
            {
                retry = false;
                try
                {
                    using (var Pconn = new SqlConnection(Konstanten.PufferConnectionstring))
                    {
                        Pconn.Open();
                        if (Soll)
                        {
                            UpdateTageszaehlerAluschaleSoll(tageszaehler, conn, Pconn);
                            Soll = false;
                        }
                        if (Fertig)
                        {
                            UpdateTageszaehlerAluschaleFertig(tageszaehler, conn, Pconn);
                            Fertig = false;
                        }
                        if (Rueck)
                        {

                            UpdateTageszaehlerAluschaleRueck(tageszaehler, conn, Pconn);
                            Rueck = false;

                        }
                        if (Vor)
                        {
                            UpdateTageszaehlerAluschaleVor(tageszaehler, conn, Pconn);
                            Vor = false;
                        }
                    }
                }
                catch (MySqlException MysqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, MysqlEx.Message + Environment.NewLine + MysqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
                catch (SqlException SqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, SqlEx.Message + Environment.NewLine + SqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
            } while (retry);

            return ret;
        }

        private static void UpdateTageszaehlerAluschaleSoll(TageszaehlerBereich tageszaehler, MySqlConnection conn, SqlConnection Pconn)
        {
            IEnumerable<string> SollAuftraege = HoleAuftraegeSollAluschale(tageszaehler.Datum, tageszaehler.Datum);

            SollAuftraege = FilterAuftragListForAluschale(SollAuftraege);

            #region "GetSoll"
            tageszaehler.Soll = 0;
            tageszaehler.SollM2 = 0m;
            tageszaehler.VorlaufSoll = 0;
            tageszaehler.VorlaufSollM2 = 0m;
            foreach (var AuNr in SollAuftraege)
            {
                var Auf = GetAuftragSollAluschale(AuNr, conn);
                tageszaehler.Soll += Auf.SollKanteln;
                tageszaehler.SollM2 += Auf.Sollm2;
                var Ist = HoleFertigAluschaleFromAuNr(AuNr, tageszaehler.Datum.AddDays(-1), conn, Pconn);
                tageszaehler.VorlaufSoll += Ist.Menge;
                tageszaehler.VorlaufSollM2 += Ist.M2;
            }
            #endregion
        }
        private static void UpdateTageszaehlerAluschaleFertig(TageszaehlerBereich tageszaehler, MySqlConnection conn, SqlConnection Pconn)
        {
            tageszaehler.Fertig = 0;
            tageszaehler.FertigM2 = 0m;
            tageszaehler.Vorlauf = 0;
            tageszaehler.VorlaufM2 = 0m;
            tageszaehler.Rueckstand = 0;
            tageszaehler.RueckstandM2 = 0m;
            tageszaehler.FertigSoll = 0;
            tageszaehler.FertigSollM2 = 0m;

            #region "Anzahl/m²"
            DateTime tagdat = tageszaehler.Datum.Date;
            var pLst = new List<Tuple<string, string, DateTime?, DateTime?>>();
            using (var cmd = new SqlCommand(@"SELECT [Auftrag],[Pos],[App45_RahmenDatum],[App45_FlügelDatum] FROM [HO].[AluschalenFertigung] WHERE DATEDIFF(dd,@Date,[App45_RahmenDatum]) = 0 OR DATEDIFF(DAY,@Date,[App45_FlügelDatum]) = 0 ORDER BY [Auftrag],[Pos]", Pconn))
            {
                cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = tagdat;
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        DateTime? Rahmendat = null;
                        DateTime? Fluegeldat = null;
                        var ordi = reader.GetOrdinal("App45_RahmenDatum");
                        if (!reader.IsDBNull(ordi))
                        {
                            Rahmendat = reader.GetDateTime(ordi);
                            if (Rahmendat.Value.Date > tagdat)
                                Rahmendat = null;
                        }

                        ordi = reader.GetOrdinal("App45_FlügelDatum");
                        if (!reader.IsDBNull(ordi))
                        {
                            Fluegeldat = reader.GetDateTime(ordi);
                            if (Fluegeldat.Value.Date > tagdat)
                                Fluegeldat = null;
                        }

                        pLst.Add(new Tuple<string, string, DateTime?, DateTime?>(reader.GetString(reader.GetOrdinal("Auftrag")), reader.GetInt32(reader.GetOrdinal("Pos")).ToString(), Rahmendat, Fluegeldat));
                    }
            }

            using (var cmd = new MySqlCommand(
                @"SELECT 	IFNULL(SUM(RahmenSoll),0) AS RahmenSoll, 
		                    IFNULL(SUM(FluegelSoll),0) AS FluegelSoll,
                            IFNULL(SUM((pos.`TotalHeight` * pos.`TotalWidth`)/1000000),0) AS m2Soll
                    FROM (	SELECT  pos.`Id`,
			                    COUNT(DISTINCT IF( fw.`FrameTyp` =1,fw.`FrameId`, NULL)) as RahmenSoll
			                    ,COUNT(DISTINCT IF( fw.`FrameTyp` =2,fw.`FrameId`, NULL)) as FluegelSoll
		                    FROM `auftragsstatus` AS AuS
		                    JOIN `auftrag` AS Au
			                    ON Au.`Id` = AuS.`auftrag_Id`
		                    JOIN `position` AS pos 
			                    ON pos.`OrderNr` = Au.`AuftragsNr`
		                    JOIN `framewood` AS fw 
			                    ON pos.`Id` = fw.`Position_Id`
		                    WHERE Aus.`Auftragsnummer` LIKE CONCAT(@AuNr,'%')
			                    AND fw.`Produce` = 1
			                    AND fw.`FrameTyp` IN (1, 2)
			                    AND pos.`OrderPosNr` = @OrderPosNr
			                    AND EXISTS(SELECT * FROM `aluschalen` AS Alu WHERE Alu.`Framewood_Frameid` = fw.`FrameId`)
		                    GROUP BY pos.`Id`) as tbl
                    JOIN `position` AS pos
	                    ON pos.`Id` = tbl.`Id`", conn))
            {
                var para_AuNr = cmd.Parameters.Add("@AuNr", MySqlDbType.VarString);
                var para_OrderPosNr = cmd.Parameters.Add("@OrderPosNr", MySqlDbType.VarString);

                string AuNr = "";
                DateTime? Soll = null;

                foreach (var p in pLst)
                {
                    para_AuNr.Value = p.Item1;
                    para_OrderPosNr.Value = p.Item2;
                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                        {
                            int Anz = 0;
                            decimal QM = 0m;
                            if (p.Item3.HasValue && p.Item3.Value.Date == tagdat)
                                Anz += reader.GetInt32(reader.GetOrdinal("RahmenSoll"));
                            if (p.Item4.HasValue && p.Item4.Value.Date == tagdat)
                                Anz += reader.GetInt32(reader.GetOrdinal("FluegelSoll"));
                            if ((p.Item3.HasValue || reader.GetInt32("RahmenSoll") < 1) && (p.Item4.HasValue || reader.GetInt32("FluegelSoll") < 1))
                            {
                                QM += reader.GetDecimal("m2Soll");
                            }
                            tageszaehler.Fertig += Anz;
                            tageszaehler.FertigM2 += QM;

                            if (AuNr != p.Item1)
                            {
                                AuNr = p.Item1;
                                Soll = Wertbau.HoleOberflaecheDatumDTMySql(AuNr);
                            }
                            if (Soll.HasValue)
                            {
                                DateTime solldat = Soll.Value.Date;

                                if (solldat == tagdat)
                                {
                                    tageszaehler.FertigSoll += Anz;
                                    tageszaehler.FertigSollM2 += QM;
                                }
                                else if (solldat < tagdat && solldat.Year > Konstanten.ignoreNADat.Year)
                                {
                                    tageszaehler.Rueckstand += Anz;
                                    tageszaehler.RueckstandM2 += QM;
                                }
                                else if (solldat > tagdat)
                                {
                                    tageszaehler.Vorlauf += Anz;
                                    tageszaehler.VorlaufM2 += QM;
                                }
                            }
                        }
                }
            }
            #endregion

            #region "NF"
            // tageszaehler.Fertig_NF = -1;
            #endregion
        }
        private static void UpdateTageszaehlerAluschaleRueck(TageszaehlerBereich tageszaehler, MySqlConnection conn, SqlConnection Pconn)
        {
            tageszaehler.RueckstandBisher = 0;
            tageszaehler.RueckstandBisherM2 = 0m;
            IEnumerable<string> SollAuftraege = HoleAuftraegeSollAluschale(Konstanten.ignoreNADat, tageszaehler.Datum.AddDays(-1), true);

            SollAuftraege = FilterAuftragListStatus100(SollAuftraege, tageszaehler.Datum, conn);

            SollAuftraege = FilterAuftragListForAluschale(SollAuftraege);

            foreach (var AuNr in SollAuftraege)
            {
                var pos_coll = GetAuftragSollAluschalePosition(AuNr, tageszaehler.Datum, conn, Pconn);
                foreach (var pos in pos_coll)
                {
                    if (!pos.Fertig)
                    {
                        tageszaehler.RueckstandBisher += pos.SollGesamt - pos.IstGesamt;
                        tageszaehler.RueckstandBisherM2 += pos.Sollm2;
                    }
                }
            }

            tageszaehler.RueckstandBisherMitSoll = tageszaehler.RueckstandBisher + tageszaehler.Soll - tageszaehler.FertigSoll - tageszaehler.VorlaufSoll;
            tageszaehler.RueckstandBisherMitSollM2 = tageszaehler.RueckstandBisherM2 + tageszaehler.SollM2 - tageszaehler.FertigSollM2 - tageszaehler.VorlaufSollM2;
        }
        private static void UpdateTageszaehlerAluschaleVor(TageszaehlerBereich tageszaehler, MySqlConnection conn, SqlConnection Pconn)
        {
            tageszaehler.VorlaufBisher = 0;
            tageszaehler.VorlaufBisherM2 = 0m;

            IEnumerable<string> SollAuftraege = HoleAuftraegeSollAluschale(tageszaehler.Datum.AddDays(1), tageszaehler.Datum.AddDays(7));

            SollAuftraege = FilterAuftragListForAluschale(SollAuftraege);

            foreach (var Au in SollAuftraege)
            {
                var Ist = HoleFertigAluschaleFromAuNr(Au, tageszaehler.Datum.AddDays(-1), conn, Pconn);
                tageszaehler.VorlaufBisher += Ist.Menge;
                tageszaehler.VorlaufBisherM2 += Ist.M2;
            }
            tageszaehler.VorlaufBisher += tageszaehler.VorlaufSoll;
            tageszaehler.VorlaufBisherM2 += tageszaehler.VorlaufSollM2;
        }
        #endregion
        #endregion

        #region "Endmontage NGWA"
        #region "HoleFertig"
        public static FertigRet HoleFertigEndNGWAFromAuNr(string AuNr)
        {
            return HoleFertigEndNGWAFromAuNr(AuNr, DateTime.Now);
        }
        public static FertigRet HoleFertigEndNGWAFromAuNr(string AuNr, DateTime MaxDT)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();
                return HoleFertigEndNGWAFromAuNr(AuNr, MaxDT, conn);
            }
        }
        public static FertigRet HoleFertigEndNGWAFromAuNr(string AuNr, MySqlConnection conn)
        {
            return HoleFertigEndNGWAFromAuNr(AuNr, DateTime.Now, conn);
        }
        public static FertigRet HoleFertigEndNGWAFromAuNr(string AuNr, DateTime MaxDT, MySqlConnection conn)
        {
            var ret = new FertigRet()
            {
                Menge = 0,
                M2 = 0m
            };
            using (var cmd = new MySqlCommand(
               @"SELECT COUNT(*) AS Anzahl, IFNULL(SUM((pos.`TotalHeight` * pos.`TotalWidth`) / 1000000),0) AS QM
                FROM position AS pos
                JOIN vw_auftrag AS au
	                ON au.`AuftragsNr` = pos.`OrderNr`
                WHERE @AuNr IN (au.`SAPNr`,au.`AuftragsNr`)
	                AND pos.Bereich_Endmontage < CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"');", conn))
            {
                cmd.Parameters.Add("@AuNr", MySqlDbType.VarString).Value = AuNr;
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = MaxDT;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        ret.Menge += reader.GetInt32("Anzahl");
                        ret.M2 += reader.GetDecimal("QM");
                    }
            }
            return ret;
        }
        #endregion
        #region "Filter
        public static IEnumerable<string> FilterAuftragListForEndNGWA(IEnumerable<string> Basis)
        {
            return FilterAuftragListForEndNGWA(Basis, AuNR => AuNR);
        }
        public static IEnumerable<T> FilterAuftragListForEndNGWA<T>(IEnumerable<T> Basis, Func<T, string> GetAuNr)
        {
            //  Basis = FilterAuftragListHalle6(Basis, GetAuNr,conn);

            return Basis;
        }
        #endregion
        #region "GetSoll"
        public static AuftragSoll GetAuftragSollEndNGWA(string AuNr, MySqlConnection conn)
        {
            AuftragSoll ret = new AuftragSoll() { AuNr = AuNr };

            using (var cmd = new MySqlCommand(
                @"SELECT `Anzahl_fenster` AS Anzahl, `Quadratmeter` AS QM FROM `auftragsstatus` WHERE `Auftragsnummer` = @AuNr;", conn))
            {
                cmd.Parameters.Add("@AuNR", MySqlDbType.VarString).Value = AuNr;

                using (var reader = cmd.ExecuteReader())
                    if (reader.Read())
                    {
                        ret.SollKanteln = reader.GetInt32("Anzahl");
                        ret.Sollm2 = reader.GetDecimal("QM");
                    }
            }

            return ret;
        }

        public static ICollection<PositionSollViereck> GetAuftragSollEndNGWAPosition(string AuNr, MySqlConnection conn)
        {
            return GetAuftragSollEndNGWAPosition(AuNr, DateTime.Now, true, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollEndNGWAPosition(string AuNr, bool Ende, MySqlConnection conn)
        {
            return GetAuftragSollEndNGWAPosition(AuNr, DateTime.Now, Ende, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollEndNGWAPosition(string AuNr, DateTime MaxDT, MySqlConnection conn)
        {
            return GetAuftragSollEndNGWAPosition(AuNr, MaxDT, true, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollEndNGWAPosition(string AuNr, DateTime MaxDT, bool Ende, MySqlConnection conn)
        {
            var ret = new List<PositionSollViereck>();

            Wertbau.HoleAuftragrueckstellung(AuNr, conn, out bool Rck, out string RckGrund);


            var Bereich = Ende ? "`Bereich_Endmontage`" : "`Bereich_Endmontage_Start`";

            using (var cmd = new MySqlCommand(
                @"  SELECT pos.`Id`, pos.`PositionsId`, pos.`ProfileSystem`,pos.`OrderPosNr`,pos.`Instanz`,pos.`Description`,a.`Prod_Jahr`,a.`Prod_KW`
                    ,MAX(fw.`timberkind`) AS timberkind,MAX(fw.`CoatingInside`) AS CoatingInside,MAX(fw.`CoatingOutside`) AS CoatingOutside
	                ,IFNULL(SUM(CASE WHEN fw.`FrameTyp` = 1 THEN (pos.`TotalHeight` * pos.`TotalWidth`)/1000000 ELSE NULL END),
                    (pos.`TotalHeight` * pos.`TotalWidth`)/1000000) AS m2Soll
	                ,COUNT(CASE WHEN fw.`FrameTyp` = 1 THEN 1 ELSE NULL END) AS RahmenSoll
	                ,COUNT(CASE WHEN fw.`FrameTyp` = 2 THEN 1 ELSE NULL END) AS FluegelSoll
                    ,(CASE WHEN pos_s.`Id` IS NULL THEN FALSE ELSE TRUE END) AS Finish
                    ,MAX(pos." + (Bereich) + @") AS FertigDat
                FROM Auftrag AS a
                JOIN vw_position AS pos
	                ON pos.`OrderNr` = a.`AuftragsNr`
                JOIN framewood AS fw 
	                ON fw.`Position_Id` = pos.`Id`
                LEFT JOIN vw_position AS pos_s
	                ON pos.`Id` = pos_s.`Id`
	                AND pos_s." + (Bereich) + " < CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')                    
                WHERE @AuNr IN (a.`AuftragsNr`, a.`SAPNr`)
                    AND fw.`Produce` = 1
                AND fw.`FrameTyp` IN (1,2)
                GROUP BY pos.`Id`;", conn))
            {
                cmd.Parameters.Add("@AuNR", MySqlDbType.VarString).Value = AuNr;
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = MaxDT;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var Pos = new PositionSollViereck()
                        {
                            AuNr = AuNr,
                            PosId = reader.GetInt32("Id"),
                            PosPositionsId = reader.GetString("PositionsId"),
                            PosNr = reader.GetString("OrderPosNr"),
                            InstanzNr = reader.GetString("Instanz"),
                            Beschreibung = reader.GetString("Description"),
                            System = reader.GetString("ProfileSystem"),
                            HolzArt = reader.GetString("timberkind"),
                            FarbeAussen = reader.GetString("CoatingOutside"),
                            FarbeInnen = reader.GetString("CoatingInside"),
                            Sollm2 = reader.GetDecimal("m2Soll"),
                            SollRahmen = reader.GetInt32("RahmenSoll"),
                            SollFluegel = reader.GetInt32("FluegelSoll"),
                            ProdKw = reader.GetInt32("Prod_KW"),
                            ProdJahr = reader.GetInt32("Prod_Jahr"),
                            Rckstellen = Rck,
                            RckstellenGrund = RckGrund
                        };
                        bool finish = reader.GetBoolean("Finish");
                        if (finish)
                        {
                            Pos.Istm2 = Pos.Sollm2;
                            Pos.IstFluegel = Pos.SollFluegel;
                            Pos.IstRahmen = Pos.SollRahmen;
                            Pos.FertigDat = reader.GetDateTime("FertigDat");
                        }
                        else
                            Pos.FertigDat = null;

                        ret.Add(Pos);
                    }

            }

            return ret;
        }
        #endregion
        #region "Update"
        private static bool UpdateTageszaehlerEndNGWA(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            int counter = 0;
            bool ret = true;
            bool Soll = true;
            bool Fertig = true;
            bool Rueck = true;
            bool Vor = true;
            bool retry;
            do
            {
                retry = false;
                try
                {
                    if (Soll)
                    {
                        UpdateTageszaehlerEndNGWASoll(tageszaehler, conn);
                        Soll = false;
                    }
                    if (Fertig)
                    {
                        UpdateTageszaehlerEndNGWAFertig(tageszaehler, conn);
                        Fertig = false;
                    }
                    if (Rueck)
                    {
                        UpdateTageszaehlerEndNGWARueck(tageszaehler, conn);
                        Rueck = false;
                    }
                    if (Vor)
                    {
                        UpdateTageszaehlerEndNGWAVor(tageszaehler, conn);
                        Vor = false;
                    }
                }
                catch (MySqlException MysqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, MysqlEx.Message + Environment.NewLine + MysqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
                catch (SqlException SqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, SqlEx.Message + Environment.NewLine + SqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
            } while (retry);

            return ret;
        }

        private static void UpdateTageszaehlerEndNGWASoll(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            IEnumerable<string> SollAuftraege = HoleAuftraegeSollEndNGWA(tageszaehler.Datum, tageszaehler.Datum);

            SollAuftraege = FilterAuftragListForEndNGWA(SollAuftraege);

            #region "GetSoll"
            tageszaehler.Soll = 0;
            tageszaehler.SollM2 = 0m;
            tageszaehler.VorlaufSoll = 0;
            tageszaehler.VorlaufSollM2 = 0m;
            foreach (var AuNr in SollAuftraege)
            {
                var Auf = GetAuftragSollEndNGWA(AuNr, conn);
                tageszaehler.Soll += Auf.SollKanteln;
                tageszaehler.SollM2 += Auf.Sollm2;
                var Ist = HoleFertigEndNGWAFromAuNr(AuNr, tageszaehler.Datum.AddDays(-1), conn);
                tageszaehler.VorlaufSoll += Ist.Menge;
                tageszaehler.VorlaufSollM2 += Ist.M2;
            }
            #endregion
        }
        private static void UpdateTageszaehlerEndNGWAFertig(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {

            tageszaehler.Fertig = 0;
            tageszaehler.FertigM2 = 0m;
            tageszaehler.Vorlauf = 0;
            tageszaehler.VorlaufM2 = 0m;
            tageszaehler.Rueckstand = 0;
            tageszaehler.RueckstandM2 = 0m;
            tageszaehler.FertigSoll = 0;
            tageszaehler.FertigSollM2 = 0m;

            #region "Anzahl/m²"
            using (var cmd = new MySqlCommand(
                @"  SELECT IFNULL(au.`cSAPNr`,au.`AuftragsNr`) AS `AuNr`,COUNT(*) AS Anzahl, IFNULL(SUM((pos.`TotalHeight` * pos.`TotalWidth`) / 1000000),0) AS QM
                    FROM position AS pos
                    JOIN vw_auftrag AS au
	                    ON au.`AuftragsNr` = pos.`OrderNr`
                    JOIN `auftragsstatus` AS AuS
	                    ON au.`Id` = AuS.`auftrag_Id`
                    WHERE pos.Bereich_Endmontage BETWEEN CONCAT(@Date, ' " + Konstanten.UhrzeitSchichtstart + @"') AND CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
                        AND AuS.`Kennzeichen` IN ('HA','NGWA')
                    GROUP BY AuNr;", conn))
            {
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = tageszaehler.Datum.Date;
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("AuNr")))
                        {
                            var AuNr = reader.GetString("AuNr");

                            int Anz = reader.GetInt32("Anzahl");
                            decimal QM = reader.GetDecimal("QM");
                            tageszaehler.Fertig += Anz;
                            tageszaehler.FertigM2 += QM;

                            DateTime? Soll = Wertbau.HoleAnschlagDatumDTMySql(AuNr);
                            if (Soll.HasValue)
                            {
                                DateTime solldat = Soll.Value.Date;
                                DateTime tagdat = tageszaehler.Datum.Date;
                                if (solldat == tagdat)
                                {
                                    tageszaehler.FertigSoll += Anz;
                                    tageszaehler.FertigSollM2 += QM;
                                }
                                else if (solldat < tagdat && solldat.Year > Konstanten.ignoreNADat.Year)
                                {
                                    tageszaehler.Rueckstand += Anz;
                                    tageszaehler.RueckstandM2 += QM;
                                }
                                else if (solldat > tagdat)
                                {
                                    tageszaehler.Vorlauf += Anz;
                                    tageszaehler.VorlaufM2 += QM;
                                }
                            }

                        }
                    }
            }
            #endregion

            #region "NF"
            // tageszaehler.Fertig_NF = -1;
            #endregion

        }
        private static void UpdateTageszaehlerEndNGWARueck(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.RueckstandBisher = 0;
            tageszaehler.RueckstandBisherM2 = 0m;
            IEnumerable<string> SollAuftraege = HoleAuftraegeSollEndNGWA(Konstanten.ignoreNADat, tageszaehler.Datum.AddDays(-1), true);

            SollAuftraege = FilterAuftragListStatus100(SollAuftraege, tageszaehler.Datum, conn);

            SollAuftraege = FilterAuftragListForEndNGWA(SollAuftraege);

            foreach (var AuNr in SollAuftraege)
            {
                var pos_coll = GetAuftragSollEndNGWAPosition(AuNr, tageszaehler.Datum, conn);
                foreach (var pos in pos_coll)
                {
                    if (!pos.Fertig)
                    {
                        tageszaehler.RueckstandBisher += 1;
                        tageszaehler.RueckstandBisherM2 += pos.Sollm2;
                    }
                }
            }

            tageszaehler.RueckstandBisherMitSoll = tageszaehler.RueckstandBisher + tageszaehler.Soll - tageszaehler.FertigSoll - tageszaehler.VorlaufSoll;
            tageszaehler.RueckstandBisherMitSollM2 = tageszaehler.RueckstandBisherM2 + tageszaehler.SollM2 - tageszaehler.FertigSollM2 - tageszaehler.VorlaufSollM2;
        }
        private static void UpdateTageszaehlerEndNGWAVor(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.VorlaufBisher = 0;
            tageszaehler.VorlaufBisherM2 = 0m;

            IEnumerable<string> SollAuftraege = HoleAuftraegeSollEndNGWA(tageszaehler.Datum.AddDays(1), tageszaehler.Datum.AddDays(7));

            SollAuftraege = FilterAuftragListForEndNGWA(SollAuftraege);

            foreach (var Au in SollAuftraege)
            {
                var Ist = HoleFertigEndNGWAFromAuNr(Au, tageszaehler.Datum.AddDays(-1), conn);
                tageszaehler.VorlaufBisher += Ist.Menge;
                tageszaehler.VorlaufBisherM2 += Ist.M2;
            }
            tageszaehler.VorlaufBisher += tageszaehler.VorlaufSoll;
            tageszaehler.VorlaufBisherM2 += tageszaehler.VorlaufSollM2;
        }
        #endregion
        #endregion

        #region "Endmontage Holz"
        #region "HoleFertig"
        public static FertigRet HoleFertigEndHolzFromAuNr(string AuNr)
        {
            return HoleFertigEndHolzFromAuNr(AuNr, DateTime.Now);
        }
        public static FertigRet HoleFertigEndHolzFromAuNr(string AuNr, DateTime MaxDT)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();
                return HoleFertigEndHolzFromAuNr(AuNr, MaxDT, conn);
            }
        }
        public static FertigRet HoleFertigEndHolzFromAuNr(string AuNr, MySqlConnection conn)
        {
            return HoleFertigEndHolzFromAuNr(AuNr, DateTime.Now, conn);
        }
        public static FertigRet HoleFertigEndHolzFromAuNr(string AuNr, DateTime MaxDT, MySqlConnection conn)
        {
            return HoleFertigEndNGWAFromAuNr(AuNr, MaxDT, conn);
        }
        #endregion
        #region "Filter"
        public static IEnumerable<string> FilterAuftragListForEndHolz(IEnumerable<string> Basis)
        {
            return FilterAuftragListForEndHolz(Basis, AuNR => AuNR);
        }
        public static IEnumerable<T> FilterAuftragListForEndHolz<T>(IEnumerable<T> Basis, Func<T, string> GetAuNr)
        {
            // Basis = FilterAuftragListHalle6(Basis, GetAuNr,conn);

            return Basis;
        }
        #endregion
        #region "GetSoll"
        public static AuftragSoll GetAuftragSollEndHolz(string AuNr, MySqlConnection conn)
        {
            return GetAuftragSollEndNGWA(AuNr, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollEndHolzPosition(string AuNr, MySqlConnection conn)
        {
            return GetAuftragSollEndHolzPosition(AuNr, DateTime.Now, true, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollEndHolzPosition(string AuNr, bool Ende, MySqlConnection conn)
        {
            return GetAuftragSollEndHolzPosition(AuNr, DateTime.Now, Ende, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollEndHolzPosition(string AuNr, DateTime MaxDT, MySqlConnection conn)
        {
            return GetAuftragSollEndHolzPosition(AuNr, MaxDT, true, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollEndHolzPosition(string AuNr, DateTime MaxDT, bool Ende, MySqlConnection conn)
        {
            return GetAuftragSollEndNGWAPosition(AuNr, MaxDT, Ende, conn);
        }
        #endregion
        #region "Update"
        private static bool UpdateTageszaehlerEndHolz(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            int counter = 0;
            bool ret = true;
            bool retry;
            bool Soll = true;
            bool Fertig = true;
            bool Rueck = true;
            bool Vor = true;
            do
            {
                retry = false;
                try
                {
                    if (Soll)
                    {
                        UpdateTageszaehlerEndHolzSoll(tageszaehler, conn);
                        Soll = false;
                    }
                    if (Fertig)
                    {
                        UpdateTageszaehlerEndHolzFertig(tageszaehler, conn);
                        Fertig = false;
                    }
                    if (Rueck)
                    {
                        UpdateTageszaehlerEndHolzRueck(tageszaehler, conn);
                        Rueck = false;
                    }
                    if (Vor)
                    {
                        UpdateTageszaehlerEndHolzVor(tageszaehler, conn);
                        Vor = false;
                    }
                }
                catch (MySqlException MysqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, MysqlEx.Message + Environment.NewLine + MysqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
                catch (SqlException SqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, SqlEx.Message + Environment.NewLine + SqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
            } while (retry);

            return ret;
        }

        private static void UpdateTageszaehlerEndHolzSoll(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            IEnumerable<string> SollAuftraege = HoleAuftraegeSollEndHolz(tageszaehler.Datum, tageszaehler.Datum);

            SollAuftraege = FilterAuftragListForEndHolz(SollAuftraege);

            #region "GetSoll"
            tageszaehler.Soll = 0;
            tageszaehler.SollM2 = 0m;
            foreach (var AuNr in SollAuftraege)
            {
                var Auf = GetAuftragSollEndHolz(AuNr, conn);
                tageszaehler.Soll += Auf.SollKanteln;
                tageszaehler.SollM2 += Auf.Sollm2;
                var Ist = HoleFertigEndHolzFromAuNr(AuNr, tageszaehler.Datum.AddDays(-1), conn);
                tageszaehler.VorlaufSoll += Ist.Menge;
                tageszaehler.VorlaufSollM2 += Ist.M2;
            }
            #endregion
        }
        private static void UpdateTageszaehlerEndHolzFertig(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.Fertig = 0;
            tageszaehler.FertigM2 = 0m;
            tageszaehler.Vorlauf = 0;
            tageszaehler.VorlaufM2 = 0m;
            tageszaehler.Rueckstand = 0;
            tageszaehler.RueckstandM2 = 0m;
            tageszaehler.FertigSoll = 0;
            tageszaehler.FertigSollM2 = 0m;

            #region "Anzahl/m²"
            using (var cmd = new MySqlCommand(
                @"  SELECT IFNULL(au.`cSAPNr`,au.`AuftragsNr`) AS `AuNr`,COUNT(*) AS Anzahl, IFNULL(SUM((pos.`TotalHeight` * pos.`TotalWidth`) / 1000000),0) AS QM
                    FROM position AS pos
                    JOIN vw_auftrag AS au
	                    ON au.`AuftragsNr` = pos.`OrderNr`
                    JOIN `auftragsstatus` AS AuS
	                    ON au.`Id` = AuS.`auftrag_Id`
                    WHERE pos.Bereich_Endmontage BETWEEN CONCAT(@Date, ' " + Konstanten.UhrzeitSchichtstart + @"') AND CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')
                        AND AuS.`Kennzeichen` IN ('HO')
                    GROUP BY AuNr;", conn))
            {
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = tageszaehler.Datum.Date;
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("AuNr")))
                        {
                            var AuNr = reader.GetString("AuNr");

                            int Anz = reader.GetInt32("Anzahl");
                            decimal QM = reader.GetDecimal("QM");
                            tageszaehler.Fertig += Anz;
                            tageszaehler.FertigM2 += QM;

                            DateTime? Soll = Wertbau.HoleAnschlagDatumDTMySql(AuNr);
                            if (Soll.HasValue)
                            {
                                DateTime solldat = Soll.Value.Date;
                                DateTime tagdat = tageszaehler.Datum.Date;
                                if (solldat == tagdat)
                                {
                                    tageszaehler.FertigSoll += Anz;
                                    tageszaehler.FertigSollM2 += QM;
                                }
                                else if (solldat < tagdat && solldat.Year > Konstanten.ignoreNADat.Year)
                                {
                                    tageszaehler.Rueckstand += Anz;
                                    tageszaehler.RueckstandM2 += QM;
                                }
                                else if (solldat > tagdat)
                                {
                                    tageszaehler.Vorlauf += Anz;
                                    tageszaehler.VorlaufM2 += QM;
                                }
                            }

                        }
                    }
            }
            #endregion

            #region "NF"
            // tageszaehler.Fertig_NF = -1;
            #endregion
        }
        private static void UpdateTageszaehlerEndHolzRueck(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.RueckstandBisher = 0;
            tageszaehler.RueckstandBisherM2 = 0m;
            IEnumerable<string> SollAuftraege = HoleAuftraegeSollEndHolz(Konstanten.ignoreNADat, tageszaehler.Datum.AddDays(-1), true);

            SollAuftraege = FilterAuftragListStatus100(SollAuftraege, tageszaehler.Datum, conn);

            SollAuftraege = FilterAuftragListForEndHolz(SollAuftraege);

            foreach (var AuNr in SollAuftraege)
            {
                var pos_coll = GetAuftragSollEndHolzPosition(AuNr, tageszaehler.Datum, conn);
                foreach (var pos in pos_coll)
                {
                    if (!pos.Fertig)
                    {
                        tageszaehler.RueckstandBisher += 1;
                        tageszaehler.RueckstandBisherM2 += pos.Sollm2;
                    }
                }
            }

            tageszaehler.RueckstandBisherMitSoll = tageszaehler.RueckstandBisher + tageszaehler.Soll - tageszaehler.FertigSoll - tageszaehler.VorlaufSoll;
            tageszaehler.RueckstandBisherMitSollM2 = tageszaehler.RueckstandBisherM2 + tageszaehler.SollM2 - tageszaehler.FertigSollM2 - tageszaehler.VorlaufSollM2;
        }
        private static void UpdateTageszaehlerEndHolzVor(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.VorlaufBisher = 0;
            tageszaehler.VorlaufBisherM2 = 0m;

            IEnumerable<string> SollAuftraege = HoleAuftraegeSollEndHolz(tageszaehler.Datum.AddDays(1), tageszaehler.Datum.AddDays(7));

            SollAuftraege = FilterAuftragListForEndHolz(SollAuftraege);

            foreach (var Au in SollAuftraege)
            {
                var Ist = HoleFertigEndHolzFromAuNr(Au, tageszaehler.Datum.AddDays(-1), conn);
                tageszaehler.VorlaufBisher += Ist.Menge;
                tageszaehler.VorlaufBisherM2 += Ist.M2;
            }
            tageszaehler.VorlaufBisher += tageszaehler.VorlaufSoll;
            tageszaehler.VorlaufBisherM2 += tageszaehler.VorlaufSollM2;
        }
        #endregion
        #endregion

        #region "Holz Schiebetür"
        #region "HoleFertig"
        public static FertigRet HoleFertigHSTFromAuNr(string AuNr)
        {
            return HoleFertigHSTFromAuNr(AuNr, DateTime.Now);
        }
        public static FertigRet HoleFertigHSTFromAuNr(string AuNr, DateTime MaxDT)
        {
            using (var conn = new MySqlConnection(Konstanten.MySqlDatenConnectionstring))
            {
                conn.Open();
                return HoleFertigHSTFromAuNr(AuNr, MaxDT, conn);
            }
        }
        public static FertigRet HoleFertigHSTFromAuNr(string AuNr, MySqlConnection conn)
        {
            return HoleFertigHSTFromAuNr(AuNr, DateTime.Now, conn);
        }
        public static FertigRet HoleFertigHSTFromAuNr(string AuNr, DateTime MaxDT, MySqlConnection conn)
        {
            return HoleFertigEndNGWAFromAuNr(AuNr, MaxDT, conn);
        }
        #endregion
        #region "Filter"
        public static IEnumerable<string> FilterAuftragListForHST(IEnumerable<string> Basis)
        {
            return FilterAuftragListForHST(Basis, AuNR => AuNR);
        }
        public static IEnumerable<T> FilterAuftragListForHST<T>(IEnumerable<T> Basis, Func<T, string> GetAuNr)
        {
            // Basis = FilterAuftragListHalle6(Basis, GetAuNr,conn);

            return Basis;
        }
        #endregion
        #region "GetSoll"
        public static AuftragSoll GetAuftragSollHST(string AuNr, MySqlConnection conn)
        {
            return GetAuftragSollEndNGWA(AuNr, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollHSTPosition(string AuNr, MySqlConnection conn)
        {
            return GetAuftragSollHSTPosition(AuNr, DateTime.Now, true, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollHSTPosition(string AuNr, bool Ende, MySqlConnection conn)
        {
            return GetAuftragSollHSTPosition(AuNr, DateTime.Now, Ende, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollHSTPosition(string AuNr, DateTime MaxDT, MySqlConnection conn)
        {
            return GetAuftragSollHSTPosition(AuNr, MaxDT, true, conn);
        }
        public static ICollection<PositionSollViereck> GetAuftragSollHSTPosition(string AuNr, DateTime MaxDT, bool Ende, MySqlConnection conn)
        {
            return GetAuftragSollEndNGWAPosition(AuNr, MaxDT, Ende, conn);
        }
        #endregion
        #region "Update"
        private static bool UpdateTageszaehlerHST(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            int counter = 0;
            bool ret = true;
            bool retry;
            bool Soll = true;
            bool Fertig = true;
            bool Rueck = true;
            bool Vor = true;
            do
            {
                retry = false;
                try
                {
                    if (Soll)
                    {
                        UpdateTageszaehlerHSTSoll(tageszaehler, conn);
                        Soll = false;
                    }
                    if (Fertig)
                    {
                        UpdateTageszaehlerHSTFertig(tageszaehler, conn);
                        Fertig = false;
                    }
                    if (Rueck)
                    {
                        UpdateTageszaehlerHSTRueck(tageszaehler, conn);
                        Rueck = false;
                    }
                    if (Vor)
                    {
                        UpdateTageszaehlerHSTVor(tageszaehler, conn);
                        Vor = false;
                    }
                }
                catch (MySqlException MysqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, MysqlEx.Message + Environment.NewLine + MysqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
                catch (SqlException SqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, SqlEx.Message + Environment.NewLine + SqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
            } while (retry);

            return ret;
        }

        private static void UpdateTageszaehlerHSTSoll(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            IEnumerable<string> SollAuftraege = HoleAuftraegeSollHST(tageszaehler.Datum, tageszaehler.Datum);

            SollAuftraege = FilterAuftragListForHST(SollAuftraege);

            #region "GetSoll"
            tageszaehler.Soll = 0;
            tageszaehler.SollM2 = 0m;
            foreach (var AuNr in SollAuftraege)
            {
                var Auf = GetAuftragSollHST(AuNr, conn);
                tageszaehler.Soll += Auf.SollKanteln;
                tageszaehler.SollM2 += Auf.Sollm2;
                var Ist = HoleFertigHSTFromAuNr(AuNr, tageszaehler.Datum.AddDays(-1), conn);
                tageszaehler.VorlaufSoll += Ist.Menge;
                tageszaehler.VorlaufSollM2 += Ist.M2;
            }
            #endregion
        }
        private static void UpdateTageszaehlerHSTFertig(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.Fertig = 0;
            tageszaehler.FertigM2 = 0m;
            tageszaehler.Vorlauf = 0;
            tageszaehler.VorlaufM2 = 0m;
            tageszaehler.Rueckstand = 0;
            tageszaehler.RueckstandM2 = 0m;
            tageszaehler.FertigSoll = 0;
            tageszaehler.FertigSollM2 = 0m;

            #region "Anzahl/m²"
            using (var cmd = new MySqlCommand(
                @"  SELECT IFNULL(au.`cSAPNr`,au.`AuftragsNr`) AS `AuNr`,COUNT(*) AS Anzahl, IFNULL(SUM((pos.`TotalHeight` * pos.`TotalWidth`) / 1000000),0) AS QM
                    FROM position AS pos
                    JOIN vw_auftrag AS au
	                    ON au.`AuftragsNr` = pos.`OrderNr`
                    JOIN `auftragsstatus` AS AuS
	                    ON au.`Id` = AuS.`auftrag_Id`
                    WHERE pos.Bereich_Endmontage BETWEEN CONCAT(@Date, ' " + Konstanten.UhrzeitSchichtstart + @"') AND CONCAT(DATE_ADD(@Date, INTERVAL 1 DAY), ' " + Konstanten.UhrzeitSchichtende + @"')	                    
                        AND AuS.`HST` = TRUE
                    GROUP BY AuNr;", conn))
            {
                cmd.Parameters.Add("@Date", MySqlDbType.Date).Value = tageszaehler.Datum.Date;
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("AuNr")))
                        {
                            var AuNr = reader.GetString("AuNr");

                            int Anz = reader.GetInt32("Anzahl");
                            decimal QM = reader.GetDecimal("QM");
                            tageszaehler.Fertig += Anz;
                            tageszaehler.FertigM2 += QM;

                            DateTime? Soll = Wertbau.HoleAnschlagDatumDTMySql(AuNr);
                            if (Soll.HasValue)
                            {
                                DateTime solldat = Soll.Value.Date;
                                DateTime tagdat = tageszaehler.Datum.Date;
                                if (solldat == tagdat)
                                {
                                    tageszaehler.FertigSoll += Anz;
                                    tageszaehler.FertigSollM2 += QM;
                                }
                                else if (solldat < tagdat && solldat.Year > Konstanten.ignoreNADat.Year)
                                {
                                    tageszaehler.Rueckstand += Anz;
                                    tageszaehler.RueckstandM2 += QM;
                                }
                                else if (solldat > tagdat)
                                {
                                    tageszaehler.Vorlauf += Anz;
                                    tageszaehler.VorlaufM2 += QM;
                                }
                            }

                        }
                    }
            }
            #endregion

            #region "NF"
            // tageszaehler.Fertig_NF = -1;
            #endregion
        }
        private static void UpdateTageszaehlerHSTRueck(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.RueckstandBisher = 0;
            tageszaehler.RueckstandBisherM2 = 0m;
            IEnumerable<string> SollAuftraege = HoleAuftraegeSollHST(Konstanten.ignoreNADat, tageszaehler.Datum.AddDays(-1), true);

            SollAuftraege = FilterAuftragListStatus100(SollAuftraege, tageszaehler.Datum, conn);

            SollAuftraege = FilterAuftragListForHST(SollAuftraege);

            foreach (var AuNr in SollAuftraege)
            {
                var pos_coll = GetAuftragSollHSTPosition(AuNr, tageszaehler.Datum, conn);
                foreach (var pos in pos_coll)
                {
                    if (!pos.Fertig)
                    {
                        tageszaehler.RueckstandBisher += 1;
                        tageszaehler.RueckstandBisherM2 += pos.Sollm2;
                    }
                }
            }

            tageszaehler.RueckstandBisherMitSoll = tageszaehler.RueckstandBisher + tageszaehler.Soll - tageszaehler.FertigSoll - tageszaehler.VorlaufSoll;
            tageszaehler.RueckstandBisherMitSollM2 = tageszaehler.RueckstandBisherM2 + tageszaehler.SollM2 - tageszaehler.FertigSollM2 - tageszaehler.VorlaufSollM2;
        }
        private static void UpdateTageszaehlerHSTVor(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.VorlaufBisher = 0;
            tageszaehler.VorlaufBisherM2 = 0m;

            IEnumerable<string> SollAuftraege = HoleAuftraegeSollHST(tageszaehler.Datum.AddDays(1), tageszaehler.Datum.AddDays(7));

            SollAuftraege = FilterAuftragListForHST(SollAuftraege);

            foreach (var Au in SollAuftraege)
            {
                var Ist = HoleFertigHSTFromAuNr(Au, tageszaehler.Datum.AddDays(-1), conn);
                tageszaehler.VorlaufBisher += Ist.Menge;
                tageszaehler.VorlaufBisherM2 += Ist.M2;
            }
            tageszaehler.VorlaufBisher += tageszaehler.VorlaufSoll;
            tageszaehler.VorlaufBisherM2 += tageszaehler.VorlaufSollM2;
        }
        #endregion
        #endregion

        #region "Sonderbau Gebäude 6"
        private static bool UpdateTageszaehlerGeb6Sonder(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            int counter = 0;
            bool ret = true;
            bool retry;
            bool Soll = true;
            bool Fertig = true;
            bool Rueck = true;
            bool Vor = true;
            do
            {
                retry = false;
                try
                {
                    if (Soll)
                    {
                        UpdateTageszaehlerGeb6SonderSoll(tageszaehler, conn);
                        Soll = false;
                    }
                    if (Fertig)
                    {
                        UpdateTageszaehlerGeb6SonderFertig(tageszaehler, conn);
                        Fertig = false;
                    }
                    if (Rueck)
                    {
                        UpdateTageszaehlerGeb6SonderRueck(tageszaehler, conn);
                        Rueck = false;
                    }
                    if (Vor)
                    {
                        UpdateTageszaehlerGeb6SonderVor(tageszaehler, conn);
                        Vor = false;
                    }
                }
                catch (MySqlException MysqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, MysqlEx.Message + Environment.NewLine + MysqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
                catch (SqlException SqlEx)
                {
                    if (counter < 10)
                    {
                        counter++;
                        retry = true;
                        SchreibeException(tageszaehler, SqlEx.Message + Environment.NewLine + SqlEx.StackTrace);
                        Thread.Sleep(100);
                    }
                    else
                        ret = false;
                }
            } while (retry);

            return ret;
        }

        private static void UpdateTageszaehlerGeb6SonderSoll(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            List<string> SollAuftraege = new List<string>();
            #region "GetAufträge"

            #endregion

            #region "GetSoll"

            #endregion
        }

        private static void UpdateTageszaehlerGeb6SonderFertig(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        {
            tageszaehler.Fertig = 0;
            tageszaehler.FertigM2 = 0m;
            tageszaehler.Vorlauf = 0;
            tageszaehler.VorlaufM2 = 0m;
            tageszaehler.Rueckstand = 0;
            tageszaehler.RueckstandM2 = 0m;
            #region "Anzahl/m²"

            #endregion

            #region "NF"

            #endregion
        }

        private static void UpdateTageszaehlerGeb6SonderRueck(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        { }

        private static void UpdateTageszaehlerGeb6SonderVor(TageszaehlerBereich tageszaehler, MySqlConnection conn)
        { }
        #endregion
        #endregion

        #region "Station"

        #endregion
    }
}
