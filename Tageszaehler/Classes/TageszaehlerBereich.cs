﻿using System;

using WertbauLibrary.Types;

namespace TageszaehlerLibary.Classes
{
    public class TageszaehlerBereich
    {
        public Fertigungsbereich Fertigungsbereich { get; set; }
        public DateTime Datum { get; set; }

        public int Soll { get; set; }
        public decimal SollM2 { get; set; }
        public int Fertig { get; set; }
        public decimal FertigM2 { get; set; }
        public int FertigNf { get; set; }
        public int FertigSoll { get; set; }
        public decimal FertigSollM2 { get; set; }
        public int Vorlauf { get; set; }
        public decimal VorlaufM2 { get; set; }
        public int VorlaufSoll { get; set; }
        public decimal VorlaufSollM2 { get; set; }
        public int Rueckstand { get; set; }
        public decimal RueckstandM2 { get; set; }
        public bool Finished { get; set; }
        public int RueckstandBisher { get; set; }
        public decimal RueckstandBisherM2 { get; set; }
        public int RueckstandBisherMitSoll { get; set; }
        public decimal RueckstandBisherMitSollM2 { get; set; }
        public int VorlaufBisher { get; set; }
        public decimal VorlaufBisherM2 { get; set; }
    }
}
