﻿using System;

namespace TageszaehlerLibary.Classes
{
    public class PositionSollViereck
    {
        public string AuNr { get; set; }
        public int PosId { get; set; }
        public string PosPositionsId { get; set; }
        public string PosNr { get; set; }
        public string InstanzNr { get; set; }

        public string System { get; set; }
        public string HolzArt { get; set; }
        public string FarbeInnen { get; set; }
        public string FarbeAussen { get; set; }
        public string Beschreibung { get; set; }
        public int ProdKw { get; set; }
        public int ProdJahr { get; set; }

        public int SollRahmen { get; set; }
        public int SollFluegel { get; set; }
        public int SollGesamt { get => SollRahmen + SollFluegel; }
        public decimal Sollm2 { get; set; }
        public int IstRahmen { get; set; }
        public int IstFluegel { get; set; }
        public int IstGesamt { get => IstFluegel + IstRahmen; }
        public decimal Istm2 { get; set; }

        public bool Rckstellen { get; set; }
        public string RckstellenGrund { get; set; }
        public bool Fertig { get => SollRahmen <= IstRahmen && SollFluegel <= IstFluegel; }
        public DateTime? FertigDat { get; set; }
    }
}
