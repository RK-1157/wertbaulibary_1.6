﻿using System;

namespace TageszaehlerLibary.Classes
{
    public class PositionSoll
    {
        public string AuNr { get; set; }
        public int PosId { get; set; }
        public string PosPositionsId { get; set; }
        public string PosNr { get; set; }
        public int SollKanteln { get; set; }
        public decimal Sollm2 { get; set; }
        public int KantelnSchlecht { get; set; }
        public int KantelnRep { get; set; }
        public int KantelnIst { get; set; }
        public DateTime? FertigDatum { get; set; }

        public bool Rckstellen { get; set; }
        public string RckstellenGrund { get; set; }
        public bool Fertig { get => SollKanteln <= KantelnRep + KantelnIst; }
        internal bool SetFertig { get; set; }
    }
}
