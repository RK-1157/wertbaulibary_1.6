﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TageszaehlerLibary.Classes
{
    public class AuftragSoll
    {
        public string AuNr { get; set; }
        public int SollKanteln { get; set; }
        public decimal Sollm2 { get; set; }
        public string Kennzeichen { get; set; }
    }
}
